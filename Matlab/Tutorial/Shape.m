classdef Shape
    properties
        height
        width
    end
    methods(Static)
        function out = setGetNumShapes(data)
            persistent Var; % Var is shared among all shapes
            if isempty(Var)
                Var = 0;
            end
            if nargin % no argument present
                Var = Var + data
            end
            out = Var;
        end
    end
    methods
        % Constructor with the same Name as the Class and File!
        function obj = Shape(h, w)
            obj.height = h;
            obj.width = w;
            obj.setGetNumShapes(1);
        end
        % Overloaded Display function
        function disp(obj)
            fprintf('Height: %.2f / Width :%.2f\n',...
                obj.height, obj.width);
        end
        function area = getArea(obj)
            area = obj.height * obj.width;
        end
        % Operator overloading
        function b = gt(obja,objb)
            b = obja.getArea > objb.getArea;
        end
    end
end