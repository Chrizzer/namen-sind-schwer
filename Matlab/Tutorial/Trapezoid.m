% Inheritance
classdef Trapezoid < Shape
    properties
        width2
    end
    
    methods
        function obj = Trapezoid(h, w1, w2)
            obj@Shape(h,w1);
            obj.width2 = w2;
        end
        
        % Overloaded Display function
        function disp(obj)
            fprintf('Height: %.2f / Width :%.2f / Width2 : %.2f\n',...
                obj.height, obj.width, obj.width2);
        end
        function area = getArea(obj)
            area = (obj.height / 2) * (obj.width + obj.width2);
        end
    end
end

