format compact
close all
clear all
clc % clear console
%% Input 
%{
name = input("This is how you get input! Type something...", "s");

if ~isempty(name)
    fprintf("Hello %s\n", name);
end
in = input("Enter a vector");
disp(in);
%}

%% Use Structures of Arrays, not Arrays of Structures
%{
s.coupon = [5.625 5.500 5.000 6.000]
TU1_index=[1 2 3];
FV1_index=[2 4];
s
s.coupon(TU1_index)
s.coupon(FV1_index)
%}

%% Variables
%{
% Every Number is double by default!
n = 8;
disp("8 = " + class(8));
% int8 - int64, uint8-uint64
disp("min(int8) = " + intmin("int8"));
disp("max(int8) = " + intmax("int8"));
disp("min(uint8) = " + intmin("uint8"));
disp("max(uint8) = " + intmax("uint8"));
% char
c = "A";
disp(c);
% logical
% single, double (Floating Point Numbers)
%}

%% help elfun
%{
help elfun
%}

%% CONDITIONALS
%{
% Relational Operators : >, <, >=, <=, == and ~=
% Logical Operators : ||, &&, ~ (Not)
%}

%% Vector
%{
% Row-Vectors
v1 = [1 2 3 4];
v11 = 1:1:4;
v11 = sort(v11, "descend");
disp(v1);
disp(v11);
v2 = [v1 v11];
disp(v2);
% Column-Vectors
v3 = [1;2;3;4];
disp(v3);
disp(v3 * v1);
disp(v1 * v3);
% implicit declaration
v4(1) = 1;
v4(2) = 2;
%}

%% Matrices
%{
m1 = [1 2 3;7 8 9];
disp(m1);
m2 =[4 5 6;3 2 1];
disp(m2);
disp(m1(1,2));
%}

%% Cell Array
%{
a1 = {"a", 1, [1 2 3]}
a2 = cell(5)

a1{4} = "appended";
disp(a1);
%}

%% Strings
%{
% Single Quotes 'Character Array'
% Double Quotes "String Array"
s1 = 'Hallo';
s2 = 'Welt';
disp(length(s1));
disp(strcat(s1, ' ', s2));
charArray = strsplit('Moin ich bin split bar');
disp(charArray);

ischar(s1)
ischar('Hallo')
%}

%% Structures
%{
g = struct(...
    'nodes', 1:12, ...
    'representation', '000000000000' ...
);
h.nodes = 1:12;
h.representation = '000000000000';
disp("g = ");
disp(g);
disp("h = ");
disp(h);
%}

%% Tables
%{
nodes =   {'1','2','3','4','5','6','7','8','9','10','11','12'};
visited = [0;0;0;0;0;0;0;0;0; 0; 0; 0];
varNames = "Visited"; % String Array!

table1 = table(...
    visited, ...
    'VariableNames', varNames, ...
    'RowNames', nodes...
);
table2 = table(...
	[0 0 0; 0 0 0; 0 0 0; 0 0 0], ...
    'VariableNames', "Row", ...
    'RowNames', {'1','2','3','4'}...
);
disp(table2);
%}

%% Functions
%{
% Functions have to be declared at the end of a file!
[a,b] = two_returnvalues_function
disp(cube(a));
disp(cube(b));

% Anonymous Functions / Lambdas
ccube = @(n) n * n * n;
disp(ccube(a));
disp(ccube(b));
factorial(5);
function result = cube(number)
    result = number * number * number;
end

function parameterless_function()
    % Do something
end

function [first, second] = two_returnvalues_function()
    first = randi(10,1);
    second = randi(100,1);
end

function result = variadic_input(varargin)
    result = 0;
    for i = 1:length(varargin)
       result = result + varargin{i}(1);
    end
end

function [varargout] = variadic_output(n)
    for i = 1:n
       varargout{1}(i) = i; 
    end
end

function val = factorial(num)
    if num == 1
        val = 1;
    else
        val = num * factorial(num - 1);
    end
end

% Functions are First-Class-Citizens, can be passed and returned as args
function func = hof(n)
    func = @(x) n + x
end
%}

%% Object-Oriented Matlab
% see Shape.m and Trapezoid
s = Shape(10, 5);
Shape.setGetNumShapes
t = Trapezoid(10,5,4);
disp(s);
disp(t);