import primitive_types as v

# comparison_operators: '<'|'>'|'=='|'>='|'<='|'<>'|'!='|'in'|'not' 'in'|'is'|'is' 'not'
# int_values op int_values und int_values op float_values worken wie erwartet...

for a in v.str_values:
    for b in v.str_values:
        print(f"{a} < {b} = {a < b}")
        print(f"{a} > {b} = {a > b}")
        print(f"{a} == {b} = {a == b}")
        print(f"{a} >= {b} = {a >= b}")
        print(f"{a} <= {b} = {a <= b}")
        # print(f"{a} <> {b} = {a <> b}") SyntaxError: Python3.7 does not support <> (Coming Soon)
        print(f"{a} != {b} = {a != b}")
        # print(f"{a} in {b} = {a in b}") -> TypeError: argument of type '<class>' is not iterable
        # print(f"{a} not in {b} = {a not in b}") -> TypeError: argument of type '<class>' is not iterable
        print(f"{a} is {b} = {a is b}")
        print(f"{a} is not {b} = {a is not b}")
        print("----------")

