condition = True
other_condition = False

# pass Keyword denotes a "null" operation that does nothing
# If-ElseIf-Else Statement
if condition:
    pass
elif other_condition:
    pass
else:
    pass
# While-Loop
while condition:
    break

while other_condition:
    pass
else:
    print("executes when condition is false")


collection = []

for i in collection:
    break

for i in range(0, 10, 2):
    break

letters = ["A", "B", "C"]
numbers = ["1", "2", "3"]

for letter, number in zip(letters, numbers):
    print(f"{letter} - {number}")

for index, letter in enumerate(letters):
    print(f"{index} - {letter}")

for i in collection:
    pass
else:
    print("always executed after the for loop terminates normally (no break)")

