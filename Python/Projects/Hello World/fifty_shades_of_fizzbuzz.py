n = 31


def f(x):
    if (x % 3 == 0) and (x % 5 == 0):
        return 'fizzbuzz'
    elif x % 3 == 0:
        return 'fizz'
    elif x % 5 == 0:
        return 'buzz'
    else:
        return str(x)


# Shade 2 of Fizzbuzz
[print(z, end=' ') for z in map(lambda x: 'fizzbuzz' if (x % 3 == 0) and (x % 5 == 0) else (
    'fizz' if (x % 3 == 0) else ('buzz' if x % 5 == 0 else str(x))), range(1, n))]


# map(x²,[1,2,3]) => [1,4,9]
# map(x+x,[1,2,3]) => [2,4,6]
# map(str(x),[1,2,3]) => ['1','2','3']

def mymap(a: int -> str)

    :
pass
