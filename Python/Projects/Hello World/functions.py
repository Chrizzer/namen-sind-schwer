# 2 New lines after a function suite (scope, but not really...)
def is_even(number: int) -> bool:
    """
    Description

    :param number: integer
    :return: True if the argument is even, otherwise False
    """
    return number % 2 == 0


def void_example() -> None:
    """
    If there is no return statement in a suite, None is returned

    :return: None
    """
    pass

