class Chessclass(object):
    def __init__(self, name: str, effect: []):
        self.name = name
        self.condition = f"Active when you have at least (N) different {name} pieces on chessboard."
        self.effect = effect

    def __repr__(self):
        return self.name
