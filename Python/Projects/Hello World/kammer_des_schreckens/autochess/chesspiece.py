from kammer_des_schreckens.autochess.chessclass import Chessclass
from kammer_des_schreckens.autochess.race import Race


class Chesspiece:
    def __init__(self, name: str, race: Race, klass: Chessclass, cost: int):
        self.name = name
        self.race = race
        self.klass = klass
        self.cost = cost
