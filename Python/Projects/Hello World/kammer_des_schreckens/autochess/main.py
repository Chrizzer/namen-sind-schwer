# Autochess Analyse helper...
import functools

from kammer_des_schreckens.autochess.chessclass import Chessclass
from kammer_des_schreckens.autochess.race import Race


def gen_classes() -> []:
    assassin = Chessclass(
        "Assassin",
        [
            "(3) Assassins: All friendly assassins have 15% chance to critical hit for 300% damage.",
            "(6) Assassins: All friendly assassins have 15% chance to critical hit for 400% damage.",
            "(9) Assassins: All friendly assassins have 15% chance to critical hit for 500% damage."
        ]
    )
    demon_hunter = Chessclass(
        "Demon Hunter",
        [
            "(1) DemonHunter: Count as an enemy demon.",
            "(2) DemonHunter: All friendly demons retain their Fel Power."
        ]
    )
    druid = Chessclass(
        "Druid",
        [
            "(2) Druid: two Druid★ can upgrade to a Druid★★.",
            "(4) Druid: two Druid★★ can upgrade to a Druid★★★."
        ]
    )
    hunter = Chessclass(
        "Hunter",
        [
            "(3) Hunters: All friendly hunters have +30 base attack damage and +30% chance to pierce through evasion.",
            "(6) Hunters: All friendly hunters have an additional +40 base attack damage and +40% chance to pierce through evasion."
        ]
    )
    knight = Chessclass(
        "Knight",
        [
            "Shield gives 75% magic resistance and 30 armor for 3 seconds",
            "(2) Knights: All friendly knights are now, for 30% of the time, protected by a damage-reduction shield.",
            "(4) Knights: All friendly knights have an additional 30% of the time being protected by a damage-reduction shield.",
            "(6) Knights: All friendly knights have an additional 30% of the time being protected by a damage-reduction shield."
        ]
    )
    mage = Chessclass(
        "Mage",
        [
            "(3) Mages: All enemies have their magic resistance reduced by 40%.",
            "(6) Mages: All enemies have their magic resistance reduced by an additional 40%."
        ]
    )
    mech = Chessclass(
        "Mech",
        [
            "(2) Mechs: All friendly mechs have +15 HP regeneration.",
            "(4) Mechs: All friendly mechs have an additional +25 HP regeneration."
        ]
    )
    shaman = Chessclass(
        "Shaman",
        [
            "(2) Shaman: Hexes a random enemy for 6s when battle starts."
        ]
    )
    warlock = Chessclass(
        "Warlock",
        [
            "(3) Warlocks: All allies have +20% lifesteal.",
            "(6) Warlocks: All allies have an additional +30% lifesteal.",
            ""
        ]
    )
    warrior = Chessclass(
        "Warrior",
        [
            "(3) Warriors: All friendly warriors have +7 armor.",
            "(6) Warriors: All friendly warriors have +8 armor.",
            "(9) Warriors: All friendly warriors have +9 armor."
        ]
    )
    return [assassin, demon_hunter, druid, hunter, knight, mage, mech, shaman, warlock, warrior]


def gen_races() -> []:
    def default_condition(name: str):
        return f"Active when there are at least (N) different {name} chess pieces on the chessboard"

    dragon = Race("Dragon", default_condition("Dragon"),
                  ["(3) Dragons: All friendly dragons have 100 mana when battle starts."])
    dwarf = Race("Dwarf", "", ["Attack range increased by 300."])
    demon = Race("Demon", "Active when you control only one demon on the chessboard",
                 ["Deal 50% extra pure damage to the target."])
    ogre = Race("Ogre", "", ["Max hp increased by 10%"])
    elf = Race("Elf", default_condition("Elf"),
               ["(3) Elfs: All friendly elves have +20% evasion.", "(6) Elfs: All friendly elves have +25% evasion.",
                "(9) Elfs: All friendly elves have +30% evasion."])
    undead = Race("Undead", default_condition("Undead"),
                  ["(2) Undeads: All enemies have -4 armor.", "(4) Undeads: All enemies have -6 armor."])
    orc = Race("Orc", default_condition("Orc"),
               ["(2) Orcs: All friendly orcs have +200 max HP.", "(4) Orcs: All friendly orcs have +300 max HP."])
    goblin = Race("Goblin", default_condition("Goblin"),
                  ["(3) Goblins: Grants a random ally +15 armor and +10 HP regeneration.",
                   "(6) Goblins: Grants all allies with +15 armor and +10 HP regeneration."])
    elemental = Race("Elemental", default_condition("Elemental"), [
        "(2) Elementals: All friendly elementals have 30% chance to turn the attacker into stone for 4s when attacked by melee chesses.",
        "(4) Elementals: All friendly chesses have 30% chance to turn the attacker into stone for 4s when attacked by melee chesses."])
    human = Race("Human", default_condition("Human"),
                 ["(2) Humans: All friendly humans have 20% chance to silence target for 4s when attacking.",
                  "(4) Humans: All friendly humans have 25% chance to silence target for 4s when attacking.",
                  "(6) Humans: All friendly humans have 30% chance to silence target for 4s when attacking."])
    naga = Race("Naga", default_condition("Naga"), ["(2) Nagas: All allies have +30% magic resistance.",
                                                    "(4) Nagas: All allies have an additional +30% magic resistance."])
    troll = Race("Troll", default_condition("Troll"), ["(2) Trolls: All friendly trolls have +35 attack speed.",
                                                       "(4) Trolls: All allies have +30 attack speed."])
    beast = Race("Beast", default_condition("Beast"),
                 ["(2) Beasts: All allies have +10% attack damage, including summoned units.", "", ""])
    satyr = Race("Satyr", "",
                 ["Hides your bench from other players' vision when there are Satyrs in your waiting bench.",
                  "Hides your pieces on the chess board until the battle begins if you have a Satyr on the board."])

    return [dragon, dwarf, demon, ogre, elf, undead, orc, goblin, elemental, human, naga, troll, beast, satyr]


def gen_chesspieces(classes, races) -> {}:
    chesspieces = {}

    assassin = classes[0]
    demon_hunter = classes[1]
    druid = classes[2]
    hunter = classes[3]
    knight = classes[4]
    mage = classes[5]
    mech = classes[6]
    shaman = classes[7]
    warlock = classes[8]
    warrior = classes[9]

    dragon = races[0]
    dwarf = races[1]
    demon = races[2]
    ogre = races[3]
    elf = races[4]
    undead = races[5]
    orc = races[6]
    goblin = races[7]
    elemental = races[8]
    human = races[9]
    naga = races[10]
    troll = races[11]
    beast = races[12]
    satyr = races[13]

    # 1-Cost pieces
    chesspieces["Axe"] = (orc, warrior, 1)
    chesspieces["Enchantress"] = (beast, druid, 1)
    chesspieces["Ogre Magi"] = (ogre, mage, 1)
    chesspieces["Tusk"] = (beast, warrior, 1)
    chesspieces["Drow Ranger"] = (undead, hunter, 1)
    chesspieces["Bounty Hunter"] = (goblin, assassin, 1)
    chesspieces["Clockwerk"] = (goblin, mech, 1)
    chesspieces["Shadow Shaman"] = (troll, shaman, 1)
    chesspieces["Batrider"] = (troll, knight, 1)
    chesspieces["Tinker"] = (goblin, mech, 1)
    chesspieces["Anti Mage"] = (elf, demon_hunter, 1)
    chesspieces["Tiny"] = (elemental, warrior, 1)

    # 2-Cost pieces
    chesspieces["Crystal Maiden"] = (human, mage, 2)
    chesspieces["Beastmaster"] = (orc, hunter, 2)
    chesspieces["Juggernaut"] = (orc, warrior, 2)
    chesspieces["Timbersaw"] = (goblin, mech, 2)
    chesspieces["Queen of Pain"] = (demon, assassin, 2)
    chesspieces["Puck"] = ([elf, dragon], mage, 2)
    chesspieces["Witch Doctor"] = (troll, warlock, 2)
    chesspieces["Slardar"] = (naga, warrior, 2)
    chesspieces["Chaos Knight"] = (demon, knight, 2)
    chesspieces["Treant protector"] = (elf, druid, 2)
    chesspieces["Morphling"] = (elemental, assassin, 2)
    chesspieces["Luna"] = (elf, knight, 2)
    chesspieces["Furion"] = (elf, druid, 2)
    chesspieces["Slark"] = (naga, assassin, 2)

    # 3-Cost Pieces
    chesspieces["Lycan"] = ([human, beast], warrior, 3)
    chesspieces["Venomancer"] = (beast, warrior, 3)
    chesspieces["Omniknight"] = (human, knight, 3)
    chesspieces["Razor"] = (elemental, mage, 3)
    chesspieces["Windranger"] = (elf, hunter, 3)
    chesspieces["Phantom Assassin"] = (elf, assassin, 3)
    chesspieces["Abaddon"] = (undead, knight, 3)
    chesspieces["Sand King"] = (beast, assassin, 3)
    chesspieces["Sniper"] = (dwarf, hunter, 3)
    chesspieces["Terrorblade"] = (demon, demon_hunter, 3)
    chesspieces["Viper"] = (dragon, assassin, 3)
    chesspieces["Shadow Fiend"] = (demon_hunter, warrior, 3)
    chesspieces["Lina"] = (human, mage, 3)
    chesspieces["Riki"] = (satyr, assassin, 3)
    chesspieces["Mirana"] = (elf, hunter, 3)

    # 4-Cost Pieces
    chesspieces["Doom"] = (demon, warrior, 4)
    chesspieces["Kunkka"] = (human, warrior, 4)
    chesspieces["Troll Warlord"] = (troll, warrior, 4)
    chesspieces["Keeper of the Light"] = (human, mage, 4)
    chesspieces["Necrophos"] = (undead, warlock, 4)
    chesspieces["Templar Assassin"] = (elf, assassin, 4)
    chesspieces["Alchemist"] = (goblin, warlock, 4)
    chesspieces["Disruptor"] = (orc, shaman, 4)
    chesspieces["Medusa"] = (naga, hunter, 4)
    chesspieces["Dragon Knight"] = ([human, dragon], knight, 4)
    chesspieces["Lone Druid"] = (beast, druid, 4)

    # 5-Cost Pieces
    chesspieces["Gyrocopter"] = (dwarf, mech, 5)
    chesspieces["Lich"] = (undead, mage, 5)
    chesspieces["Tidehunter"] = (naga, hunter, 5)
    chesspieces["Enigma"] = (elemental, warlock, 5)
    chesspieces["Techies"] = (goblin, mech, 5)
    chesspieces["Death Prophet"] = (undead, warlock, 5)

    return chesspieces


def print_all(pieces: dict, names: []):
    matches = []

    for name in names:
        for key in pieces:
            piece = pieces[key]
            if isinstance(piece[0], list):
                if name in piece[0]:
                    matches.append((key,) + piece)
            else:
                if name in piece:
                    matches.append((key,) + piece)
    if len(matches) > 0:

        most_likely_race = {}
        for race in [x for x in map(lambda e: e[1], matches)]:
            if isinstance(race, list):
                for rrace in race:
                    if rrace not in most_likely_race:
                        most_likely_race[rrace] = 0
                    most_likely_race[rrace] += 1
            else:
                if race not in most_likely_race:
                    most_likely_race[race] = 0
                most_likely_race[race] += 1

        (most_prevalent_race, race_max) = "", 0
        for (race_name, occurences) in most_likely_race.items():
            if race_max < occurences:
                race_max = occurences
                most_prevalent_race = race_name

        most_likely_class = {}
        for klass in [x for x in map(lambda e: e[2], matches)]:
            if isinstance(klass, list):
                for rklass in klass:
                    if rklass not in most_likely_class:
                        most_likely_class[rklass] = 0
                    most_likely_class[rklass] += 1
            else:
                if klass not in most_likely_class:
                    most_likely_class[klass] = 0
                most_likely_class[klass] += 1

        (most_prevalent_class, class_max) = "", 0
        for (class_name, occurences) in most_likely_class.items():
            if class_max < occurences:
                class_max = occurences
                most_prevalent_class = class_name

        print(f"{len(most_likely_race)} {most_prevalent_race}{'s' if race_max > 1 else ''}")
        print(most_prevalent_race.condition)
        print("\n".join(most_prevalent_race.effect))
        print()
        print(f"{len(most_likely_class)} {most_prevalent_class}{'s' if class_max > 1 else ''}")
        print(most_prevalent_class.condition)
        print("\n".join(most_prevalent_class.effect))
        print()
        for match in matches:
            print(match[0], match[1:])
        print()


def get_object(list: [], names: []):
    return [x for x in list if x.name in names]


if __name__ == '__main__':
    classes = gen_classes()
    races = gen_races()

    assassin, demon_hunter, druid, hunter, knight, mage, mech, shaman, warlock, warrior = classes
    dragon, dwarf, demon, ogre, elf, undead, orc, goblin, elemental, human, naga, troll, beast, satyr = races

    pieces = gen_chesspieces(classes, races)

    print_all(pieces, get_object(races, "Beast"))
