class Race(object):
    def __init__(self, name: str, condition: str, effect: []):
        self.name = name
        self.condition = condition
        self.effect = effect

    def __repr__(self):
        return self.name
