from tkinter import *
from tkinter.ttk import *
from tkinter.tix import *
import sys
import PyPDF4 as PDF


class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

        # Hotkey ESCAPE -> Closes Window
        master.bind('<Escape>', lambda event: sys.exit())

        # Faster Iteration
        def use_example_pdf(event):
            # print("\n".join(list(filter(lambda s: s[0] != "_", dir(self.filehandler)))))
            self.filehandler.subwidget("entry").insert(END,
                                                       "D:\Repo Dump\\namen-sind-schwer\Python\Projects\Hello World\kammer_des_schreckens\\beispiel.pdf")
            self.filehandler.focus()

        master.bind("ä", use_example_pdf)

        self.create_keywordhandler()
        self.create_filehandler()
        self.create_footer()
        self.create_contentrenderer()
        self.pack(fill="both", expand=True)

    def create_contentrenderer(self):
        self.contentrenderer = ScrolledText(self, scrollbar="auto")
        self.contentrenderer.pack(side="bottom", fill="both", expand=True)

    def create_filehandler(self):
        def load(path: str):
            if path != "":
                try:
                    reader = PDF.PdfFileReader(path)
                    self.contentrenderer.text.delete(0.0, END)
                    # PyPDF4 - Version
                    # Type-Hint (Bad Practise?)
                    page: PDF.pdf.PageObject
                    for page in reader.pages:
                        self.contentrenderer.text.insert(END, " ".join(page.extractText().split()))
                except FileNotFoundError:
                    pass
                except UnicodeDecodeError:
                    pass

        # FileEntry Setup
        self.filehandler = FileEntry(self, label="Choose a file", command=load)
        self.filehandler.pack(side="top", fill="x", padx=16, pady=4, expand=True)

    def hotkey(event):
        key = repr(event.char)
        if key == "\x1b":
            pass

    def callback(event):
        print("clicked at", event.x, event.y)

    def create_keywordhandler(self):
        def search() -> bool:
            keyword = self.keywordhandler.entry.get()

            # for (k, v) in self.contentrenderer.text.configure().items():
            #     print(f"{k}: {v}")
            text = self.contentrenderer.text.get("1.0", "end-1c")

            self.update_footer(f"{self.search(keyword, text)} Treffer für \"{keyword}\"")

            return True

        self.keywordhandler = LabelEntry(self, label="Keyword")
        self.keywordhandler.entry["validate"] = "focusout"
        self.keywordhandler.entry["validatecommand"] = search
        # print("\n".join(dir(self.keywordhandler.entry)))

        self.keywordhandler.pack(side="top", fill="x", padx=16, pady=4, expand=True)

    def search(self, keyword: str, text: str) -> int:
        counter = 0
        if keyword == "":
            return 0
        words = text.split(" ")
        for (index, word) in enumerate(words):
            if word == keyword:
                counter += 1
                print(" ".join(words[max(0, index - 5):min(index + 5, len(words))]))
        return counter

    def create_footer(self):
        self.footer = Label(self, text="Bitte Suchwort eingeben!")
        # for (k, v) in self.footer.configure().items():
        #     print(f"{k}: {v}")

        self.footer.pack(side="bottom", fill="x", padx=16, pady=4, expand=True)

    def update_footer(self, somestr: str):
        self.footer["text"] = somestr
        self.footer.update()
