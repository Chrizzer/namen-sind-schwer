from tkinter import tix
import kammer_des_schreckens.tkinter_test.application
from screeninfo import get_monitors

# TIP 1: configure() -> some_widget.configure() => All options

monitor = get_monitors()[0]
# print(screen_width, screen_height)
root = tix.Tk()

# Width x Height + X_Offset + Y_Offset (Screen coordinates)
root.geometry(f"512x256+{monitor.width // 2 - 320}+{monitor.height // 2 - 320}")
del monitor
app = kammer_des_schreckens.tkinter_test.application.Application(master=root)
app.mainloop()
