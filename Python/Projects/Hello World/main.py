with open("NetflixViewingHistory.csv") as f:

    entries = {}
    for line in f:
        name = line.split(",")[0]
        true_name = name.split(":")[0]

        if true_name in entries:
            entries[true_name] += 1
        else:
            entries[true_name] = 1

    sorted_x = reversed(sorted(filter(lambda kv: kv[1] > 1, entries.items()), key=lambda kv: kv[1]))
    count = 0
    for (name, n) in sorted_x:
        print(f"{n} - {name}")
        count += 1

    print(count)
