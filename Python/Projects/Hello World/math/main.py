from typing import Callable

from numpy import *
from itertools import chain, combinations
import matplotlib.pyplot as plt


# Logikzeichen (∀, ∃, ∄, ∧, ∨, )
# Mengenzeichen (∅, ∈, ∉, ∖, ∩, ∪

# LADS 2_2.)
def lads_2_2():
    m1 = set([1, 2, 3, 4, 5])
    m2 = set([n for n in range(0, 100) if n >= 4])
    m3 = set([2 * n for n in range(0, 100)])
    m4 = set([3 * n for n in range(0, 100)])

    print("M1 = {1,2,3,4,5}")
    print("M2 = {n ∈ N0 | n >= 4}")
    print("M3 = {2n | n ∈ N}")
    print("M4 = {3n | n ∈ N}")
    print()

    print("M1 ∩ M2 = ", m1.intersection(m2))
    print("M1 ∪ M2 = ", m1.union(m2))
    print("M1 ∖ M3 = ", m1.difference(m3))
    print("(M2 ∖ M4) ∩ M3 = ", m2.difference(m4).intersection(m3))
    print()


# LADS 3_1.)

def potenzmenge(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return set(chain.from_iterable(combinations(s, r) for r in range(len(s) + 1)))


def lads_3_1():
    m = [1, 2, 3]
    n = [2, 3]

    print(potenzmenge(m).intersection(potenzmenge(n)))
    print()


# LADS 3_2.)

def carthesian(a: set, b: set) -> set:
    return set([(x, y) for x in a for y in b])


def lads_3_2():
    L = {1}
    M = {2}
    N = {3}
    O = {4}
    print("L =", L, "M =", M, "N =", N, "O =", O)

    LM = carthesian(L, M)
    print("LxM =", LM)
    NO = carthesian(N, O)
    print("NxO =", NO)
    LN = L.union(N)
    print("L∪N =", LN)
    MO = M.union(O)
    print("M∪O =", MO)
    print("(LxM) ∪ (NxO) =", LM.union(NO))
    print("(L∪N) x (M∪O) =", carthesian(LN, MO))

    def prove_A1(L: set, M: set, N: set, O: set) -> bool:
        return carthesian(L, M).union(carthesian(N, O)).issubset(carthesian(L.union(N), M.union(O)))

    def prove_A2(L: set, M: set, N: set, O: set) -> bool:
        return carthesian(L.union(N), M.union(O)).issubset(carthesian(L, M).union(carthesian(N, O)))

    print("A1: (LxM) ∪ (NxO) ist teilmenge von (L∪N) x (M∪O)")
    print("A2: (LxM) ∪ (NxO) ist obermenge von (L∪N) x (M∪O)")
    print()

    def construct_proves(l, m, n, o):
        interpretation = f"L={set(l)}, M={set(m)}, N={set(n)}, O={set(o)}".replace("set()", "∅")

        a1 = prove_A1(set(l), set(m), set(n), set(o))
        a2 = prove_A2(set(l), set(m), set(n), set(o))

        print("A1:", a1, end=" " * (2 if a1 else 1))
        print("A2:", a2, end=" " * (2 if a2 else 1))
        print(interpretation)

    construct_proves([], [], [], [])
    construct_proves([1], [], [3], [])
    construct_proves([], [2], [], [4])
    print()
    construct_proves([], [2], [3], [])
    construct_proves([1], [2], [3], [])
    construct_proves([], [2], [3], [4])
    construct_proves([1], [], [], [4])


f1 = lambda n: n + 1
f2 = lambda n: n + 1
f3 = lambda n, m: n + m
f4 = lambda n: 2 * n
f5 = lambda n: 2 * n
# f6 lässt sich nicht beschreiben, weil sie linkstotal

natural_numbers = list(range(0, 11))
integers = list(range(-10, 11))
rational_numbers = sorted([n/m for n in filter(lambda x: x != 0, integers) for m in filter(lambda x: x != 0, natural_numbers)])

plt.figure(1)

plt.subplot(121)
plt.plot(natural_numbers, [f1(n) for n in natural_numbers], color="b")
plt.axis([0, 10, 0, 10])
plt.subplot(122)
plt.plot(integers, [f2(n) for n in integers], color="r")
plt.axis([-10, 10, -10, 10])


# plt.show()

def plot_binary_matrix(x: [], f: Callable[[int], int]):
    # r: (a,b) -> string
    print("x", end=":")
    for n in x:
        s = str(n)
        my_length = len(s)
        other_length = len(str(f(n)))
        indent = " " * (max(0, other_length - my_length))
        print(indent + s, end="|")
    print("\ny", end=":")
    for n in x:
        s = str(f(n))
        my_length = len(s)
        other_length = len(str(n))
        indent = " " * (max(0, other_length - my_length))
        print(indent + s, end="|")
    print()


def plot_matrix(numbers: [], f: Callable[[int, int], int]):
    print(0, end="|")
    indent = len(str(max([f(n, m) for n in numbers for m in numbers]))) - 1
    indent_str = " " * indent
    for x in numbers:
        print(indent_str + str(x), end="|")
    print()
    for x in numbers:
        print(x, end="|")
        for y in numbers:
            n = f(x, y)
            print((indent_str if n < 10 else "") + str(n), end="|")
        print()
    print()


plot_binary_matrix(natural_numbers, f1)
print()
plot_binary_matrix(integers, f2)
print()
plot_matrix(natural_numbers, f3)
print()
plot_binary_matrix(integers, f4)
print()
plot_binary_matrix(rational_numbers, f5)
print()