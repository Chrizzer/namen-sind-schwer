"""
In Part A, we unrealistically assumed that your salary didn’t change.  But you are an MIT graduate, and
clearly you are going to be worth more to your company over time! So we are going to build on your
solution to Part A by factoring in a raise every six months.
In ​ps1b.py​, copy your solution to Part A (as we are going to reuse much of that machinery). 
Modify your program to include the following
    1. Have the user input a semi-annual salary raise ​semi_annual_raise​​ (as a decimal percentage)
    2. After the 6​ th​  month, increase your salary by that percentage.  Do the same after the 12th month, 18th month, and so on

Write a program to calculate how many months it will take you save up enough money for a down
payment. Like before, assume that your investments earn a return of ​r​​ = 0.04 (or 4%) and the
required down payment percentage is 0.25 (or 25%).  Have the user enter the following variables:
    1. The starting annual salary (annual_salary)
    2. The percentage of salary to be saved (portion_saved)
    3. The cost of your dream home (total_cost)
    4. The semi­annual salary raise (semi_annual_raise)
"""


def calc_months_to_downpayment_with_raises(
        annual_salary: float,
        cost: float,
        percentage_saved_per_month: float,
        semi_annual_raise: float
) -> int:
    current_savings = 0
    portion_down_payment = 0.25
    r = 0.04
    monthly_salary = annual_salary / 12
    month = 0

    while current_savings < (portion_down_payment * cost):
        # Advance a month
        month += 1
        # raise every 6 months #Ternary Operator
        # reap in the cash
        money_saved = monthly_salary * percentage_saved_per_month
        annual_return = current_savings * r / 12
        current_savings = current_savings + money_saved + annual_return
        monthly_salary += monthly_salary * semi_annual_raise if month % 6 == 0 else 0

    return month


# Tests
print(
    calc_months_to_downpayment_with_raises(
        annual_salary=120000,
        cost=500000,
        percentage_saved_per_month=0.05,
        semi_annual_raise=0.03
    ) == 142
)

print(
    calc_months_to_downpayment_with_raises(
        annual_salary=80000,
        cost=800000,
        percentage_saved_per_month=0.1,
        semi_annual_raise=0.03
    ) == 159
)

print(
    calc_months_to_downpayment_with_raises(
        annual_salary=75000,
        cost=1500000,
        percentage_saved_per_month=0.05,
        semi_annual_raise=0.05
    ) == 261
)


# while input("continue? ") not in ["n", "no"]:
#     annual_salary = float(input("Enter the starting annual salary: "))
#     total_cost = float(input("Enter the cost of your dream home: "))
#     portion_saved = int(input("Enter the percent of your salary to save, as %: ")) / 100
#     semi_annual_raise = int(input("Enter the semi-annual raise, as %: ")) / 100
#     print("Number of months:",
#           calc_months_to_downpayment_with_raises(annual_salary, total_cost, portion_saved, semi_annual_raise))
