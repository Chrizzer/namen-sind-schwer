# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

from itertools import permutations as perm


def get_permutations(sequence: str) -> list:
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    result = set()
    if len(sequence) == 1:
        return [sequence]
    else:
        for char in sequence:
            [result.add(char + permutation) for permutation in get_permutations(sequence.replace(char, ""))]
    return list(result)


if __name__ == '__main__':
    #    #EXAMPLE
    #    example_input = 'abc'
    #    print('Input:', example_input)
    #    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
    #    print('Actual Output:', get_permutations(example_input))

    #    # Put three example test cases here (for your sanity, limit your inputs
    #    to be three characters or fewer as you will have n! permutations for a 
    #    sequence of length n)

    # Test 1: ("abc") -> ["abc", "acb", "bac", "bca", "cab", "cba"]
    expected_abc = ["abc", "acb", "bac", "bca", "cab", "cba"]
    actual_abc = get_permutations("abc")
    expected_abc.sort()
    actual_abc.sort()
    print(f"Test 1: Expected {expected_abc}\n        Actual   {actual_abc}")
    all = len(expected_abc) == len(actual_abc)
    if all:
        for permutation in actual_abc:
            all &= permutation in expected_abc
            if not all:
                print(f"Test 1: FAILURE - Unexpected permutation \"{permutation}\"")
    else:
        print(f"Test 1: FAILURE - Actual and Expected don't have the same length")
    if all:
        print(f"Test 1: SUCCESS")

    # Test 2: ("bcd") -> ???
    expected_bcd = ["bcd", "bdc", "cbd", "cdb", "dbc", "dcb"]
    actual_bcd = get_permutations("bcd")
    expected_bcd.sort()
    actual_bcd.sort()
    print(f"Test 2: Expected {expected_bcd}\n        Actual   {actual_bcd}")
    all = len(expected_bcd) == len(actual_bcd)
    if all:
        for permutation in expected_bcd:
            all &= permutation in actual_bcd
            if not all:
                print(f"Test 2: FAILURE - Unexpected permutation \"{permutation}\"")
    else:
        print(f"Test 2: FAILURE - Actual and Expected don't have the same length")
    if all:
        print(f"Test 2: SUCCESS")
