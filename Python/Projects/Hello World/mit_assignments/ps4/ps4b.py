# Problem Set 4B
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string
import random


### HELPER CODE ###
def load_words(file_name):
    """
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    wordlist = []
    with open(file_name, 'r') as file:
        for line in file:
            wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def is_word(word_list, word):
    """
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    """
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'


class Message(object):
    def __init__(self, text: str):
        """
        Initializes a Message object
                
        text (string): the message's text

        a Message object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        """
        self.message_text = text
        self.valid_words = []

    def get_message_text(self) -> str:
        """
        Used to safely access self.message_text outside of the class
        
        Returns: self.message_text
        """
        return self.message_text

    def get_valid_words(self) -> list:
        """
        Used to safely access a copy of self.valid_words outside of the class.
        This helps you avoid accidentally mutating class attributes.
        
        Returns: a COPY of self.valid_words
        """
        return load_words(WORDLIST_FILENAME)

    def build_shift_dict(self, shift: int) -> dict:
        """
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift. The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.        
        
        shift (integer): the amount by which to shift every letter of the 
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        """
        cipher = {}
        for char in string.ascii_lowercase:
            cipher[char] = string.ascii_lowercase[(string.ascii_lowercase.find(char) + shift) % 26]
        for char in string.ascii_uppercase:
            cipher[char] = string.ascii_uppercase[(string.ascii_uppercase.find(char) + shift) % 26]
        return cipher

    def apply_shift(self, shift: int) -> str:
        """
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift        
        
        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
        down the alphabet by the input shift
        """
        encrypted_message = ""
        cipher = self.build_shift_dict(shift)
        for char in self.get_message_text():
            encrypted_message += cipher.get(char, char)
        return encrypted_message


class PlaintextMessage(Message):
    def __init__(self, text: str, shift: int):
        """
        Initializes a PlaintextMessage object        
        
        text (string): the message's text
        shift (integer): the shift associated with this message

        A PlaintextMessage object inherits from Message and has five attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
            self.shift (integer, determined by input shift)
            self.encryption_dict (dictionary, built using shift)
            self.message_text_encrypted (string, created using shift)

        """
        Message.__init__(self, text)
        self.shift = shift
        # self.encryption_dict = self.build_shift_dict(shift)
        self.message_text_encrypted = self.apply_shift(shift)

    def get_shift(self) -> int:
        """
        Used to safely access self.shift outside of the class
        
        Returns: self.shift
        """
        return self.shift

    def get_encryption_dict(self):
        """
        Used to safely access a copy self.encryption_dict outside of the class
        
        Returns: a COPY of self.encryption_dict
        """
        return self.build_shift_dict(self.get_shift())

    def get_message_text_encrypted(self):
        """
        Used to safely access self.message_text_encrypted outside of the class
        
        Returns: self.message_text_encrypted
        """
        return self.message_text_encrypted

    def change_shift(self, shift):
        """
        Changes self.shift of the PlaintextMessage and updates other 
        attributes determined by shift.        
        
        shift (integer): the new shift that should be associated with this message.
        0 <= shift < 26

        Returns: nothing
        """
        self.shift = shift if 0 <= shift < 26 else self.shift


class CiphertextMessage(Message):
    def __init__(self, text: str):
        """
        Initializes a CiphertextMessage object
                
        text (string): the message's text

        a CiphertextMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        """
        Message.__init__(self, text)

    def decrypt_message(self, valid_words=None) -> tuple:
        """
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value 
        for decrypting it.

        Note: if multiple shifts are equally good such that they all create 
        the maximum number of valid words, you may choose any of those shifts 
        (and their corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        """
        shift, best = 0, 0
        accepted_words = self.get_valid_words() if valid_words is None else valid_words

        for i in range(1, 27):
            decrypt_i = self.apply_shift(i)
            decrypted_words = decrypt_i.strip(",").split(" ")
            count = 0
            for word in decrypted_words:
                if word in accepted_words:
                    count += 1
            # Replace shift, best
            if best < count:
                shift = i
                best = count
            # if we found the best case (fully translated) then break
            if best == len(decrypted_words):
                break
        return shift, best


if __name__ == '__main__':
    #    #Example test case (PlaintextMessage)
    #    plaintext = PlaintextMessage('hello', 2)
    #    print('Expected Output: jgnnq')
    #    print('Actual Output:', plaintext.get_message_text_encrypted())
    #
    #    #Example test case (CiphertextMessage)
    #    ciphertext = CiphertextMessage('jgnnq')
    #    print('Expected Output:', (24, 'hello'))
    #    print('Actual Output:', ciphertext.decrypt_message())
    words = load_words(WORDLIST_FILENAME)

    # Test 1: Identity Test, should cover all instances

    message = Message("hallo welt")
    for i in range(50):
        og_shifted: str = message.apply_shift(i)
        re_shifted: str = Message(og_shifted).apply_shift(26 - i)
        if message.get_message_text() != re_shifted:
            print(f"FAILURE: {message.get_message_text()} -> {og_shifted} -> {re_shifted}")
    else:
        print(f"Identity Test: SUCCESS")

    # Test 2: CiphertextMessage
    words_length = len(words)
    for i in range(4):
        # choose random words
        max = random.randrange(3, 10)
        msg = ""
        for j in range(1, max):
            msg += words[random.randrange(0, words_length)] + " "
        # choose a random shift
        r_shift = random.randint(0, 26)
        # construct objects
        og_cipher = CiphertextMessage(msg)
        # construct encrypted message
        encrypted_msg = og_cipher.apply_shift(r_shift)
        enc_cipher = CiphertextMessage(encrypted_msg)
        # decrypt message
        (shift, best) = enc_cipher.decrypt_message(words)
        print(f"Message = {msg} -> {encrypted_msg} with {r_shift}", end=" ")
        if 26 - shift != r_shift:
            print(f"FAILURE: {msg} -> {r_shift} != {shift}")
            break
        else:
            print(f"SUCCESS")
    else:
        print(f"TEST SUCCESSFUL")
