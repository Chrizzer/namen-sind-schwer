# 6.0001/6.00 Problem Set 5 - RSS Feed Filter
# Name:
# Collaborators:
# Time:

import mit_assignments.ps5.feedparser as feedparser
import string
import time
import threading
from mit_assignments.ps5.project_util import translate_html
from mit_assignments.ps5.mtTkinter import *
from datetime import datetime
import pytz
import itertools


# -----------------------------------------------------------------------

# ======================
# Code for retrieving and parsing
# Google and Yahoo News feeds
# Do not change this code
# ======================

def process(url):
    """
    Fetches news items from the rss url and parses them.
    Returns a list of NewsStory-s.
    """
    feed = feedparser.parse(url)
    entries = feed.entries
    ret = []
    for entry in entries:
        guid = entry.guid
        title = translate_html(entry.title)
        link = entry.link
        description = translate_html(entry.description)
        pubdate = translate_html(entry.published)

        try:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %Z")
            pubdate.replace(tzinfo=pytz.timezone("GMT"))
        #  pubdate = pubdate.astimezone(pytz.timezone('EST'))
        #  pubdate.replace(tzinfo=None)
        except ValueError:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %z")

        newsStory = NewsStory(guid, title, description, link, pubdate)
        ret.append(newsStory)
    return ret


# ======================
# Data structure design
# ======================

# Problem 1

class NewsStory(object):
    def __init__(self, guid: str, title: str, description: str, link: str, pubdate: datetime):
        self.guid = guid
        self.title = title
        self.description = description
        self.link = link
        self.pubdate = pubdate

    def get_guid(self) -> str:
        return self.guid

    def get_title(self) -> str:
        return self.title

    def get_description(self) -> str:
        return self.description

    def get_link(self) -> str:
        return self.link

    def get_pubdate(self) -> datetime:
        return self.pubdate


# ======================
# Triggers
# ======================

class Trigger(object):
    def evaluate(self, story: NewsStory) -> bool:
        """
        Returns True if an alert should be generated
        for the given news item, or False otherwise.
        """
        # DO NOT CHANGE THIS!
        raise NotImplementedError


# PHRASE TRIGGERS

# Problem 2
class PhraseTrigger(Trigger):
    IGNORE_CHARACTERS = string.punctuation + string.whitespace

    def __init__(self, phrase: string = ""):
        self.phrase = phrase.lower()

    def evaluate(self, story: NewsStory) -> bool:
        raise NotImplementedError

    def is_phrase_in(self, text: str) -> bool:
        """
        Returns True, if phrase is present in text.

        Returns False, otherwise
        """
        # print(f"Phrase: {self.phrase}")
        # print(f"Text  : {text}")
        filtered_text = []
        buffer = ""
        for char in text:
            if char in PhraseTrigger.IGNORE_CHARACTERS:
                if len(buffer) > 0:
                    filtered_text.append(buffer)
                    buffer = ""
            else:
                buffer += char.lower()
        else:
            if buffer != "":
                filtered_text.append(buffer)
            del buffer
        # print(f"Filtered Text: {filtered_text}")
        filtered_phrase = []
        buffer = ""
        for char in self.phrase:
            if char in PhraseTrigger.IGNORE_CHARACTERS:
                if len(buffer) > 0:
                    filtered_phrase.append(buffer)
                    buffer = ""
            else:
                buffer += char.lower()
        else:
            if buffer != "":
                filtered_phrase.append(buffer)
        # print(f"Filtered Phrase: {filtered_phrase}")
        if len(filtered_phrase) > len(filtered_text):
            return False
        else:
            phrase_len = len(filtered_phrase)
            for (index, word) in enumerate(filtered_text):
                if word == filtered_phrase[0]:
                    if filtered_phrase == filtered_text[index: index + phrase_len]:
                        return True
        return False


# Problem 3
class TitleTrigger(PhraseTrigger):

    def __init__(self, phrase: string = ""):
        PhraseTrigger.__init__(self, phrase)

    def evaluate(self, story: NewsStory) -> bool:
        return self.is_phrase_in(story.get_title())

    def __str__(self) -> str:
        return "(" + "Title " + self.phrase.__str__() + ")"

    # Problem 4


class DescriptionTrigger(PhraseTrigger):

    def __init__(self, phrase: string = ""):
        PhraseTrigger.__init__(self, phrase)

    def evaluate(self, story: NewsStory) -> bool:
        return self.is_phrase_in(story.get_description())

    def __str__(self) -> str:
        return "(" + "Description " + self.phrase.__str__() + ")"


# TIME TRIGGERS

# Problem 5
class TimeTrigger(Trigger):

    def __init__(self, time: str):
        self.time = datetime.strptime(time, "%d %b %Y %X")

    def evaluate(self, story: NewsStory) -> bool:
        raise NotImplementedError


# Problem 6
class BeforeTrigger(TimeTrigger):

    def evaluate(self, story: NewsStory) -> bool:
        return self.time < story.get_pubdate()

    def __str__(self) -> str:
        return "(" + "Before " + self.time.__str__() + ")"


class AfterTrigger(TimeTrigger):

    def evaluate(self, story: NewsStory) -> bool:
        return story.get_pubdate() < self.time

    def __str__(self) -> str:
        return "(" + "After " + self.time.__str__() + ")"


# COMPOSITE TRIGGERS

# Problem 7
class NotTrigger(Trigger):

    def __init__(self, trigger: Trigger):
        self.trigger = trigger

    def evaluate(self, story: NewsStory) -> bool:
        return not self.trigger.evaluate(story)

    def __str__(self) -> str:
        return "(" + "NOT " + self.trigger.__str__() + ")"


# Problem 8
class AndTrigger(Trigger):

    def __init__(self, left_trigger: Trigger, right_trigger: Trigger):
        self.left_trigger = left_trigger
        self.right_trigger = right_trigger

    def evaluate(self, story: NewsStory) -> bool:
        return self.left_trigger.evaluate(story) and self.right_trigger.evaluate(story)

    def __str__(self) -> str:
        return "(" + self.left_trigger.__str__() + " OR " + self.right_trigger.__str__() + ")"


# Problem 9
class OrTrigger(Trigger):

    def __init__(self, left_trigger: Trigger, right_trigger: Trigger):
        self.left_trigger = left_trigger
        self.right_trigger = right_trigger

    def evaluate(self, story: NewsStory) -> bool:
        return self.left_trigger.evaluate(story) or self.right_trigger.evaluate(story)

    def __str__(self) -> str:
        return "(" + self.left_trigger.__str__() + " AND " + self.right_trigger.__str__() + ")"


# ======================
# Filtering
# ======================

# Problem 10
def filter_stories(stories: list, triggerlist: list) -> list:
    """
    Takes in a list of NewsStory instances.

    Returns: a list of only the stories for which a trigger in triggerlist fires.
    """
    # TODO: Problem 10
    result = []
    for story in stories:
        for trigger in triggerlist:
            if trigger.evaluate(story):
                result.append(story)
                break
    return result


# ======================
# User-Specified Triggers
# ======================
# Problem 11
def read_trigger_config(filename: str) -> list:
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """

    def resolve(tokens: list) -> (str, Trigger):
        name = tokens[0]
        type_ = tokens[1]
        t = None
        if type_ == "AND":
            t = AndTrigger(triggers[tokens[2]], triggers[tokens[3]])
        elif type_ == "OR":
            t = OrTrigger(triggers[tokens[2]], triggers[tokens[3]])
        elif type_ == "NOT":
            t = NotTrigger(triggers[tokens[2]])
        elif type_ == "TITLE":
            t = TitleTrigger(tokens[2])
        elif type_ == "DESCRIPTION":
            t = DescriptionTrigger(tokens[2])
        elif type_ == "BEFORE":
            t = BeforeTrigger(tokens[2])
        elif type_ == "AFTER":
            t = AfterTrigger(tokens[2])
        return name, t

    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    result = []
    with open(filename, 'r') as trigger_file:
        triggers = {}
        for line in trigger_file:
            line = line.rstrip()
            if not (len(line) == 0 or line.startswith('//')):
                tokens = line.split(",")
                if tokens[0] == "ADD":
                    # ADD named triggers
                    for i in range(1, len(tokens)):
                        result.append(triggers[tokens[i]])
                else:
                    # Trigger definition
                    (name, trigger) = resolve(tokens)
                    triggers[name] = trigger

    return result


SLEEPTIME = 120  # seconds -- how often we poll


def main_thread(master):
    # A sample trigger list - you might need to change the phrases to correspond
    # to what is currently in the news
    try:
        # t1 = TitleTrigger("Trump")
        # t2 = DescriptionTrigger("Trump")
        # triggerlist = [OrTrigger(t1, t2)]

        # Problem 11
        # TODO: After implementing read_trigger_config, uncomment this line 
        triggerlist = read_trigger_config('triggers.txt')
        [print(t) for t in triggerlist]
        # HELPER CODE - you don't need to understand this!
        # Draws the popup window that displays the filtered stories
        # Retrieves and filters the stories from the RSS feeds
        frame = Frame(master)
        frame.pack(side=BOTTOM)
        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT, fill=Y)

        t = "Google & Yahoo Top News"
        title = StringVar()
        title.set(t)
        ttl = Label(master, textvariable=title, font=("Helvetica", 18))
        ttl.pack(side=TOP)
        cont = Text(master, font=("Helvetica", 14), yscrollcommand=scrollbar.set)
        cont.pack(side=BOTTOM)
        cont.tag_config("title", justify='center')
        button = Button(frame, text="Exit", command=root.destroy)
        button.pack(side=BOTTOM)
        guidShown = []

        def get_cont(newstory: NewsStory):
            if newstory.get_guid() not in guidShown:
                cont.insert(END, newstory.get_title() + "\n", "title")
                cont.insert(END, "\n---------------------------------------------------------------\n", "title")
                cont.insert(END, newstory.get_description())
                cont.insert(END, "\n*********************************************************************\n", "title")
                guidShown.append(newstory.get_guid())

        while True:
            print("Polling . . .", end=' ')
            # Get stories from Google's Top Stories RSS news feed
            stories = process("http://news.google.com/news?output=rss")

            # Get stories from Yahoo's Top Stories RSS news feed
            stories.extend(process("http://news.yahoo.com/rss/topstories"))

            stories = filter_stories(stories, triggerlist)

            list(map(get_cont, stories))
            scrollbar.config(command=cont.yview)

            print("Sleeping...")
            time.sleep(SLEEPTIME)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    root = Tk()
    root.title("Some RSS parser")
    t = threading.Thread(target=main_thread, args=(root,))
    t.start()
    root.mainloop()
