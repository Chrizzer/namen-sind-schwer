# Numbers know no bounds in Python3
# Integers
int_value_small = -1
int_value_zero = 0
int_value_big = 1
int_values = [int_value_small, int_value_zero, int_value_big]
# Floats
float_value_small = -1.0
float_value_zero = 0.0
float_value_big = 1.0
float_values = [float_value_small, float_value_zero, float_value_big]
# Strings
str_value_empty = ""
str_value_short = "short"
str_value_long = "something"
str_values = [str_value_empty, str_value_short, str_value_long]
# Booleans
true_value = True
false_value = False
bool_values = [true_value, false_value]
