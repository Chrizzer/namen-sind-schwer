s = "abc"
print("length")
print(f"len(\"{s}\") = {len(s)}")

print("Indices")
# last index is always at -1
for i in [0, -1, len(s) - 1]:
    print(f"\"{s}\"[{i}] = {s[i]}:{type(s[i])}")

print("Iterating over Strings")
for character in s:
    print(character)

for index, character in enumerate(s):
    print(f"{character} at {index}")

print("Slicing [start:stop:step]")
start = 0
stop = -1
step = 2

str = "asdghjköjlhgfgdgfhghj"

print(f"str[::] == str[0:len(str):1] = {str[::] == str[0:len(str):1]}")
print(f"str[::-1] == str[-1:-(len(str) + 1):-1] = {str[::-1] == str[-1:-(len(str) + 1):-1]}")

print("Strings are immutable")

s = "hello"
print(f"{s} - {id(s)}")
# s[0] = "y" TypeError: 'str' object does not support item assignment
s = "y"+s[1:]
print(f"{s} - {id(s)}")
