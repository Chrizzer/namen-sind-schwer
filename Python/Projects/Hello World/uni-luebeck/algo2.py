def efif(X, k):
    C = []  # C = Cache
    M = {}  # M = Occurences of all elements
    I = {}  # I = Visited map for constant lookup in M

    n = len(X)

    for i in range(n):
        element = X[i]
        if element in M:
            M[element].append(i)
        else:
            M[element] = [i]
            I[element] = 0

    for i in range(n):
        element = X[i]
        I[element] += 1
        if element not in C:
            if k <= len(C):
                # Evict-Furthest-In-Future
                furthest = C[0]
                for j in range(1, k):
                    cached = C[j]
                    A = M[furthest]
                    B = M[cached]
                    a = A[(I[furthest])] if I[furthest] < len(A) else n + 1
                    b = B[(I[cached])] if I[cached] < len(B) else n + 1
                    if a < b:
                        furthest = cached
                evict(C, furthest)
            load(C, element)
        else:
            print(f'Cache Hit  {element}')


def load(cache, x):
    cache.append(x)
    print(f'Load {x}  -> {cache}')


def evict(cache, x):
    print(f'Evict {x}', end=", ")
    cache.remove(x)


efif(['a', 'b', 'c', 'd', 'a', 'd', 'e', 'a', 'd', 'b', 'c'], 3)
#      a    a    a    a    a    a    a    a    a    b    c
#           b    b    b    b    b    e    e    e    e    e
#                c    d    d    d    d    d    d    d    d
#
#                     c              b              a    b
