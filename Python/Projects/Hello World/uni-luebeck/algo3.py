d = 4
P = (4, -4, -4, 12, 4)


# A -(4)-> B -(-4)-> C -(-4)-> D -(19)-> E -(5)-> F

def solve1(d, P):
    '''returns indices of T.S.B stops'''
    S = []
    accu = d
    most_recent_hill = -1
    i = 0
    while i < len(P):
        # if we can reach the next point with accu do that
        w = P[i]

        # consider most_recent_hill as resting stop
        if w <= 0 and most_recent_hill == -1:
            most_recent_hill = i

        # accu is not sufficiently loaded for this hill
        if accu < w:
            # recharge accu at a shop
            if w <= accu + d and most_recent_hill != -1:
                # rest at most recent hill
                S.append(most_recent_hill)
                accu += d
                most_recent_hill = -1
                continue
            else:
                S.append(i)
                accu = d
        else:
            accu -= w
        i += 1
    return S


def solve123(d, P):
    '''returns indices of T.S.B stops'''
    S = []
    L = len(P)
    # Assuming there is a subset S of P that is valid
    i = L - 1
    accu = d
    # given that there is valid solution for P
    while 0 <= i:
        # w is the point we need to surpass
        w = P[i]
        # if accu is sufficiently loaded then
        if w <= accu:
            # yes we can pass
            accu -= w
            i -= 1
        else:
            # otherwise we need to stop at j <= i
            j = max(0, i - 1)
            e = 0
            while 0 <= j and accu - e < w:
                # Given the existence of a valid solution for P
                # P_j may only be 0 or negative
                e -= P[j]
                j -= 1
            # rest at P_j
            print(f'resting at P({j + 1})')
            S.append(j + 1)
            i = j
            accu = d
    return S


def solve(d, P):
    S = []  # Stops
    # Reduktion des Problems
    i = 0
    n = len(P)
    R = []
    while i < n:
        edge = P[i]
        if edge < 0:
            j = i + 1
            negative_weights = edge
            while negative_weights <= 0 and j < n:
                negative_weights += P[j]
                j += 1
            # collapse edge from i to j
            R.append(negative_weights)
            i = j
        else:
            R.append(edge)
            i += 1
    # Greedy-Algorithm
    accu = d
    i = 0
    while i < len(R):
        if R[i] <= accu:
            accu -= R[i]
            i += 1
        else:  # rest
            accu = d
            S.append(i)
    return S


print(solve(10, (5, 5, -9, 5, 5, 6, -4, -4)))
