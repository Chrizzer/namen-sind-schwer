def greatest_common_subsequence(X: str, Y: str) -> str:
    Sigma: set = set(X + Y)
    if len(X) == 0 or len(Y) == 0:
        return ""
    best_a: int = max(len(Y) + 1, len(X) + 1)
    best_b: int = max(len(Y) + 1, len(X) + 1)
    best_symbol: str = ""
    for symbol in Sigma:  # Für jeden Buchstaben s in Sigma
        # Finde das erste Vorkommen von s in X,Y
        # an den Positionen a(s) für X und b(s) für Y
        a = X.index(symbol) if symbol in X else max(len(Y) + 1, len(X) + 1)
        b = Y.index(symbol) if symbol in Y else max(len(Y) + 1, len(X) + 1)
        # Wähle ein s für das a(s)+b(s) minimal ist
        if abs(a + b) < abs(best_a + best_b):
            best_a = a
            best_b = b
            best_symbol = symbol
    # Löse das Problem rekursiv für die Suffixe beginnend an Positionen
    # a(s)+1 und b(s)+1 bis einer der Suffixe der leere String ist
    rest: str = greatest_common_subsequence(X[best_a + 1::], Y[best_b + 1::])
    return best_symbol + rest


def longest_palindrom_recursive(X: str) -> str:
    n = len(X)

    if n <= 1:
        # if |X| = 1 then X is a palindrome
        return X
    else:
        # if |X| = 2 and xy = X and x != y then x and y are palindromes
        first, last = X[0], X[n - 1]
        if first == last:
            # if xyz = X and x=z then x and z are part of solution
            return first + longest_palindrom(X[1:n - 1]) + last
        else:
            left: str = longest_palindrom(X[0:n - 1])
            right: str = longest_palindrom(X[1:n])
            return left if len(right) < len(left) else right


def is_palindrome(X: str) -> bool:
    """:returns if X is a palindrome"""
    # X is a palindrome if and only if
    #   |X| == 1 or
    #   if xyz = X then x == z (y may be empty)
    return len(X) >= 1 and X[0] == X[-1]


def longest_of(left: str, right: str) -> str:
    """:returns the longest of two string"""
    return left if len(right) < len(left) else right


def longest_palindrom(X: str) -> str:
    """:returns longest sub-palindrom in X"""
    n: int = len(X)
    # solution lookup map
    sol: dict = {symbol: symbol for symbol in X}
    sol[""] = ""  # to handle edge cases for 2-sequences
    # search for solutions bottom up
    for length in range(2, n + 1):
        for i in range(0, n - length + 1):
            Z: str = X[i:i + length]
            if is_palindrome(Z):
                sol[Z] = sol[(Z[0])] \
                         + sol[(Z[1:length - 1])] \
                         + sol[(Z[-1])]
            else:  # Z is not a palindrome
                left = Z[0:length - 1]
                right = Z[1:length]
                # Solution for Z is equal to max(left, right) where
                sol[Z] = longest_of(sol[left], sol[right])
    return sol[X]


def lev(s: str, t: str) -> (int, list):
    c1 = 2  # Insert, Delete
    c2 = 3  # Replace
    n, m = len(s), len(t)
    M = [[0 for _ in range(m + 1)] for __ in range(n + 1)]
    # Utility function to assign value
    f = lambda a, b: 0 if a == b else c2
    for i in range(n + 1):
        M[i][0] = i * c1
    for j in range(m + 1):
        M[0][j] = j * c1

    path = []
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            x = M[i][j - 1]
            y = M[i - 1][j]
            z = M[i - 1][j - 1]
            c = f(s[i - 1], t[j - 1])
            M[i][j] = min(x + c1, y + c1, z + c)
    # print("\n".join(str(row) for row in M))
    # Backtracking to get operations
    i, j = n, m
    while 0 < i and 0 < j:
        # Choose direction
        # z y
        # x _
        x = M[i][j - 1]
        y = M[i - 1][j]
        z = M[i - 1][j - 1]
        direction = min(x, y, z)
        # Ties are resolved correctly because
        # if z and y are tied or z and x
        # then no replacement took place, because
        # x and y can only ever cost c1
        # whereas z always either costs 0 or c2 and c1 < c2
        if y == direction:
            # There was an insertion / deletion
            i -= 1  # go up
            path.append(f'Deletion {s[i-1]}->_')
        elif x == direction:
            j -= 1  # go left
            path.append(f'Insertion _->{t[j-1]}')
        else:
            # There was a replacement
            i -= 1
            j -= 1
            path.append(f'Replacement {s[i-1]}->{t[j-1]}' if f(s[i], t[j]) == c2 else f'Nothing {s[i-1]}')
    path.reverse()
    print("\n".join(str(row) for row in M))
    return M[n][m], path


# print(greatest_common_subsequence("traum", "tagtraum"))
# print(longest_palindrom("abbbbaottto"))
# print(lev("mergesort", "meerengen"))
print(lev("tor", "tier"))
print(lev("tier", "tor"))
#    -   m   e   e   r   e   n   g   e   n
# -  0,   ,   ,   ,   ,   ,   ,   ,   ,
# m   ,  0,   ,   ,   ,   ,   ,   ,   ,
# e   ,   ,  0,  2,   ,   ,   ,   ,   ,
# r   ,   ,   ,   ,  2,  4,  6,   ,   ,
# g   ,   ,   ,   ,   ,   ,   ,  6,   ,
# e   ,   ,   ,   ,   ,   ,   ,   ,  6,
# s   ,   ,   ,   ,   ,   ,   ,   ,  8,
# o   ,   ,   ,   ,   ,   ,   ,   , 10,
# r   ,   ,   ,   ,   ,   ,   ,   , 12,
# t   ,   ,   ,   ,   ,   ,   ,   ,   , 15
#    0   1   2   3   4   5   6   7   8   9
# 0  0,  2,  4,  6,  8, 10, 12, 14, 16, 18
# 1  2,  0,  2,  4,  6,  8, 10, 12, 14, 16
# 2  4,  2,  0,  2,  4,  6,  8, 10, 12, 14
# 3  6,  4,  2,  3,  2,  4,  6,  8, 10, 12
# 4  8,  6,  4,  5,  4,  5,  7,  6,  8, 10
# 5 10,  8,  6,  4,  6,  4,  6,  8,  6,  8
# 6 12, 10,  8,  6,  7,  6,  7,  9,  8,  9
# 7 14, 12, 10,  8,  9,  8,  9, 10, 10, 11
# 8 16, 14, 12, 10,  8, 10, 11, 12, 12, 13
# 9 18, 16, 14, 12, 10, 11, 13, 14, 14, 15
