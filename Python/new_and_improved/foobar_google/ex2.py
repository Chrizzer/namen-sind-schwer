left, right = -1, 1
up, down = -1, 1

board = [[i + (j * 8) for i in range(8)] for j in range(8)]
cache = {}


def neighbours(square):
    """ yields all possible adjacent squares"""
    global left, right, up, down
    if square in cache:
        return cache[square]
    x, y = square % 8, int(square / 8)
    adjacent = set()
    if 0 <= x + left and 0 <= y + up + up:
        adjacent.add(board[y + up + up][x + left])
    if x + right < 8 and 0 <= y + up + up:
        adjacent.add(board[y + up + up][x + right])
    if x + right + right < 8 and 0 <= y + up:
        adjacent.add(board[y + up][x + right + right])
    if x + right + right < 8 and y + down < 8:
        adjacent.add(board[y + down][x + right + right])
    if x + right < 8 and y + down + down < 8:
        adjacent.add(board[y + down + down][x + right])
    if 0 <= x + left and y + down + down < 8:
        adjacent.add(board[y + down + down][x + left])
    if 0 <= x + left + left and y + down < 8:
        adjacent.add(board[y + down][x + left + left])
    if 0 <= x + left + left and 0 <= y + up:
        adjacent.add(board[y + up][x + left + left])

    cache[square] = adjacent
    return adjacent


iterations = 0


def expand(vals):
    global iterations
    iterations = iterations + 1
    expansion = set()
    for v in vals:
        for node in neighbours(v):
            expansion.add(node)
    return set.union(vals, expansion)


def solution(src, dest):
    if src == dest:
        return 0
    # iterative deepening approach
    # solution must be within each nodes neighbours neighbours
    global iterations
    iterations = 0

    xs = expand(set([src]))
    if dest in xs:
        return 1
    ys = expand(set([dest]))
    if src in ys:
        return 1

    while set.isdisjoint(xs, ys):
        # expand sets
        xs = expand(xs)

    return iterations


# 0 17 27 33 50 56
# M[27][56] = 5 muss aber 3 sein...
assert solution(50, 56) == 1, f"Actual = {solution(50, 56)}"
assert solution(33, 56) == 2, f"Actual = {solution(33, 56)}"
assert solution(27, 56) == 3, f"Actual = {solution(27, 56)}"
assert solution(43, 56) == 3, f"Actual = {solution(43, 56)}"
assert solution(0, 56) == 5, f"Actual = {solution(0, 56)}"
assert solution(0, 2) == 2, f"Actual = {solution(0, 2)}"
assert solution(0, 63) == 6, f"Actual = {solution(0, 63)}"
assert solution(0, 1) == 3, f"Actual = {solution(0, 1)}"
assert solution(19, 36) == 1, f"Actual = {solution(19, 36)}"
assert solution(0, 0) == 0, f"Actual = {solution(0, 0)}"
assert solution(0, 7) == 5, f"Actual = {solution(0, 7)}"
print(neighbours(0))
print(solution(7, 0))
print(solution(63, 56))
# assert solution(7, 0) == 5
for i in range(63):
    for j in range(63):
        assert solution(i, j) == solution(j, i), f"{i},{j} != {j},{i}"
