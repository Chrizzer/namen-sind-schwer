"""
Fuel Injection Perfection
=========================

Commander Lambda has asked for your help to refine the automatic quantum antimatter fuel injection system for her LAMBCHOP doomsday device. It's a great chance for you to get a closer look at the LAMBCHOP - and maybe sneak in a bit of sabotage while you're at it - so you took the job gladly. 

Quantum antimatter fuel comes in small pellets, which is convenient since the many moving parts of the LAMBCHOP each need to be fed fuel one pellet at a time. However, minions dump pellets in bulk into the fuel intake. You need to figure out the most efficient way to sort and shift the pellets down to a single pellet at a time. 

The fuel control mechanisms have three operations: 

1) Add one fuel pellet
2) Remove one fuel pellet
3) Divide the entire group of fuel pellets by 2 (due to the destructive energy released when a quantum antimatter pellet is cut in half, the safety controls will only allow this to happen if there is an even number of pellets)

Write a function called solution(n) which takes a positive integer as a string and returns the minimum number of operations needed to transform the number of pellets to 1. The fuel intake control panel can only display a number up to 309 digits long, so there won't ever be more pellets than you can express in that many digits.

For example:
solution(4) returns 2: 4 -> 2 -> 1
solution(15) returns 5: 15 -> 16 -> 8 -> 4 -> 2 -> 1
"""


def solution(n):
    """
      returns an integer representing the number of operations needed to transform 
      the input n (positive integer as string) 
    """
    number = int(n)
    num_operations = 0

    while number != 1:
      if number % 2 == 0 and number != 0:
        # divide if even
        # this is the fastest operation to shrink to 1
        number = number / 2
      elif (number == 3) or (number % 4 == 1):
        # subtract if 3 or greater than multiples of 4
        number = number - 1
      else:
        # otherwise increment
        number = number + 1
      num_operations = num_operations + 1

    return num_operations

solution("9"*309)
assert solution('15') == 5, solution('15')
assert solution('4') == 2, solution('4')
assert solution('0') == 1, solution('0')
assert solution('8') == 3, solution('8')
assert solution('10') == 4, solution('10')
assert solution('1') == 0, solution('1')
assert solution('15') == 5, solution('15')
assert solution('4') == 2, solution('4')
assert solution('3') == 2, solution('3')
assert solution('2') == 1, solution('2')
assert solution('6') == 3, solution('6')
assert solution('7') == 4, solution('7')
assert solution('1024') == 10, solution('1024')
assert solution('1025') == 11, solution('1025')
assert solution('1026') == 11, solution('1026')
assert solution('1027') == 12, solution('1027')
assert solution('768') == 10, solution('768')
