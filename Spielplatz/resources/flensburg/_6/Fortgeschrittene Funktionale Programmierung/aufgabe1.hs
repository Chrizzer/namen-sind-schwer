ggT::Integer->Integer->Integer

ggT a b | b == 0 = a
        | a == 0 = b
        | a > b  = ggT (a-b) b
        | otherwise = ggT a (b-a)

kgV::Integer->Integer->Integer

kgV a b = (a * b) `div` (ggT a b)

fib::Integer->Integer

fib 0 = 1
fib 1 = 1
fib n = fib (n-2) + fib (n-1)