fib_iter 0 = 0
fib_iter 1 = 1
fib_iter n = f n 0 1
    where f 0 cur _ = cur
          f n last cur = f (n-1) (cur) (last+cur)

f_iter n | n > 1 = (n^2) + f_iter (n-1)
         | otherwise = n

filter' _ [] = []
filter' p list = foldr (\x acc -> if p x then x : acc else acc) [] list

list = [1..10]

binom _ 0 = 1
binom 0 _ = 0
binom n k | n >= k = fac n `div` (fac k * fac (n-k))
          | otherwise = error "! n >= k"
    where fac = factorial

factorial 0 = 1
factorial 1 = 1
factorial n = n * factorial (n-1)

fibList n = [fib_iter x | x <- [1..n]]

sieb n = f [2..n]
    where f [] = []
          f (x:xs) = x : f (filter (\n ->if (n `mod` x == 0) then False else True) xs)

data Tree = Leaf Int | Node Int [Tree]

contains (Leaf v) x = v==x
contains (Node v (t:ts)) x = v==x || contains t x

tree = Node 1 [Node 2 [Leaf 3]]
binaryTree = BinaryNode 1 (BinaryNode 2 (BinaryNode 3 Empty Empty) (BinaryNode 4 Empty Empty)) (BinaryNode 5 Empty Empty)
list' = [3,2,4]

data BinaryTree = Empty | BinaryNode Integer (BinaryTree) (BinaryTree)

instance Show BinaryTree where
    show Empty = ""
    show (BinaryNode x l r) = " Node(" ++ show x ++ show l ++ show r ++ ")"

instance Num BinaryTree where
    Empty + Empty = Empty
    (BinaryNode x l r) + Empty = BinaryNode x l r
    Empty + (BinaryNode x l r) = BinaryNode x l r
    (BinaryNode x al ar) + (BinaryNode b bl br) = BinaryNode (x + b) (al + bl) (ar + br)

contains' Empty _ = False
contains' (BinaryNode v l r) x = v==x || contains' l x || contains' r x

toList Empty = []
toList (BinaryNode v l r) = toList l ++ [v] ++ toList r

toTree [] = Empty
toTree (x:xs) = BinaryNode x left right
    where left = toTree (filter (<=x) xs)
          right = toTree (filter (>x) xs)

treeTest = toTree [45,34,52,6423,76432,2,536,7,1,2]
stackTest = Stack [1,2,3,4]

data Stack s = EmptyStack | Stack [s]

instance (Show s) => Show (Stack s) where
    show EmptyStack = "Empty"
    show (Stack []) = ""
    show (Stack (x:xs)) = show x ++ " " ++ show (Stack xs)

instance (Eq a) => Eq (Stack a) where
    (==) EmptyStack EmptyStack = True
    (==) (Stack (x:xs)) (Stack (y:ys)) = x==y && xs==ys 
    (==) _ _ = False

data Baum = Blatt Int | Knoten Int [Baum] deriving Show

data Liste = Leer | Num Int Liste deriving Show

count (Blatt _) = 1
count (Knoten _ []) = 1
count (Knoten _ bs) = 1 + count' bs
    where count' [] = 0
          count' (x:xs) = count x + count' xs

baum = Knoten 1 [Knoten 2 [Blatt 3], Knoten 4 [Blatt 5], Knoten 6 []]
