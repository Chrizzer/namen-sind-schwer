-- Aufgabe 1 : Aufgabe 1 Mittelwert aller Zahlen
summe von bis = 
	if von < bis
	then von + summe (von+1) bis
	else von

anzahl von bis =
	if von < bis
	then 1 + anzahl (von+1) bis
	else 1

durchschnitt von bis = fromIntegral((summe von bis)) / fromIntegral((anzahl von bis))

bonbon geld preis mPreis 
    | preis <= geld && preis <= mPreis = 1 + bonbon (geld-preis) (preis+10) mPreis
    | otherwise = 0
