data BinaryTree a = Nil | Node a (BinaryTree a) (BinaryTree a)

instance (Show a, Num a) => Show (BinaryTree a) where 
    show Nil = " NIL"
    show (Node x l r) = "(" ++ show x ++ show l ++ show r ++ ")"

instance (Num a) => Num (BinaryTree a) where
    Nil + Nil = Nil
    (Node x l r) + Nil = Node x l r
    Nil + (Node x l r) = Node x l r
    (Node x al ar) + (Node b bl br) = Node (x + b) (al + bl) (ar + br)

contains::Eq a => BinaryTree a->a->Bool
contains Nil _ = False
contains (Node v l r) x = if v == x then True else contains l x || contains r x

sum'::(Num a)=>BinaryTree a->a
sum' Nil = 0 
sum' (Node x l r) = x + (sum' l) + (sum' r)

toTree::Ord a => [a]->BinaryTree a
toTree [] = Nil
toTree (x:xs) = Node x (toTree (filter' (<x) xs)) (toTree (filter' (>x) xs))

filter' :: (a -> Bool) -> [a] -> [a]
filter' p list = foldr (\x acc -> if p x then x : acc else acc) [] list

-- + bei ungleichen Baumen jede Node adden oder einen neuen Baum machen mit Root und der kompletten Summe
-- übliche Operationen push pop top stack
-- add remove 
tree = toTree [45,34,52,6423,76432,2,536,7,1,2]

data Stack s = Empty | Stack [s] deriving Eq

instance (Show s) => Show (Stack s) where
    show Empty = "Empty"
    show (Stack []) = ""
    show (Stack (x:xs)) = show x ++ show (Stack xs)

empty::(Eq a) => Stack a -> Bool
empty x = x == Empty

pop::Stack a -> Stack a
pop (Stack []) = Empty
pop (Stack (x:xs)) = Stack xs

push::a -> Stack a -> Stack a
push x (Stack xs) = Stack (x:xs)

data Queue q = Leer | Queue [q] deriving Eq

instance (Show s) => Show (Queue s) where
    show Leer = "Leer"
    show (Queue []) = ""
    show (Queue (x:xs)) = show x ++ " " ++ show (Queue xs)

enqueue::a -> Queue a -> Queue a
-- Unfug hier fehlt noch enqueue x Leer = Queue [x] #Klausur oben bei Stack fehlt auf der Case...
enqueue x (Queue xs) = Queue (xs ++ [x])

dequeue::Queue a -> Queue a
dequeue (Queue (x:xs)) = Queue (xs)

test = Queue [1,2,3,4,5]