{-
    Welchen Typen haben die folgenden Ausdrücke?
        map(\x -> x * x)
            Ich      -> map::(a->b)->[a]->[b]
        Partielle Applikation ? Currying?
        ('a':[],'c':[]):[]
            Ich      -> :: (char, [a])
            Compiler -> :: [([Char], [Char])]
    
    Welchen Wert haben die folgenden Ausdrücke?
        ausdruck1 = let f = \x y -> mod x y in f 2 3                  =   2
        ausdruck2 = (\xs -> [x | x <- xs, 2 * x + 1 < 10]) [10,9,..1] = [4,3,2,1]
-}
data Baum = Blatt Int | Knoten Int [Baum] deriving Show

data Liste = Leer | Num Int Liste deriving Show

count::Baum->Int
count (Knoten _ []) = 1
count (Blatt _) = 1
count (Knoten n (x:xs)) = 1 + count x

baum = Knoten 1 [Knoten 2 [Blatt 3], Knoten 4 [Blatt 5]]

toList::Baum->Liste
toList (Blatt n) = Num n Leer
toList (Knoten n xs) = Num n (f xs)
    where f::[Baum]->Liste
          f [] = Leer
          f ((Blatt x):xs) = Num x (f xs)
          f ((Knoten y ys) : xs) = Num y (f (ys++xs))

instance Num (Baum) where
    (Blatt x) + (Blatt y) = Blatt (x+y)
    (Knoten x xs) + (Blatt y) = Knoten (x+y) xs
    (Blatt x) + (Knoten y ys) = Knoten (x+y) ys
    (Knoten x xs) + (Knoten y ys) = Knoten (x+y) ([x+y | x <- xs,y<-ys])