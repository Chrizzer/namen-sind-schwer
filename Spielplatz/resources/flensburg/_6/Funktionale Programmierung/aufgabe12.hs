data Exp = Num Float
         | Add Exp Exp
         | Sub Exp Exp
         | Mult Exp Exp
         | Div Exp Exp deriving (Eq,Show,Read)

-- Add Num 1.5 (Mult Num 3 Num 2) = 7.5
-- x ist eine Expression und calc Expression liefert einen Float zurück
-- y ^
-- auf Floats kann

calc::String->Float
-- mit string.words parsen und input verwalten
-- Bei der Liste die Länge überprüfen
calc str = calc' $ eval (words str)
    where eval::[String]->Exp
          eval (x:xs)
           | x == "Add" = Add (eval xs)
           | x == "Sub" = Sub (eval xs)
           | x == "Mult" = Mult (eval xs)
           | x == "Div" = Div (eval xs)
           | x == "Num" = Num (read (head xs)::Float)
           | otherwise = read x::Float

              -- die nächste Zahl speichern

calc'::Exp->Float
calc' (Num x) = x
calc' (Add x y) = calc' x + calc' y
calc' (Sub x y) = calc' x - calc' y
calc' (Mult x y) = calc' x * calc' y
calc' (Div x y) = calc' x / calc' y