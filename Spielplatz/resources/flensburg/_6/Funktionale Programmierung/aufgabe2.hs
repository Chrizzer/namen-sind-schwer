-- Aufgabe 2 : Aufgabe 1 Funktionstypen
summe::(Num a, Ord a)=>a->a->a
summe von bis
    | von < bis = von + summe (von+1) bis
    | otherwise = von

anzahl::(Num a, Ord a)=>a->a->Int
anzahl von bis
    | von < bis = 1 + anzahl(von+1) bis
	| otherwise = 1

durchschnitt::(Ord a, Fractional a)=>a->a->a
durchschnitt von bis = (summe von bis) / fromIntegral(anzahl von bis)

bonbon:: (Num a, Ord a)=>a->a->a->Int
bonbon geld preis mPreis
    | preis <= geld && preis <= mPreis = 1 + bonbon (geld-preis) (preis+10) mPreis
    | otherwise = 0

{-
Aufgabe 2 Welchen Typ haben die folgenden Ausdrücke?
[(1.3,'a')] :: Fractional t => [(t, Char)] Tupel
[] :: [t] Leere Liste
-}

-- Aufgabe 3 Stack Push pop top und istLeer

istLeer::[a]->Bool
istLeer stack
    | length stack == 0 = True
	| otherwise = False

pop::[a]->[a]
pop stack
	| istLeer stack = error "Leerer Stack"
	| otherwise = take ((length stack)-1) stack

top::[a]->a
top stack
	| istLeer stack = error "Leerer Stack"
	| otherwise = last stack

push:: a->[a]->[a]
push item stack = [item] ++ stack

-- Aufgabe 4 n bis m Elemente aus einer Liste

unterListe::Int->Int->[a]->[a]
unterListe n m liste
  |istLeer liste = error "Leere Liste"
  |n > m = error "n muss kleiner als m sein"
  |n > length liste || m > length liste = error "n oder m war nicht größer als die liste"
  |otherwise = take m (drop (n-1) liste)

  -- Aufgabe 5 :fibonacci
  -- 1 2 3 5 8 13 21 34 55
fibonacci:: Int->[Int]
fibonacci n
  |n > 1 = fibonacci (n-1) + fibonacci (n-2)
