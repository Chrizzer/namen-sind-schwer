f::Integer->Integer
f n
 | n > 0 = (n*n) + f (n-1)
 | otherwise = 0

fib::Integer->Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

-- 3 b und c nochmal nachfragen was rekursiver prozess und iterativer prozess sein sollen
-- Nur ein rekursiver Aufruf für eine von den Aufgaben

pascal::Int->Int->Int
-- Jede Spalte ist Zeile+1 lang
-- f 4 2 = 6
-- 1. [4 2] => [3 1] + [3 3]
-- 2. [3 1] => [2 0] + [2 2] && [3 3] => 1
-- 3. [2 0] => 1 && [2 2] => 1
pascal zeile spalte
    | spalte == 0 = 1
    | zeile == spalte = 1
    | otherwise = pascal (zeile-1) (spalte-1) + pascal (zeile-1) spalte

dreieck::Int->[[Int]]
dreieck zeile = [[pascal y x | x <- [0..y]] | y <- [0..zeile]]
{-
dreieck zeile
    | 1 <= zeile = dreieck zeile-1 : createZeile zeile 0
    | otherwise = createZeile zeile 0
   where createZeile zeile spalte = if spalte <= zeile then
                                                        pascal zeile spalte : createZeile zeile (spalte+1)
                                                       else
                                                        [pascal zeile spalte]
-}
