-- Aufgabe 1
-- :t (<)
-- (<):: Ord a => a->a->Bool
-- :t (++[1..10])
-- (++[1..10])::(Enum a, Num a) => [a] -> [a]
-- f x = (\x->x +1) x
-- f :: Num a => a->a
-- Aufgabe 2
potenz::Integer->Integer->Integer
potenz 0 _ = 1
potenz n basis = basis * potenz (n-1) basis

quadrat::Integer->Integer
quadrat = potenz 2
kubik::Integer->Integer
kubik = potenz 3

--Aufgabe 3
filterListe predicate list = foldr nestedFunction [] list
    where nestedFunction currentElement result = if predicate currentElement then currentElement : result else result
-- Aufgabe 4

pascal zeile spalte
    | spalte == 0 = 1
    | zeile == spalte = 1
    | otherwise = pascal (zeile-1) (spalte-1) + pascal (zeile-1) spalte

factorial 0 = 1
factorial n = factorial (n-1) * n
factorial n zwischenergebnis = factorial
-- rekursiver prozess ^
-- iterative Prozesse müssen zwischenergebnisse übergeben und keinen rekursiven Aufruf auf der rechten Seite haben

binom n 0 = 1
binom 0 k = 0
binom n k = if k <= n then factorial n `div` (factorial k * factorial (n-k)) else error "!(K <= N)"

-- Den Unterschied von iterativen und rekursiven Prozessen nochmal nachfragen...
