{-
Aufgabe 1:
:t foldr (-)    :: (Num a, Foldable t) => b -> t b -> b
:t map (==2)    :: (Num a, Eq a) => [a] -> [Bool]
:t (+2)         :: Num a => a -> a
-}
import Data.Char

sumFib n m
    | m < 4000000 = sumFib m (n+m)
    | otherwise = n
{-
    import Data.List (insert)
    insertionSort :: Ord a => [a] -> [a]
    insertionSort = foldr insert []
-}
{-  [ 3,7,4,9,5,2,6,1 ]
    [3] 7 [4,9,5,2,6,1]
    [3,7] 4 [9,5,2,6,1]
    [3,4,7] 9 [5,2,6,1]
    [3,4,7,9] 5 [2,6,1]
    [3,4,5,7,9] 2 [6,1]
    [2,3,4,5,7,9] 6 [1]
    [2,3,4,5,6,7,9] 1[]
    [ 1,2,3,4,5,6,7,9 ]
    length sortedAfterElement = (length sorted)-i
    length sortedBeforeElement = i
-}
-- Nachfragen wo der Fehler liegt wtf...
insertionSort (x:xs) = f [x] (tail xs) (head xs) 1
    where f sorted [] _ _ = sorted
          f sorted unsorted e i
            |i == 0 = f (e:sorted) (rest unsorted) (next unsorted) (length sorted)
            |e < sorted !! (i-1) = f sorted unsorted e (i-1)
            |otherwise = f ((pre i) ++ e : (post i)) (rest unsorted) (next unsorted) (length sorted)
            where pre i = take i sorted
                  post i = take (length sorted - i) (drop i sorted)
                  rest list = tail list
                  next [] = 0
                  next list = head list

f' n = map digitToInt (show n)
