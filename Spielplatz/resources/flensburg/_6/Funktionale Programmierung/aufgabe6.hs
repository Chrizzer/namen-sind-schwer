{-
    f1 :: Num t1 => [t1] -> t
    f2 :: (t -> t) -> t
    Compiler sagt
    f2 :: t -> t
    g1 :: Num a => [a] -> a -> t -> a
    g2 :: Num a => [a] -> a -> a -> a
-}
filter' :: (a -> Bool) -> [a] -> [a]
filter' p list = foldr (\x acc -> if p x then x : acc else acc) [] list
-- filter' p = foldr (\x acc -> if p x then x : acc else acc) []

map' :: (a -> b) -> [a] -> [b]
map' f list = foldr (\x acc -> f x : acc) [] list
-- map' f = foldr (\x acc -> f x : acc) []

concat' :: [[a]] -> [a]
concat' list = foldr (++) [] list
-- concat' = foldr (++) []

filterWiederholungen :: Eq a => [a] -> [a]
filterWiederholungen = foldr (\x acc -> if elem x acc then acc else x : acc) []

-- wortListe :: String -> [(String,Int)]
-- wortListe str = foldr (\x acc -> (x , length $ filter (==x) acc) : acc) [("a",1)] (words str)
wortListe str = foldr (\x acc -> (x, length $ filter (==x) g) : acc) [] (filterWiederholungen g)
    where g = words str
-- Nachfragen warum das nicht so geht
-- Helferfunktion bauen die TupelListe zurückgibt
