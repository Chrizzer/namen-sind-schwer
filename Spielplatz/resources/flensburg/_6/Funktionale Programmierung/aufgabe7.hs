map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f list = [f x | x <- list]

filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' p (x:xs) = [x | x <- xs, p x]

-- auf Listenbeschreibungen umbauen
fibListe :: Int -> [Int]
fibListe n = 1 : 1 : f 0 1 1
    where f counter b c
            | counter < n = (b+c) : f (counter+1) c (b+c)
            | otherwise = []

sieb n = f [2..n]
    where f [] = []
          f (x:xs) = x : f (filter (\n ->if (n `mod` x == 0) then False else True) xs)
