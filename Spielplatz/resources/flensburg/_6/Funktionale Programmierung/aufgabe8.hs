list = [[x] | x<-[10,9..1]]
list' = [[x..x+z] | x<-[1..10], z<-[2,4..8], x==5]

add 0 m = m
add n m = inc (add (dec n) m)
{-
add 3 4 = inc (add (dec 3) 4)
        = (add (dec 3) 4) + 1
        = (add (dec 2) 4) + 1 + 1
        = (add (dec 1) 4) + 1 + 1 + 1
        = (add (dec 0) 4) + 1 + 1 + 1
        =        4        + 1 + 1 + 1
-}
add' 0 m = m
add' n m = add'(dec n) (inc m)
{-
add' 3 4 =  add' (dec 3) (inc 4) -> add' (2 - 1) (4 + 1)
         =  add' (dec 2) (inc 5) -> add' (2 - 1) (5 + 1)
         =  add' (dec 1) (inc 6) -> add' (1 - 1) (6 + 1)
         =  add'    0       7    -> 7
-}
inc n = n + 1
dec n = n - 1

-- Der Prozess add  ist rekursiv
-- Der Prozess add' ist iterativ

potenz basis 0 = 1
potenz basis n
    | mod n 2 == 0 = let x = potenz basis (div n 2) in (x*x)
    | otherwise = basis * potenz basis (n-1)
-- diese Lösung ist unfug #Klausurlernen
potenzIter basis 0 = 1
potenzIter basis n = potenzIter' basis n 1
potenzIter' _ 0 _ = 1
potenzIter' basis n acc = basis * potenzIter' basis (n-1) (acc*basis)
-- besser, acc muss immer mit 1 initialisiert werden!
potenzIter'' b 0 acc = acc
potenzIter'' b p acc = potenzIter'' b (p-1) (b*acc)