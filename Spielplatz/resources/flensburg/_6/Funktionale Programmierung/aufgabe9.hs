{-
    Binary Tree
        - Each Node can only have a left and a right child
        - Each Node can have either
            - LeftChild | RightChild
            - LeftChild | RightChild -> NULL
            - LeftChild -> Null | RightChild
            - No Childs -> NULL == Leaf
        - Height = Number of edges in longest path from root to a leaf
        - Height of empty tree = -1
        - Height of tree with one node = 0
        -> Convert to List -> Start left on each level and concatenate them
            !In a complete binary Tree!
            -> BinaryTreeToList ->
                Node at index i
                    -> left-child-index = 2i+1
                    -> right-child-index = 2i+2
    Tree
        - A Leaf Node has no childs
-}
-- data Tree a = Nil | Node (Tree a) a (Tree a) deriving Show
data Tree a = EmptyTree
            | Branch a [Tree a]
    deriving Show
-- Typvariable a muss mit spezifiziert werden!!

data BinaryTree a = Nil | Node a (BinaryTree a) (BinaryTree a) deriving Show

contains::Eq a => BinaryTree a->a->Bool
contains Nil _ = False
contains (Node v l r) x = if v == x then True else contains l x || contains r x

toList::BinaryTree a->[a]
toList Nil = []
toList (Node v l r) = toList l ++ v : toList r

toTree::Ord a => [a]->BinaryTree a
toTree [] = Nil
toTree (x:xs) = Node x (toTree (filter' (<x) xs)) (toTree (filter' (>x) xs))

filter' :: (a -> Bool) -> [a] -> [a]
filter' p list = foldr (\x acc -> if p x then x : acc else acc) [] list

-- test = contains (Node 0 (Node 1 (Node 3 Nil Nil) Nil) (Node 2 (Node 4 Nil Nil) Nil))
test' = toList (Node 0 (Node 1 (Node 3 Nil Nil) (Node 4 Nil Nil)) (Node 2 (Node 5 Nil Nil) (Node 6 Nil Nil)))
test'' = toTree [2,4,6,12,5,8]