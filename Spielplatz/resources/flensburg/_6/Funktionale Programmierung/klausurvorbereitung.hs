import Data.Char

splitComma::String->(String,String)
splitComma str = (takeWhile p str, tail $ dropWhile p str)
    where p x = x /= ','

count::String->Int
count [] = 0
count str = length $ filter (\x -> (isUpper x) || (isDigit x)) str

count'::String->Int
count' [] = 0
count' (x:xs) | isUpper x = 1 + count' xs
              | isDigit x = 1 + count' xs
              | otherwise = count' xs

isNext::Int->Int->Bool
isNext x y | x < 0 = False
           | even x = y == x `div` 2
           | odd x = y == 1 + x * 3

collatz::[Int]->Bool
collatz [] = True
collatz (x:[]) = True
collatz (x:y:xs) = isNext x y && collatz (y:xs)

f::Char->Int
f c | c `elem` "HASKELL" = 10
    | c `elem` "haskell" = 5
    | not $ isLetter c   = 0
    | isUpper c          = 2
    | otherwise          = 1

g::String->Int
g [] = 1
g (x:xs) | isLetter x = f x * g xs 
         | otherwise = g xs

c [] _ = []
c _ [] = []
c xs ys | length xs > length ys = [x | x <- ys, x `elem` xs]
        | otherwise = [x | x <- xs, x `elem` ys]

avg::Integer->Integer->Integer
avg lo hi = div (foldr (+) 0 [lo..hi]) (hi - lo + 1)

avg' lo hi = let l = [lo..hi] in div (sum l) (length l)

f1 xs ys = [(x,y)|x<-xs,y<-ys]

isPalindrom::Integer->Bool
isPalindrom x = show x == (reverse $ show x)

concat::[[a]]->[a]
concat [] = []
concat lists = [x|list<-lists, x<-list]

-- Warehouse
data Warehouse a = Corridor (Warehouse a) (Warehouse a)
                 | Shelf [a] deriving Show

data Goods = NoGoods | Box | Barrel deriving (Show, Eq)

buildWarehouse::[a]->Int->Warehouse a
buildWarehouse [] _ = Shelf []
buildWarehouse xs n | length xs < n = Shelf xs
                    | otherwise = Corridor (Shelf (take n xs)) (buildWarehouse (drop n xs) n)

aWarehouse = Corridor (Shelf [Box,Box]) (Corridor (Shelf [Barrel, NoGoods]) (Shelf [Box]))

map'::(a->b)->Warehouse a->Warehouse b
map' f (Shelf xs) = Shelf [f x|x<-xs]
map' f (Corridor l r) = Corridor (map' f l) (map' f r)


fold::(a->a->a)->a->Warehouse a->a
fold f res (Shelf xs) = foldr f res xs
fold f res (Corridor left right) = f (fold f res left) (fold f res right)

inv::Warehouse Goods->(Int, Int)
inv c = fold (\(x,y) (x',y') -> (x+x', y+y')) (0,0) (map' aux c)
    where aux Box = (1,0)
          aux Barrel = (0,1)
          aux _ = (0,0)
{-
data Tree a = Leaf a | Node a [Tree a]

instance Show a => Show (Tree a) where
    show (Leaf v) = "Leaf " ++ show v
    show (Node v xs) = "Node " ++ show v ++ " [" ++ foldr (\x acc -> show' x acc) "" xs ++ "]"
        where show' x acc = if acc == [] then show x ++""++ acc else show x ++", "++ acc
 
aTree = Node 1 [Node 21 [Leaf 31, Leaf 32], Node 22[Leaf 31, Node 32 [Leaf 41, Leaf 42, Leaf 43]]]
countTree::Tree a->Int
countTree (Leaf _) = 1
countTree (Node _ xs) = 1 + sum [countTree x | x <- xs]

sumTree (Leaf v) = v
sumTree (Node v xs) = v + sum [sumTree x | x <- xs]

contains e (Leaf v) = e == v
contains e (Node v xs) = e == v || or [contains e x|x <- xs]

treeMap::(a -> b)->Tree a -> Tree b
treeMap f (Leaf v) = Leaf (f v)
treeMap f (Node v xs) = Node (f v) (treeMap' f xs)
    where treeMap' _ [] = []
          treeMap' f (x:xs) = treeMap f x : treeMap' f xs

treeFold::(a -> b -> b)->b->Tree a -> b
treeFold f acc (Leaf v) = f v
treeFold f acc (Node v []) = f v (f acc)
treeFold f acc (Node v xs) = f v (treeFold' f acc xs)
    where treeFold' f acc [] = acc
          treeFold' f acc (x:xs) = f x (treeFold acc x)
-}
-- f( f( f( f( f( f() ) ) ) ) )

data Tree a = Empty | Node a (Tree a) (Tree a)

instance (Show a) => Show (Tree a) where
    show Empty = ""
    show (Node v l r) = "Node " ++ show v ++ "[" ++ show l ++ show r ++ "]"

instance (Eq a) => Eq (Tree a) where
    (==) Empty Empty = True
    (==) (Node a al ar) (Node b bl br) = a == b && al == bl && ar == br 
    (==) _ _ = False

instance (Num a) => Num (Tree a) where
    (+) Empty (Node v l r) = Node v l r
    (+) (Node v l r) Empty = Node v l r
    (+) (Node a al ar) (Node b bl br) = Node (a+b) (al+bl) (ar+br)

foldTree::(a->a->a)->a->Tree a-> a
foldTree _ acc Empty = acc
foldTree f acc (Node v l r) = f v (f (foldTree f acc l) (foldTree f acc r))

aTree = Node 1 (Node 2 Empty Empty) (Node 2 (Node 3 Empty Empty) (Node 3 Empty Empty))

sumTree::(Num a)=> Tree a -> a
sumTree = foldTree (+) 0
countTree Empty = 0
countTree (Node _ l r) = 1 + countTree l + countTree r

containsTree::Eq a => a -> Tree a -> Bool
containsTree _ Empty = False
containsTree e (Node v l r) = v == e || containsTree e l || containsTree e r