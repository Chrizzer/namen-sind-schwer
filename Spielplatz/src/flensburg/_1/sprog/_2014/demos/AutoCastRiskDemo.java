package flensburg._1.sprog.demos;

import java.util.Locale;
import java.util.Scanner;

/**
 * Demonstriert das Fehlerrisiko, bei Verwendung nicht typenreiner Ausdrücke.
 * Hier: Programmierung der Kugelvolumen-Formel 4/3*PI*r*r*r
 *
 */
public class AutoCastRiskDemo {
    public static void main(String[] args)
    {
        double richtig, falsch, r;
        final double PI = 3.14159;

        Locale.setDefault(Locale.US);
        Scanner eingabe = new Scanner(System.in);

        System.out.print("Bitte Kugelradius eingeben: ");
        r = eingabe.nextDouble();
        if (r >= 0)
        {
            // korrekt mit typenreinem Ausdruck:
            // möglich natürlich auch richtig = (double)4/(double)3 * PI * r * r * r;
            richtig = 4./3. * PI * r * r * r;

            // fehlerhaft mit automatischem Upcasting:
            // - zunächst wird die int-Division 4/3 ausgeführt; Ergebnis: 1
            // - erst dann wird aus 1 per Upcasting eine 1.0
            falsch  = 4/3   * PI * r * r * r;

            System.out.println("korrektes Volumen ist " + richtig);
            System.out.println("fehlerhaftes Volumen ist " + falsch);
        }
        else
        {
            System.out.println("kein negativer Radius möglich!");
        }
        eingabe.close();
    }
}
