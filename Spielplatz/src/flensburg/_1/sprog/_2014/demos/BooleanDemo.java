package flensburg._1.sprog.demos;


/**
 * Demonstriert die Verwendung von bool'scher Logik
 */
public class BooleanDemo {
    public static void main (String[] args)
    {
        int t = 0;
        //wenn man hier & verwenden würde, müsste ein Fehler auftreten, da 27/0 nicht erlaubt ist.
        //durch den && Operator wird aber abgeprochen nachdem t!=0 als false ausgewertet wird.
        //Ändern Sie den Operator auf & und schauen Sie was passiert

        // [Christopher] Das Stichwort hier lautet short circuit evaluation
        if ((t != 0) && ((27 / t) < 3))
        {
            System.out.println("Der Wert von t ist nicht 0.");
        }
        else
        {
            System.out.println("Der Wert von t ist 0.");
        }
    }
}
