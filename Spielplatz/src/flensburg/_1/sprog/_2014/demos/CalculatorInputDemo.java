package flensburg._1.sprog.demos;
import java.util.Locale;
import java.util.Scanner;

/**
 * Demo zu Übung 6. Wie man vom Scanner Operator und Zahl einliest.
 * Z.B. +4 oder -2.4
 */
public class CalculatorInputDemo
{

    public static void main(String[] args)
    {
        String text;
        char zeichen;
        double zahl;
        Scanner scan;

        Locale.setDefault(Locale.US);

        System.out.println("Bitte Operator plus Zahl eingeben: ");
        scan = new Scanner(System.in);

        try
        {
            /************************************************************************************************************/
            /** scan.next() liest die Zahl nicht mit, wenn nach dem Operator ein
             * Leerzeichen steht! **/
            /** scan.next() plus scan.nextDouble() geht nicht, wenn zwischen
             * Operator und Zahl KEIN Leerzeichen steht! **/
            /** Deshalb ganze Eingabe (-Zeile) lesen: **/
            /************************************************************************************************************/
            text = scan.nextLine();
            System.out.println("Die gesamte Eingabe ist \"" + text + "\"");

            /************************************************************************************************************/
            /** Der Operator sollte das erste Zeichen in der Eingabe sein: **/
            /************************************************************************************************************/
            zeichen = text.charAt(0);
            System.out.println("Der Operator ist " + zeichen);

            /************************************************************************************************************/
            /** Die Zahl steht im Rest der Eingabe: **/
            /************************************************************************************************************/
            System.out.println("Eingabe-Rest nach dem Operator ist \"" + text.substring(1) + "\"");

            /************************************************************************************************************/
            /** Darin findet nextDouble() die Zahl, egal ob sie im (Rest des) 1.
             * Eingabe-Token oder im zweiten steht: **/
            /************************************************************************************************************/
            scan = new Scanner(text.substring(1));
            zahl = scan.nextDouble();
            System.out.println("Die Zahl ist " + zahl);
        } catch (Exception exc)
        {
            System.out.println("Ungueltige Eingabe?!");
            exc.printStackTrace();
        }
    }
}
