package flensburg._1.sprog.demos;
/**
 *  demonstriert das Ungenauigkeitsproblem bei Fließkomma-Typen
 *
 */
import java.text.DecimalFormat;       // zur Steuerung des Ausgabeformats
import java.lang.Math;                // für die Absolutwertberechnung

public class FloatingPrecisionDemo
{
    public static void main(String[] args)
    {
        // Formatierer für 15 Nachkommastellen (gerundet) definieren:
        DecimalFormat df = new DecimalFormat("#.###############");

        double a = 4.4;
        double b = 3.4;
        double diff = a-b;
        final double maxToleranz = 0.000000000000001; // => Korrektheit auf 15 Nachkommastellen

        System.out.println("a = " + a);
        System.out.println("b = " + b);

        // Darstellung mit Formatierer gerundet:
        System.out.println("a-b (gerundete, \"korrekte\" Ausgabe) = " + df.format(diff));

        // exakte Darstellung des errechneten (fehlerhaften) Wertes:
        System.out.println("a-b (errechneter, mathematisch fehlerhafter Wert) = " + diff);

        // verarbeitungsbedingt ist hier also a-b NICHT 1.0:
        if ((a-b) != 1.0)
            System.out.println("==> 4.4 - 3.4 ist hier also NICHT 1.0 !!!");

        // Deshalb Prüfung auf geforderte Genauigkeit (Fehler < maxToleranz?):
        // im Rahmen der Verarbeitungsgenauigkeit ist das Ergebnis korrekt
        if (Math.abs(diff - 1.0) <= maxToleranz)
            System.out.println("==> Berechnung im Rahmen der Verarbeitungsgenauigkeit aber korrekt");
    }
}

