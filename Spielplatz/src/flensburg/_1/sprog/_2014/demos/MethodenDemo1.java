package flensburg._1.sprog.demos;

import java.util.Scanner;

/**
 * 
 * Zeigt die Verwendung einer einfachen Methode 
 * innerhalb einer Klasse
 *
 */
public class MethodenDemo1
{   
    public static void main(String[] args)
    {
        int nWert, summe = 0;

        Scanner eingabe = new Scanner(System.in);
            
        try
        {
            System.out.println("Berechnet die Summe über n\nbitte n eingeben: ");
            nWert = eingabe.nextInt();
            if (nWert < 0)
                throw new Exception("keine negativen Werte mˆglich!");
                
            // Summe über n mittels Methode summeÜber() berechnen:
            summe = summeÜber(nWert);
            System.out.println("Summe ¸ber " + nWert + " ist " + summe);
        }
        catch (Exception exc)
        {
            System.out.println("ung¸ltige Eingabe !");
            exc.printStackTrace();
        }
    }
    
    /**
     * Methode zur Berechnung der Summe über n
     * @param n der Wert für den die Summe über n berechnet werden soll(Parameter)
     * @return Summe über n als int (Rückgabetyp)
     */
    public static int summeÜber(int n)
    {
        int ergebnis = 0;
        
        for (int counter = 1;  counter <= n;  counter++)
        {
            ergebnis = ergebnis + counter;
        }
        return ergebnis;
    }
}
