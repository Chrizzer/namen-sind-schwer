package flensburg._1.sprog.demos;

import java.util.Scanner;

/**
 * 
 * Zeigt die Verwendung einer einfachen Methode 
 * innerhalb einer Klasse mit der Erweiterung dass Fehler abgefangen werden
 *
 */
public class MethodenDemo2
{   
    public static void main(String[] args)
    {
        int nWert, summe = 0;

        Scanner eingabe = new Scanner(System.in);
            
        //Die Methode summeÜber kann eine Exception auslösen. Daher rufen wir sie in einem
        //try Block aus
        try
        {
            System.out.println("Berechnet die Summe über n\nbitte n eingeben: ");
            nWert = eingabe.nextInt();
                
            // Summe über n mittels Methode summeÜber() berechnen:
            summe = summeÜber(nWert);  
            
            System.out.println("Summe ¸ber " + nWert + " ist " + summe);
        }
        catch (Exception exc)
        {
            System.out.println("ungültige Eingabe !");
            exc.printStackTrace();
        }
    }
    
    
    /**
     * 
     * Methode zur Berechnung der Summe über n
     * @param n der Wert für den die Summe über n berechnet werden soll(Parameter)
     * @return Summe über n als int (Rückgabetyp)
     * @throws Exception falls n < 0 
     */
    public static int summeÜber(int n)     throws Exception
    {                                      
        int ergebnis = 0;
        //wenn die Bedingung zutrifft wird die Exception geworfen
        if (n < 0)
            throw new Exception("keine negativen Werte möglich!");
        
        for (int counter = 1;  counter <= n;  counter++)
        {
            ergebnis = ergebnis + counter;
        }
        return ergebnis;
    }
}
