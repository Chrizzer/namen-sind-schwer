package flensburg._1.sprog.demos;

/**
 * Demonstriert die Verwendung von multidimensionalen Arrays
 *
 */
public class MultiDimArrays
{
    public static void main(String[] args)
    {
        // Array aus 3 int[2]-Arrays:
        int[][] demoArray1 = {{0,1}, {10,11}, {20,21}};

        System.out.println("demoArray1:");
        for (int i=0; i < demoArray1.length; i++)
        {
            for (int k=0; k < demoArray1[i].length; k++)
            {
                System.out.println((i+1) + ". int-Array, " + (k+1) + ". Element: " + demoArray1[i][k]);
            }
            System.out.println();
        }

        // Array mit unterschiedlich langen int[]-Arrays und successive Allokierung:
        int[][] demoArray2 = new int[3][];            // nur äußeres Array allokiert

        System.out.println("demoArray2:");
        for (int i=0; i < demoArray2.length; i++)
        {
            demoArray2[i] = new int[i+2];            // innere Arrays allokiert als:
            // int[2] -> int[3] -> int[4] ...
            for (int k=0; k < demoArray2[i].length; k++)
            {
                demoArray2[i][k] = 10*i + k;
                System.out.println((i+1) + ". int-Array, " + (k+1) + ". Element: " + demoArray2[i][k]);
            }
            System.out.println();
        }
    }
}
