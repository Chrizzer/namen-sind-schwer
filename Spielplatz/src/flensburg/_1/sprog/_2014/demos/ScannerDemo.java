package flensburg._1.sprog.demos;

import java.util.Locale;
import java.util.Scanner;

/**
 * 
 * Demo zur Benutzung des Scanners um verschiedene 
 * Datentypen einzulesen
 *
 */
public class ScannerDemo
{
    public static void main(String[] args)
    {
        char   zeichen;
        int    intWert;
        double doubleWert;
        String text;
        
        // Erzeugung eines Scanners und Umschaltung auf Locale US:
        Scanner eingabe = new Scanner(System.in); 
        eingabe.useLocale(Locale.US);

        System.out.println("Bitte jeweils Ganzzahl, Flieﬂkommazahl (Punktnotation)"
                           + " oder einzelnes Zeichen eingeben.");
        System.out.println("Beenden mit ctrl Z");
        System.out.print("erste Eingabe: ");
        
        //solange es irgendeine Eingabe gibt
        while (eingabe.hasNext())
        {
            // prüfe ob es sich um eine Ganzzahl-Eingabe handelt:
            if (eingabe.hasNextInt())
            {
                intWert = eingabe.nextInt();
                System.out.println("Ganzzahl " + intWert + " eingegeben\n");
            }
            
            // falls nicht, prüfe ob es sich um eine Kommazahl-Eingabe handelt:
            else if (eingabe.hasNextDouble())
            {
                doubleWert = eingabe.nextDouble();
                System.out.println("Kommazahl " + doubleWert + " eingegeben\n");
            }
            
            // treffen die beide ersten Fälle nicht zu behandele die Eingabe als Text-Eingabe:  
            else
            {
                text = eingabe.next();         // ganzes Token einlesen
                zeichen = text.charAt(0);      // erstes Zeichen aus dem Token lesen
                System.out.println("Text \"" + text + "\" eingegeben, "
                                   + "beginnt mit '" + zeichen + "'\n");
            }
            System.out.print("weitere Eingabe: ");
        }
        System.out.println("Ende");
        
        eingabe.close();
    }
}
