package flensburg._1.sprog.demos;

import java.util.Scanner;


/**
 *  Demonstriert das Arbeiten mit Strings (Zeichenketten/Texte) in SPROG
 * - Deklaration
 * - Eingabe (ohne Leerzeichen)
 * - Längenermittlung mit length()
 * - Verknüpfung (concatenation)
 * - Vergleich
 * - Zeichen auslesen
 * - Eingabe bis Zeilenende lesen (auch mit Leerzeichen)
 * - Substrings bilden 
 *   - von..bis
 *   - von..bisEnde
 *
 */
public class StringDemo
{
    public static void main(String[] args)
    {
        /********************************************************************
        * Deklaration mehrerer String-Variablen und einer String-Konstante: *   
        ********************************************************************/
        String text1, text2, kombiText, zeile, dummy, teilText, restZeile;
        final String newLine = "\n";    // <== String-Konstante
        
        char zeichen;
        int  index1, index2;
        
        Scanner eingabe = new Scanner(System.in);  // wenn Eclipse-Warnung: Scanner schließen oder ignorieren!
        
        /***********************************************
        * String-Eingabe (einzelnes token) mit next(): *   
        ***********************************************/
        System.out.println("Bitte Text ohne Leerzeichen eingeben (ruhig mal \"Hallo\" versuchen):");
        text1 = eingabe.next();
        System.out.println("Bitte zweiten Text ohne Leerzeichen eingeben (ruhig nochmal dasselbe):");
        text2 = eingabe.next();
        
        /***************************************
        * String-Länge ermitteln mit length(): *   
        ***************************************/
        System.out.println(text2 + " enthält " + text2.length() + " Zeichen");
        
        /*****************************
        * String-Verknüpfung mit + : *   
        *****************************/
        kombiText = text1 + text2;
        System.out.println("Beide Texte kombiniert: " + kombiText + newLine);
        
        /***********************************************
        * String-Vergleich (inhaltlich) mit equals() : *   
        ***********************************************/
        if (text2.equals(text1))
            System.out.println("beide Texte sind gleich");
        if (text1.equals("Hallo") || text2.equals("Hallo"))
            System.out.println("einfallslos!");
        // Der folgende Vergleich wird gegen Ende des Semesters erläutert:
        if (text1 == text2)
            System.out.println("Ups! -- Diese Ausgabe darf hier auch bei gleichen Texten nicht erscheinen!");
        
        /***************************************************
        * Zeichen aus einem String auslesen mit charAt() : *   
        ***************************************************/
        // 
        System.out.println(newLine + "Bitte Index (ab 0) eines Zeichens aus Text1 auswählen:");
        index1 = eingabe.nextInt();
        zeichen = text1.charAt(index1);
        System.out.println("Das " + (index1+1) + ". Zeichen in Text1 ist: " + zeichen);
        
        /******************************************************************************
        /* Bis Zeilen-Ende lesen mit nextLine():                                      *
        **** ACHTUNG: *****************************************************************
        * nextInt() hat oben nur den Wert für index1 aus dem Eingabepuffer gelesen;   *
        * das newLine-Zeichen (Eingabetaste) dahinter steht noch im Puffer.           *
        * Da nextLine() immer bis zum nächsten newLine-Zeichen liest, wird hier beim  *
        * folgenden nextLine() nur ein leerer Text ("") eingelesen.                   *
        ******************************************************************************/
        dummy = eingabe.nextLine();                 // sollte leerer String sein! ****/    
        System.out.println("der Rest der Eingabezeile hinter der " + index1 + " ist leer: \"" 
                           + dummy + "\"" + newLine);
        
        /*************************************************************
        * Nun die Eingabe einer ganzen (neuen) Zeile mit nextLine(): *
        *************************************************************/ 
        System.out.println("Bitte Text mit Leerzeichen eingeben:");
        // ==> Hier steht noch der gesamte Text vor dem newLine-Zeichen im Puffer: 
        zeile = eingabe.nextLine();
        
        /*************************************************************************
        * Sub-Strings bilden mit substring():                                    *
        * ==> Achtung: der erste Index ist INklusive, der zweite ist EXklusive!  *
        *************************************************************************/ 
        System.out.println("Erster und letzter Zeichen-Index für Teiltext:");
        index1 = eingabe.nextInt();
        index2 = eingabe.nextInt();
        teilText = zeile.substring(index1, index2+1);
        System.out.println("Der gewählte Teiltext ist \"" + teilText + "\"" + newLine);
        
        restZeile = zeile.substring(index2+1);
        System.out.println("Der Rest der Zeile ist \"" + restZeile + "\""); 

        System.out.println("Ende");
        
        eingabe.close();
    }
}
