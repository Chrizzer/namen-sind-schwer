package flensburg._1.sprog.demos.loops;

import java.util.Scanner;

/**
 * Berechnet die Summe von 1 bis n mit do while
 *
 */
public class Summe1BisN_do
{
    public static void main(String[] args)
    {
        int n;           
        int counter = 1; 
        int summe = 0;
        boolean validInputMissing = true;

        Scanner eingabe = new Scanner(System.in);
        
        // gültige Eingabe lesen:
        do
        {
            System.out.println("Berechnet die Summe über n\nbitte n eingeben: ");
            // ein Eingabefehler z.B. die Eingabe eines Buchstabens wird hier nicht
            // abgefangen. Das machen wir später.
            n = eingabe.nextInt();
            if (n > 0)
                validInputMissing = false;
            else
                System.out.println("Wert muss mindestens 1 sein!\n");
        } while(validInputMissing);
        
        // Summe über n berechnen:
        do
        {
            summe = summe + counter;
            counter++;
        } while (counter <= n);
            
        System.out.println("Summe über " + n + " ist " + summe);
        eingabe.close();
    }
}

