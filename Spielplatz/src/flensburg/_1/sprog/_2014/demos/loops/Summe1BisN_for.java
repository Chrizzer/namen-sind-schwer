package flensburg._1.sprog.demos.loops;

import java.util.Scanner;

/**
 * Berechnet die Summe von 1 bis n mit for und do..while für die Input-Validierung
 *
 */
public class Summe1BisN_for
{
    public static void main(String[] args)
    {
        int n = 0;
        int summe = 0;
        boolean validInputMissing = true;

        Scanner eingabe = new Scanner(System.in);
        // gültige Eingabe lesen (for-Schleife wäre hier unsinnig):
        do
        {
            System.out.println("Berechnet die Summe über n\nbitte n eingeben: ");
            // ein Eingabefehler z.B. die Eingabe eines Buchstabens wird hier nicht
            // abgefangen. Das machen wir später.
            n = eingabe.nextInt();
            if (n > 0)
                validInputMissing = false;
            else
                System.out.println("Wert mss mindestens 1 sein!\n");
        } while (validInputMissing);

        // Summe über n berechnen:
        for (int counter = 1; counter <= n; counter++)
        {
            summe = summe + counter;
        }
        System.out.println("Summe über " + n + " ist " + summe);
        eingabe.close();
    }
}
