package flensburg._1.sprog.demos.loops;

import java.util.Scanner;

/**
 * Berechnet die Summe von 1 bis n mit while und der Hilfe eines Counters
 *
 */
public class Summe1BisN_while
{
    public static void main(String[] args)
    {
        int n = 0;      
        int counter = 1; 
        int summe = 0;
        boolean validInputMissing = true;

        Scanner eingabe = new Scanner(System.in);
        
        // gültige Eingabe lesen:
        while(validInputMissing)
        {
            System.out.println("Berechnet die Summe über n\nbitte n eingeben: ");
            // ein Eingabefehler z.B. die Eingabe eines Buchstabens wird hier nicht
            // abgefangen. Das machen wir später.
            n = eingabe.nextInt();
            if (n > 0)
            {
                validInputMissing = false;
            }
            else
            {
                System.out.println("Wert muss mindestens 1 sein!\n");
            }
        } 
        
        // Summe über n berechnen:
        while (counter <= n)
        {
            summe = summe + counter;
            counter++;
        }
        System.out.println("Summe über " + n + " ist " + summe);
        
        eingabe.close();
    }
}
