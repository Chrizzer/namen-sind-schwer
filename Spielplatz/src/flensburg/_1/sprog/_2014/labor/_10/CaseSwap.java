package flensburg._1.sprog._2014.labor._10;

import javax.swing.JOptionPane;
import java.util.Scanner;

public class CaseSwap
{

    public static void main(String[] args)
    {
        // Variablenzuweisung
        Scanner input;
        String eingegebenerString;
        String ausgabeString;
        String überschrift = ("SPROG Aufgabe 10-1: Iwas mit Strings");
        // Eingabeaufforderung
        input = new Scanner(JOptionPane.showInputDialog(null, "Bitte geben sie eine Abfolge von Zeichen ein",
                überschrift, JOptionPane.PLAIN_MESSAGE));

        eingegebenerString = input.nextLine();
        ausgabeString = umwandeln(eingegebenerString);

        // Ausgabefenster
        JOptionPane.showMessageDialog(null, "Hier ist der umgewandelte String " + ausgabeString + "\n"
                + "Hier ist nochmal der eingegebene String " + eingegebenerString, überschrift,
                JOptionPane.PLAIN_MESSAGE);
        input.close();
    }
    static String umwandeln(String eingegebenerString)
    {
        // toUpperCase(); und toLowerCase(); wechseln alle buchstaben
        // String in einen ZeichenArray umwandeln und jedes Zeichen
        // überpr�fen und ggf umwandeln
        char[] eingegebenerStringAlsArray = eingegebenerString.toCharArray();
        char[] vergleichsArray = eingegebenerString.toCharArray();
        for (int counter = 0; counter < eingegebenerStringAlsArray.length;)
        {
            boolean innereBedingung = true;
            // Wird neu gesetzt damit auf jeder Stelle getestet wird
            while (innereBedingung)
            {
                //ASCII - Int werte und so
                // falls der char an der Stelle [counter] immernoch gleich ist
                // toUpperCase();
                if (eingegebenerStringAlsArray[counter] == vergleichsArray[counter])
                {
                    char temp = eingegebenerStringAlsArray[counter];
                    temp = Character.toUpperCase(temp);
                    eingegebenerStringAlsArray[counter] = temp;
                }
                // char k�nnte schon gro� gewesen sein, also falls er immernoch
                // gleich ist toLowerCase();
                if (eingegebenerStringAlsArray[counter] == vergleichsArray[counter])
                {
                    char temp = eingegebenerStringAlsArray[counter];
                    temp = Character.toLowerCase(temp);
                    eingegebenerStringAlsArray[counter] = temp;
                }
                counter++;
                innereBedingung = false;
            }
        }
        
        // Aus dem Char Array einen String bauen
        String umgewandelterString = new String(eingegebenerStringAlsArray);
        return umgewandelterString;
    }
}