package flensburg._1.sprog._2014.labor._10;

import javax.swing.JOptionPane;
import java.util.Scanner;

public class StringVariante
{

    static String spielfeldErstellen(String[][] spielfeld)
    {
        String test1 = (" ");
        //magic numbers unsch�n
        for (int zeile = 2; zeile >= 0; zeile--)
        {
            test1 = ("\n" + test1);// Zeilenumbruch nach jeder Zeile
            for (int spalte = 2; spalte >= 0; spalte--)
            {
                test1 = (spielfeld[zeile][spalte] + "  " + test1);
            }
        }
        return test1;
    }

    static boolean eingabeErfolgt(String eingabe, String[][] spielfeld)
    {
        for (int zeile = 0; zeile <= 2; zeile++)
        {
            for (int spalte = 0; spalte <= 2; spalte++)
            {
                if (spielfeld[zeile][spalte].equals(eingabe))
                {
                    return true;
                }
            }
        }
        return false;
    }

    static void markieren(String eingabe, String[][] spielfeld, int counter)
    {
        boolean eingabeIstErfolgt = eingabeErfolgt(eingabe, spielfeld);
        for (int zeile = 0; zeile <= 2 && eingabeIstErfolgt; zeile++)
        {
            for (int spalte = 0; spalte <= 2 && eingabeIstErfolgt; spalte++)
            {
                if (spielfeld[zeile][spalte].equals(eingabe))
                {
                    if (counter % 2 == 0)
                    {
                        spielfeld[zeile][spalte] = "X";
                        eingabeIstErfolgt = false;
                    } else
                    {
                        spielfeld[zeile][spalte] = "O";
                        eingabeIstErfolgt = false;
                    }

                }
            }
        }
    }

    public static void main(String[] args)
    {
        Scanner input;
        String[] a1 = { "1", "2", "3" };
        String[] b1 = { "4", "5", "6" };
        String[] c1 = { "7", "8", "9" };
        String[][] spielfeld = { a1, b1, c1 };
        String überschrift = ("�bung10-2: Tic Tac Toe");
        int counter = 0;

        while (counter < 9)
        {
            try
            {
                if (counter % 2 == 0)
                {
                    input = new Scanner(JOptionPane.showInputDialog(null, "Setzen sie ein X an eine beliebige Stelle\n"
                            + spielfeldErstellen(spielfeld), überschrift, JOptionPane.PLAIN_MESSAGE));
                    String eingabe = input.nextLine();
                    if (eingabeErfolgt(eingabe, spielfeld))
                    {
                        markieren(eingabe, spielfeld, counter);
                    } else
                    {
                        throw new EingabeException();
                    }
                } else
                {
                    input = new Scanner(JOptionPane.showInputDialog(null, "Setzen sie ein O an eine beliebige Stelle\n"
                            + spielfeldErstellen(spielfeld), überschrift, JOptionPane.PLAIN_MESSAGE));
                    String eingabe = input.nextLine();
                    if (eingabeErfolgt(eingabe, spielfeld))
                    {
                        markieren(eingabe, spielfeld, counter);
                    } else
                    {
                        throw new EingabeException();
                    }
                }
                counter++;
            } catch (EingabeException e)
            {
                JOptionPane.showMessageDialog(null, "Die Eingabe war nicht korrekt, bitte erneut versuchen.",
                        überschrift, JOptionPane.PLAIN_MESSAGE);
            }
        }
        JOptionPane.showMessageDialog(null, "Ende", überschrift, JOptionPane.PLAIN_MESSAGE);
    }
}