package flensburg._1.sprog._2014.labor._10;

import javax.swing.JOptionPane;
import java.util.Scanner;

public class TicTacToe
{

    static String spielfeldErstellen(char[][] spielfeld)
    {
        String test1 = (" ");
        // char[][] spielfeldzurüberprüfung = spielfeld.clone();
        for (int zeile = 2; zeile >= 0; zeile--)
        {
            test1 = ("\n" + test1);// Zeilenumbruch nach jeder Zeile
            for (int spalte = 2; spalte >= 0; spalte--)
            {
                test1 = (spielfeld[zeile][spalte] + "  " + test1);
            }
        }
        return test1;
    }

    static void markieren(Scanner input, char[][] spielfeld, int counter, int eingabe)
    {
        char[][] spielfeldzurüberprüfung = spielfeld.clone();
        try
        {
            for (int zeile = 0; zeile <= 2; zeile++)
            {
                for (int spalte = 0; spalte <= 2; spalte++)
                {
                    if ((int) spielfeld[zeile][spalte] == eingabe)
                    {
                        if (counter % 2 == 0)
                        {
                            spielfeld[zeile][spalte] = 'X';
                        } else
                        {
                            spielfeld[zeile][spalte] = 'O';
                        }
                        if (spielfeld[zeile][spalte] == spielfeldzurüberprüfung[zeile][spalte])
                        {
                            throw new EingabeException();
                        }
                    }
                }
            }
        } catch (EingabeException e)
        {
            JOptionPane.showMessageDialog(null, e + "\nDie Eingabe war nicht korrekt, bitte erneut versuchen.",
                    "überschrift", JOptionPane.PLAIN_MESSAGE);
        }
    }

    public static void main(String[] args)
    {
        Scanner input;
        String a2 = ("123");
        String b2 = ("456");
        String c2 = ("789");
        char[] a1 = a2.toCharArray();
        char[] b1 = b2.toCharArray();
        char[] c1 = c2.toCharArray();
        char[][] spielfeld = { a1, b1, c1 };
        String überschrift = ("�bung10-2: Tic Tac Toe");
        int counter = 0;
        JOptionPane.showMessageDialog(null, "Text\n" + spielfeldErstellen(spielfeld), überschrift,
                JOptionPane.PLAIN_MESSAGE);
        try
        {
            while (counter < 9)
            {

                if (counter % 2 == 0)
                {
                    input = new Scanner(JOptionPane.showInputDialog(null, "Setzen sie ein X an eine beliebige Stelle",
                            überschrift, JOptionPane.PLAIN_MESSAGE));
                    int eingabe = input.nextInt();
                    markieren(input, spielfeld, counter, eingabe);
                } else
                {
                    input = new Scanner(JOptionPane.showInputDialog(null, "Setzen sie ein O an eine beliebige Stelle",
                            überschrift, JOptionPane.PLAIN_MESSAGE));
                    int eingabe = input.nextInt();
                    markieren(input, spielfeld, counter, eingabe);
                }
                JOptionPane.showMessageDialog(null, "Text\n" + spielfeldErstellen(spielfeld), überschrift,
                        JOptionPane.PLAIN_MESSAGE);
            }
            counter++;
        } catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Die Eingabe war nicht korrekt, bitte erneut versuchen.", überschrift,
                    JOptionPane.PLAIN_MESSAGE);
        }
    }
}