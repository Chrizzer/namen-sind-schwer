package flensburg._1.sprog._2014.labor._3;

import java.util.Locale;
import java.util.Scanner;
public class StatistikDouble {

	public static void main(String[] args) {
		//deklarierung und initalisierung benötigter Speicherzellen
		double werte = 0.0;
		double anzahl = 0.0;
		double summe = 0.0;
		double maximum = Double.MIN_VALUE;
		double minimum = Double.MAX_VALUE;
		double mittelwert = 0.0;
		//deklarierung der Strings
		String eingabe = "Bitte geben Sie ihre Werte ein."
						+ "Zum Starten der Berechnung, geben Sie 'e' ein und drücken Enter.\n";
		String anzahlAusgabe = "\nAnzahl aller Werte: ";
		String summeAusgabe = "\nSumme aller Werte: ";
		String maximumAusgabe = "\nMaximum aller Werte: ";
		String minimumAusgabe = "\nMinimum aller Werte: ";
		String mittelwertAusgabe = "\nMittelwert aller Werte: ";
		String berechnung = "\nDie Berechnung ergab folgende Werte:";
		
		//erzeugung des Scanner-Objekt
		Locale.setDefault(Locale.US);
		Scanner scan = new Scanner(System.in);
		System.out.println(eingabe);
		//berechnung 
		while(scan.hasNext()) {
			//einlesen der Werte
			werte = scan.nextDouble();
			anzahl ++;
			summe = summe + werte;
			if (werte > maximum){
				maximum = werte;
			}else if (werte < minimum) {
				minimum = werte;
			}
		}
		mittelwert = summe / anzahl;
		//Ausgabe
		System.out.println( berechnung 
							+ anzahlAusgabe + anzahl 
							+ summeAusgabe + summe
							+ maximumAusgabe + maximum
							+ minimumAusgabe + minimum
							+ mittelwertAusgabe + mittelwert);
		//beendet den Scanner
		scan.close();
	}

}
