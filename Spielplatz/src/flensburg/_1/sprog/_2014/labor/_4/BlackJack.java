package flensburg._1.sprog._2014.labor._4;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class BlackJack
{

    public static void main(String[] args)
    {

        int zahl1 = 0;
        int zahl2 = 0;
        String bitte = ("Bitte zwei Zahlen eingeben um Spiel zu beginnen! \n");
        String überschrift = ("Laboraufgabe 4-2 Black Jack");
        String above21 = ("Die eingegebenen Zahlen sind über 21.");
        String gleich = ("Die beiden Zahlen sind identisch, bitte spielen Sie noch einmal.");
        String verloren = ("Leider sind beide Zahlen größer 21, es gewinnt die Bank!");
        String chickendinner = (" ist näher an der 21 an. \nWINNER WINNER CHICKEN DINNER");
        String zusatz = ("Tut mir leid, dieses Ereignis ist nicht Bestandteil der Aufgabenstellung.\nBitte erneut spielen.");
        String abschied = ("Vielen Dank, dass sie mit uns gespielt haben. \nWir freuen uns auf ihr nächstes mal bei SPROG.");
        String spielenString = ("Wollen sie erneut spielen?");
        boolean spielen = true;

        while (spielen == true)
        {
            Scanner input1 = new Scanner(JOptionPane.showInputDialog(null, bitte, überschrift,
                    JOptionPane.PLAIN_MESSAGE));
            Scanner input2 = new Scanner(JOptionPane.showInputDialog(null, bitte, überschrift,
                    JOptionPane.PLAIN_MESSAGE));

            zahl1 = input1.nextInt();
            zahl2 = input2.nextInt();

            if (zahl1 > zahl2 && zahl1 < 21)
            {
                JOptionPane.showMessageDialog(null, zahl1 + chickendinner, überschrift, JOptionPane.PLAIN_MESSAGE);
            } else if (zahl2 > zahl1 && zahl2 < 21)
            {
                JOptionPane.showMessageDialog(null, zahl2 + chickendinner, überschrift, JOptionPane.PLAIN_MESSAGE);
            } else if (zahl1 >= 21 && zahl2 >= 21)
            {
                JOptionPane.showMessageDialog(null, above21, überschrift, JOptionPane.PLAIN_MESSAGE);
            } else if (zahl1 == zahl2)
            {
                JOptionPane.showMessageDialog(null, gleich, überschrift, JOptionPane.PLAIN_MESSAGE);
            } else if (zahl1 + zahl2 < 21)
            {
                JOptionPane.showMessageDialog(null, verloren, überschrift, JOptionPane.PLAIN_MESSAGE);
            } else if (zahl1 > 21 && zahl2 < 21)
            {
                JOptionPane.showMessageDialog(null, zusatz, überschrift, JOptionPane.PLAIN_MESSAGE);
            }

            int antwort = JOptionPane.showConfirmDialog(null, spielenString, "Neues Spiel", JOptionPane.YES_NO_OPTION,
                    JOptionPane.INFORMATION_MESSAGE);
//unwichtig
            if (antwort == JOptionPane.OK_OPTION)
            {
                spielen = true;
            } else if (antwort == JOptionPane.NO_OPTION)
            {
                spielen = false;
                JOptionPane.showMessageDialog(null, abschied, überschrift, JOptionPane.PLAIN_MESSAGE);
            }
        }
    }
}