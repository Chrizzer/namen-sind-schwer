package flensburg._1.sprog._2014.labor._4;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Halbieren
{
    public static void main(String[] args)
    {
        String text01 = ("");
        int tl01 = 0;
        String textA = ("Bitte geben sie eine beliebige Abfolge an Zeichen ein.");
        String textg = ("Die eingegebene Abfolge von Zeichen ist gerade.\n");
        String textu = ("Die eingegebene Abfolge von Zeichen ist ungerade.\n");
        Scanner input = new Scanner(JOptionPane.showInputDialog(null, textA, "Laboraufgabe 4-1",
                JOptionPane.PLAIN_MESSAGE));
        int indikator =0;
        text01 = input.next();
        tl01 = text01.length();
        String eingabe = ("Die Eingabe war " + text01 + "\n");
        String textl = ( "Die Länge des Strings beträgt " + tl01);
        String textm = (". \nDas Zeichen in der Mitte ist ");
        String texth = (".\nDer zweite Teil der Zeichenkette lautet ");
        System.out.println("Vor While Schleife int tl01 = " + tl01);
        boolean go =true;
        while(go)
        {
            if(tl01%2 == indikator)
            {
                String text02 = text01.substring(text01.length() / 2);
                String ergebnisG = (textg + textl + texth + text02);
                JOptionPane.showMessageDialog(null, eingabe + ergebnisG, "Ergebnis", JOptionPane.PLAIN_MESSAGE);
            }
            else
            {   System.out.println(tl01);
                char middle = text01.charAt((tl01/2));
                String ergebnisU1 = (textu + textl + textm+ middle);
                JOptionPane.showMessageDialog(null, eingabe + ergebnisU1, "Ergebnis", JOptionPane.PLAIN_MESSAGE);
            }
            go = false;
        }
        input.close();
    }
}