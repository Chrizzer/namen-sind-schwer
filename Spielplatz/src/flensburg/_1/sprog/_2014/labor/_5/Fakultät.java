package flensburg._1.sprog._2014.labor._5;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Fakultät
{
    public static void main(String[] args)
    {
        int zahl;
        int fakultät = 1;
        int ergbenisFak;
        String eingabeaufforderung = ("Bitte geben sie einen ganzzahligen Wert ein.");
        String überschrift = ("Übungsaufgabe 5-1: Fakultät");
        String abschied = ("Beliebige Abschlussnachricht");
        String nochmal = ("\nSie haben keinen geeigneten Wert eingegeben.\nWert muss positiv sein und darf keine Kommastelle haben.\nBitte auf Ja klicken um eine andere Eingabe zu tätigen ");
        String ergebnis = ("Das Ergebnis ist ");
        String zugroß = ("Overflow error.\nDie eingegebene Zahl ist zu groß.");
        String ergebnis0 = ("Sie haben eine negative Zahl eingegeben.");
        boolean keinOverflow = true;
        while (keinOverflow)
        {
            Scanner input = new Scanner(JOptionPane.showInputDialog(null, eingabeaufforderung, überschrift,
                    JOptionPane.PLAIN_MESSAGE));
            try
            {
                zahl = input.nextInt();
                if (zahl < 0)
                {
                    JOptionPane.showMessageDialog(null, ergebnis0, überschrift, JOptionPane.ERROR_MESSAGE);
                } else
                {
                    for (int counter = 1; counter <= zahl && keinOverflow == true; counter++)
                    {
                        ergbenisFak = fakultät;
                        fakultät = fakultät * counter;
                        if (ergbenisFak != (fakultät / counter))
                        {
                            JOptionPane.showMessageDialog(null, zugroß, überschrift, JOptionPane.ERROR_MESSAGE);
                            keinOverflow = false;
                        }
                    }
                    if (keinOverflow != false)
                    {
                        if (fakultät >= 1)
                        {
                            JOptionPane.showMessageDialog(null, ergebnis + fakultät, überschrift,
                                    JOptionPane.PLAIN_MESSAGE);
                        } else if (zahl == 0)
                        {
                            JOptionPane.showMessageDialog(null, ergebnis + fakultät, überschrift,
                                    JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                }
                keinOverflow = false;
            } catch (Exception keinInt)
            {
                int antwort = JOptionPane.showConfirmDialog(null, keinInt + nochmal, überschrift,
                        JOptionPane.YES_OPTION);
                if (antwort == JOptionPane.YES_OPTION)
                {
                    keinOverflow = true;
                } else
                {
                    keinOverflow = false;
                    JOptionPane.showMessageDialog(null, abschied, überschrift, JOptionPane.PLAIN_MESSAGE);
                }
            }
            input.close();
        }
    }
}
