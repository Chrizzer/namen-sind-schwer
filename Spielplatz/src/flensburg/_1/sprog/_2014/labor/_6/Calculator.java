package flensburg._1.sprog._2014.labor._6;


import javax.swing.JOptionPane;
import java.util.Scanner;

public class Calculator
{
    public static void main(String[] args)
    {
        char vorzeichen;
        String text;
        String eingabe = ("Bitte Operator (+ - * /) plus Zahl eingeben: ");
        String eingabe2 = ("Die Eingabe war; \n");
        String überschrift = ("SPROG Aufgabe 6: Taschenrechner");
        String operator = ("Der Operator ist ");
        String operand = ("Der Operand ist ");
        String ergebnis = ("Das Ergebnis lautet ");
        String nullteilen = ("Es kann nicht durch 0 geteilt werden.");
        boolean Bedingung = true;
        Scanner input;

        while (Bedingung)
        {
            double zahl = 0.0;
            double speicher = 0.0;
            input = new Scanner(JOptionPane.showInputDialog(null, eingabe + "\n" + "Aktueller Wert ist " + zahl,
                    überschrift, JOptionPane.PLAIN_MESSAGE));

            text = input.nextLine();

            vorzeichen = text.charAt(0);

            input = new Scanner(text.substring(1));

            zahl = input.nextDouble();

            JOptionPane.showMessageDialog(null,
                    eingabe2 + text + "\n" + operator + vorzeichen + "\n" + operand + text.substring(1), überschrift,
                    JOptionPane.PLAIN_MESSAGE);

            input = new Scanner(JOptionPane.showInputDialog(null, eingabe + "\n" + "Aktueller Wert ist " + zahl,
                    überschrift, JOptionPane.PLAIN_MESSAGE));

            text = input.nextLine();

            vorzeichen = text.charAt(0);

            JOptionPane.showMessageDialog(null,
                    eingabe2 + text + "\n" + operator + vorzeichen + "\n" + operand + text.substring(1), überschrift,
                    JOptionPane.PLAIN_MESSAGE);

            input = new Scanner(text.substring(1));

            speicher = input.nextDouble();

            switch (text.charAt(0))
            {
                case 'e':
                    Bedingung = false;
                    JOptionPane.showMessageDialog(null, "Fehlermeldung für e", überschrift, JOptionPane.PLAIN_MESSAGE);
                    break;
                case '+':
                    double summe = 0.0;
                    zahl = input.nextDouble();
                    summe = zahl + speicher;
                    JOptionPane.showMessageDialog(null, ergebnis + summe, überschrift, JOptionPane.PLAIN_MESSAGE);
                    break;
                case '-':
                    double subtraktion = 0.0;
                    subtraktion = zahl - speicher;
                    JOptionPane.showMessageDialog(null, ergebnis + subtraktion, überschrift, JOptionPane.PLAIN_MESSAGE);
                    break;
                case '*':
                    double produkt = 0.0;
                    produkt = zahl * speicher;
                    JOptionPane.showMessageDialog(null, ergebnis + produkt, überschrift, JOptionPane.PLAIN_MESSAGE);
                    break;
                case '/':
                    if (speicher == 0.0)
                    {
                        JOptionPane.showMessageDialog(null, nullteilen, überschrift, JOptionPane.ERROR_MESSAGE);
                    }
                    int division = (int) ((double) zahl / speicher);
                    JOptionPane.showMessageDialog(null, ergebnis + division, überschrift, JOptionPane.PLAIN_MESSAGE);
                    break;
                default:
                    ;
            }
        }
    }
}
