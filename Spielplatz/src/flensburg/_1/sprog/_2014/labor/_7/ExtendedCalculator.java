package flensburg._1.sprog._2014.labor._7;

import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JOptionPane;

//case 'c' Problem , mit letztem Wert weitermachen Problem und Syntax problem eventuell?

public class ExtendedCalculator
{

    static String gibEingabeHilfe()
    {
        String eingabe = ("Bitte Operator (+ - * /) plus Zahl eingeben: ");
        return eingabe;
    }

    public static void main(String[] args)
    {
        char vorzeichen;
        String text;
        String überschrift = ("SPROG Aufgabe 7: Erweiterter Taschenrechner");
        boolean Bedingung = true;
        Scanner input;
        double zahl = 0.0;
        double zahl2 = 0.0;
        while (Bedingung)
        {
            try
            {
                input = new Scanner(JOptionPane.showInputDialog(null, gibEingabeHilfe() + "\n" + "Aktueller Wert ist "
                        + zahl2, überschrift, JOptionPane.PLAIN_MESSAGE));

                text = input.nextLine();
                vorzeichen = text.charAt(0);
                input = new Scanner(text.substring(1));
                switch (text.charAt(0))
                {
                    case 'e':
                        Bedingung = false;
                        break;
                    case 'c':
                        zahl = 0.0;
                        break;
                    case '+':
                        zahl = input.nextDouble();
                        input = new Scanner(text.substring(1));
                        zahl = zahl2 + zahl;
                        break;
                    case '-':
                        zahl = input.nextDouble();
                        input = new Scanner(text.substring(1));
                        zahl = zahl - zahl2;
                        break;
                    case '*':
                        zahl = input.nextDouble();
                        input = new Scanner(text.substring(1));
                        zahl = zahl2 * zahl;
                        break;
                    case '/':
                        zahl = input.nextDouble();
                        input = new Scanner(text.substring(1));
                        if (zahl == 0.0)
                        {
                            throw new DivideByZeroException();
                        } else
                        {
                            zahl =zahl2 / zahl;
                        }
                        break;
                    default:
                        throw new InvalidOperatorException();
                }
                zahl2 = zahl;
                input.close();
            } catch (DivideByZeroException e)
            {
                JOptionPane.showMessageDialog(null, e + "\n" + "Man kann nicht durch Null teilen. \n", überschrift,
                        JOptionPane.ERROR_MESSAGE);
            } catch (InputMismatchException e)
            {
                JOptionPane.showMessageDialog(null, e + "\n"
                        + "Sie haben keine g�ltige Eingabe vorgenommen.\n Bitte erneut versuchen", überschrift,
                        JOptionPane.ERROR_MESSAGE);
            } catch (InvalidOperatorException e)
            {
                JOptionPane.showMessageDialog(null, e + "\n"
                        + "Sie haben keine g�ltige Eingabe vorgenommen.\n Bitte erneut versuchen", überschrift,
                        JOptionPane.ERROR_MESSAGE);
            } catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, e + "\nUnbekannter Fehler", überschrift, JOptionPane.ERROR_MESSAGE);
                Bedingung = false;
            }
        }
    }
}