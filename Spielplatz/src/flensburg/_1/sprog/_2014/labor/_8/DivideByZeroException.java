package flensburg._1.sprog._2014.labor._8;

import javax.swing.JOptionPane;

public class DivideByZeroException extends Exception
{
    public static void main(String[] args)
    {
        JOptionPane.showConfirmDialog(null, "Es kann nicht durch 0 geteilt werden!", "Fehler",
                JOptionPane.ERROR_MESSAGE);
    }
}
