package flensburg._1.sprog._2014.labor._8;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class ExtendedCalculatorFakultaet
{
    static String gibEingabeHilfe()
    {
        String eingabe = ("Bitte Operator (+ - * /) plus Zahl eingeben: ");
        return eingabe;
    }

    static boolean ganzZahl(double ersteZahl)
    {
        if (ersteZahl - (int) ersteZahl == 0)
        {
            return true;
        } else
        {
            return false;
        }
    }

    static int fakultaet(int ergebnis) throws OverflowException
    {
        int fakultät = ergebnis;
        for (int counter = 1; counter < ergebnis; counter++)
        {
            fakultät = fakultät * counter;
            if ((Integer.MAX_VALUE / counter) < fakultät)
            {
                throw new OverflowException();
            }
        }
        return fakultät;
    }

    static double potenz(double ersteZahl, double ergebnis) throws InvalidExponentException
    {
        double basis = ergebnis;
        double exponent = ersteZahl;
        if (exponent == 0)
        {
            throw new InvalidExponentException();
        }
        for (int i = 1; i < exponent; i++)
        {
            basis = basis * exponent;
            ersteZahl = basis;
        }
        return ersteZahl;
    }

    public static void main(String[] args)

    {
        String text;
        String überschrift = ("SPROG Aufgabe 8-1: Erweiterter Taschenrechner mit fakultät");
        boolean Bedingung = true;
        Scanner input;
        double ersteZahl = 0.0;
        double ergebnis = 0.0;
        while (Bedingung)
        {
            try
            {
                input = new Scanner(JOptionPane.showInputDialog(null, gibEingabeHilfe() + "\n" + "Aktueller Wert ist "
                        + ergebnis, überschrift, JOptionPane.PLAIN_MESSAGE));
                text = input.nextLine();
                switch (text.charAt(0))
                {
                    case 'e':
                        Bedingung = false;
                        break;
                    case 'c':
                        ersteZahl = 0.0;
                        break;
                    case '+':
                        input = new Scanner(text.substring(1));
                        ersteZahl = input.nextDouble();
                        ersteZahl = ergebnis + ersteZahl;
                        break;
                    case '-':
                        input = new Scanner(text.substring(1));
                        ersteZahl = input.nextDouble();
                        ersteZahl = ergebnis - ersteZahl;
                        break;
                    case '*':
                        input = new Scanner(text.substring(1));
                        ersteZahl = input.nextDouble();
                        ersteZahl = ergebnis * ersteZahl;
                        break;
                    case '/':
                        input = new Scanner(text.substring(1));
                        ersteZahl = (int) input.nextDouble();
                        if (ersteZahl == 0.0)
                        {
                            throw new DivideByZeroException();
                        } else
                        {
                            ersteZahl = (int) ergebnis / ersteZahl;
                        }
                        break;
                    case '!':
                        if (ganzZahl(ergebnis))
                        {
                            ersteZahl = fakultaet((int) ergebnis);
                        } else
                        {
                            throw new NotNaturalNumberException();
                        }
                        break;
                    case '^':
                        ersteZahl = potenz(ergebnis, ersteZahl);
                        break;
                    default:
                        throw new InvalidOperatorException();
                }
                ergebnis = ersteZahl;
            } catch (InvalidExponentException e)
            {//extern gehandlet
            } catch (OverflowException e)
            {//extern gehandlet
            } catch (NotNaturalNumberException e)
            {//extern gehandlet
            } catch (DivideByZeroException e)
            {//extern gehandlet
            } catch (InputMismatchException e)
            {
                JOptionPane.showMessageDialog(null, e + "\n"
                        + "Sie haben keine g�ltige Eingabe vorgenommen.\n Bitte erneut versuchen", überschrift,
                        JOptionPane.ERROR_MESSAGE);
            } catch (InvalidOperatorException e)
            {//extern gehandlet
            } catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, e + "\nUnbekannter Fehler", überschrift, JOptionPane.ERROR_MESSAGE);
                Bedingung = false;
            }
        }
    }
}