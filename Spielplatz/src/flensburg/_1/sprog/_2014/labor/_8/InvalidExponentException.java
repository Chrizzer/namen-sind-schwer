package flensburg._1.sprog._2014.labor._8;

import javax.swing.JOptionPane;


public class InvalidExponentException extends Exception
{
    public InvalidExponentException()
    {
        JOptionPane.showMessageDialog(null,"Der Exponent m�ss gr��er als 0 sein!" ,"Fehler",JOptionPane.ERROR_MESSAGE);
    }
}
