package flensburg._1.sprog._2014.labor._8;

import javax.swing.JOptionPane;


public class InvalidOperatorException extends Exception
{
    public InvalidOperatorException()
    {
        JOptionPane.showMessageDialog(null, "Sie haben einen falschen Operator eingegeben","überschrift",JOptionPane.ERROR_MESSAGE);
    }
}
