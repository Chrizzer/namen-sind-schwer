package flensburg._1.sprog._2014.labor._8;

import javax.swing.JOptionPane;

public class NotNaturalNumberException extends Exception
{
    public NotNaturalNumberException()
    {
       //Fehlermeldung und anschlie�end erneut alten Wert ausgeben
        JOptionPane.showMessageDialog(null,"Die eingegebene Zahl ist nicht nat�rlich.\nBitte neuen Wert einlesen.","Fehler",JOptionPane.ERROR_MESSAGE);
    }
}
