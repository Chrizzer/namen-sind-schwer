package flensburg._1.sprog._2014.labor._8;

import javax.swing.JOptionPane;


public class OverflowException extends Exception
{

    public OverflowException()
    {
            JOptionPane.showMessageDialog(null, "OverflowException ist aufgetreten.", "Fehler", JOptionPane.ERROR_MESSAGE);
    }

}
