package flensburg._1.sprog._2014.labor._9;

import javax.swing.JOptionPane;


public class AbsolutZeroException extends Exception
{
    public AbsolutZeroException()
    {
        String überschrift = ("�bung9-1");
        String hinweis = ("Sie m�ssen Zahlen oder Flie�kommazahlen eingeben!\n"
                + "Die Zahlen d�rfen au�erdem nicht den absoluten Nullpunkt(-273,5�C) unterschreiten!");
        JOptionPane.showMessageDialog(null, hinweis, überschrift, JOptionPane.WARNING_MESSAGE);
    }
}
