package flensburg._1.sprog._2014.labor._9;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class ArraySort
{

    public static double[] sortArray(double[] werte)
    {

        double[] sortierterArray = werte.clone();
        for (int i = 0; i < werte.length - 1; i++)
        {
            for (int j = i + 1; j < werte.length; j++)
            {
                if (werte[i] > werte[j])
                {
                    double speicher = sortierterArray[i];
                    sortierterArray[i] = sortierterArray[j];
                    sortierterArray[j] = speicher;
                }
            }
        }

        return sortierterArray;
    }

    public static void main(String[] args)
    {
        Scanner input;
        double aktuellerWert;
        int arrayLänge;

        String überschrift = ("�bung9-2: ArraySort");

        // Eingabeaufforderung

        input = new Scanner(JOptionPane.showInputDialog(null, "Wieviele Werte m�chten Sie eingeben?", überschrift,
                JOptionPane.PLAIN_MESSAGE));

        arrayLänge = input.nextInt();

        double[] werte = new double[arrayLänge];

        // Array wird eingelesen
        for (int i = 0; i < arrayLänge; i++)
        {
            input = new Scanner(JOptionPane.showInputDialog(null, "Geben sie die verbleibenden " + (arrayLänge - i)
                    + " Werte ein!", überschrift, JOptionPane.PLAIN_MESSAGE));

            aktuellerWert = input.nextDouble();
            werte[i] = aktuellerWert;
        }
        for (int b = 0; b < werte.length; b++)
        {
            JOptionPane.showMessageDialog(null, "unsortierter Array Stelle " + (b+1) + " " + werte[b]
                    + "\nsortierter Array " + sortArray(werte)[b], "�bung9-2: ArraySort", JOptionPane.PLAIN_MESSAGE);
        }
        input.close();
    }
}