package flensburg._1.sprog._2014.labor._9;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class CelsiusToFahrenheit
{

    static double[] celsiusToFahrenheit(double[] celsiusWerte, int arrayWerte)// Array2
                                                                              // f�r
                                                                              // Fahrenheit
                                                                              // bauen
    {
        double[] fahrenheitWerte = new double[arrayWerte];
        // FahrenheitWert = 9/5 * CelsiusWert + 32
        for (int i = 0; i < arrayWerte; i++)
        {
            fahrenheitWerte[i] = (((9 / 5) * celsiusWerte[i]) + 32);
        }
        return fahrenheitWerte;
    }

    public static void main(String[] args)
    {
        String überschrift = ("�bung9-1");
        Scanner input;
        int arrayWerte = 0;
        double absolutZero = -273.5;
        double aktuellerWert = 0.0;
        boolean bedingung = true;
        int counter = 0;
        boolean eingabeKorrekt = true;
        double[] celsiusWerte = null;
        while (bedingung)
        {
            try
            {
                if (counter == 0)// ifBedingung guckt ob schon Werte
                                 // eingegeben wurden
                {
                    input = new Scanner(JOptionPane.showInputDialog(null,
                            "Wieviele Werte m�chten Sie eingeben?\nBITTE KEINE MINUSWERTE", überschrift,
                            JOptionPane.PLAIN_MESSAGE));

                    arrayWerte = input.nextInt();
                    celsiusWerte = new double[arrayWerte];
                }// ifBedingung Ende
                while (eingabeKorrekt)// forSchleife1 -Einlesen
                                      // des Arrays
                {
                    if (counter == arrayWerte)
                    {
                        eingabeKorrekt = false;
                    } else
                    {
                        input = new Scanner(JOptionPane.showInputDialog(null, "Geben sie die verbleibenden "
                                + (arrayWerte - counter) + " Werte ein!", überschrift, JOptionPane.PLAIN_MESSAGE));
                        if (input.hasNextDouble())
                        {
                            aktuellerWert = input.nextDouble();

                            if (aktuellerWert < absolutZero)// Wert darf nicht
                                                            // unter
                            // dem -273,5�C sein
                            {
                                throw new AbsolutZeroException();
                            } else
                            {
                                celsiusWerte[counter] = aktuellerWert;
                                counter++;
                            }
                        } else
                        {
                            throw new Exception();
                        }
                    }
                }// While - Ende
                
                double[] fahrenheitWerte = celsiusToFahrenheit(celsiusWerte, arrayWerte);
                for (int i = 0; i < arrayWerte; i++)// forSchleife2-Ausgabe
                                                    // des Arrays
                {
                    String auslesung = ("Wert " + (i+1) + " ist " + celsiusWerte[i] + " Grad Celsius und dementsprechend "
                            + fahrenheitWerte[i] + " Grad Fahrenheit");
                    JOptionPane.showMessageDialog(null, auslesung, überschrift, JOptionPane.PLAIN_MESSAGE);
                    bedingung = false;
                }// forSchleife2 - Ende
            }// tryBlock - Ende
            catch (AbsolutZeroException e)
            {

            } catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Ung�ltige Eingabe!", überschrift, JOptionPane.WARNING_MESSAGE);
            }
            // bedingung = false;
        }// whileSchleife - Ende
    }// main - Ende
}// class - Ende
