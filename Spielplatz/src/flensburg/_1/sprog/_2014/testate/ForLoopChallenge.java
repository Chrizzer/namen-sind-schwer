package flensburg._1.sprog.testate;

// Zeit für Lösung: 8 Minuten
// Problem: Verrechnet bei ner einfachen Kopfrechnung T_T
public class ForLoopChallenge
{
    public static void main(String[] args)
    {
        for (int i = 15; i < 30;)
        {
            for (int j = 0; j < 45; j = j + 3)
            {
                if (j % 3 == 0)
                {
                    i++;
                }
            }
            System.out.println(i);
        }

    }
}
