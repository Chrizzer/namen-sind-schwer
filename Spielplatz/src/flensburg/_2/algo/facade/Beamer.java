package flensburg._2.algo.facade;

public class Beamer {
	public void ein() {
		System.out.println("Beamer eingeschaltet.");
	}

	public void aus() {
		System.out.println("Beamer ausgeschaltet.");
	}

	public void tvModus() {
		System.out.println("Beamer im TV-Modus");
	}

	public void breitwandModus() {
		System.out.println("Beamer Breitwand-Modus.");
	}
	
	public void setDvdInput() {
		System.out.println("Eingang auf DVD.");
	}

}
