package flensburg._2.algo.facade;

public class Beleuchtung {
	public void ein() {
		System.out.println("Licht an.");
	}

	public void aus() {
		System.out.println("Licht aus.");
	}

	public void dimmen() {
		System.out.println("Licht gedimmt.");
	}
}
