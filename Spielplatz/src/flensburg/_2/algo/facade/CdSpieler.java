package flensburg._2.algo.facade;

public class CdSpieler {
	public void ein() {
		System.out.println("CD-Spieler eingeschaltet.");
	}
	
	public void aus() {
		System.out.println("CD-Spieler ausgeschaltet.");
	}
	
	public void auswerfen() {
		System.out.println("CD auswerfen.");
	}
	
	public void pause() {
		System.out.println("CD pausieren.");
	}
	
	public void spielen() {
		System.out.println("CD abspielen.");
	}
	
	public void anhalten() {
		System.out.println("CD anhalten.");
	}
}
