package flensburg._2.algo.facade;

public class DvdSpieler {
	public void on() {
		System.out.println("DVD-Spieler eingeschaltet.");
	}

	public void off() {
		System.out.println("DVD-Spieler ausgeschaltet.");
	}

	public void eject() {
		System.out.println("DVD auswerfen.");
	}

	public void pause() {
		System.out.println("DVD pausiert.");
	}

	public void play() {
		System.out.println("DVD wird abgespielt.");
	}

	public void setSurroundSound() {
		System.out.println("DVD-Audio: Surround.");
	}

	public void setZweiKanalSound() {
		System.out.println("DVD-Audio: Stereo");
	}

	public void anhalten() {
		System.out.println("DVD-Wiedergabe angehalten.");
	}

}
