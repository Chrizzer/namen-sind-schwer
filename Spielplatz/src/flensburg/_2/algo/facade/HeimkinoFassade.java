package flensburg._2.algo.facade;

public class HeimkinoFassade {

	PopcornMaschine popcorn;
	Beleuchtung beleuchtung;
	Leinwand leinwand;
	Beamer beamer;
	Verstaerker verstaerker;
	DvdSpieler dvd;
	CdSpieler cd;

	public HeimkinoFassade() {
		popcorn = new PopcornMaschine();
		beleuchtung = new Beleuchtung();
		leinwand = new Leinwand();
		beamer = new Beamer();
		verstaerker = new Verstaerker();
		dvd = new DvdSpieler();
		cd = new CdSpieler();
	}

	public void filmSchauen() {
		System.out.println("***** Heimkino-Fassade: filmschauen BEGIN *****");

		popcorn.ein();
		popcorn.starten();

		beleuchtung.dimmen();

		leinwand.runter();

		beamer.ein();
		beamer.breitwandModus();

		verstaerker.ein();
		verstaerker.setDVD();
		verstaerker.setSurroundSound();
		verstaerker.setLautstaerke(5);

		dvd.on();
		dvd.play();

		System.out.println("***** Heimkino-Fassade: filmschauen END *****");

	}

	public void filmBeenden() {
		System.out.println("");
		System.out.println("");
	}

	public void cdHoeren() {
		System.out.println("");
		System.out.println("");
	}

	public void cdBeenden() {
		System.out.println("");
		System.out.println("");
	}

	public void radioHoeren() {
		System.out.println("");
		System.out.println("");
	}

	public void radioBeenden() {
		System.out.println("");
		System.out.println("");
	}
}
