package flensburg._2.algo.facade;

public class Leinwand {
	
	public void hoch() {
		System.out.println("Leinwand eingefahren.");
	}
	
	public void runter() {
		System.out.println("Leinwand bereit.");
	}
	

}
