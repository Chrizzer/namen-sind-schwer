package flensburg._2.algo.facade;

public class PopcornMaschine {
	public void ein() {
		System.out.println("Popcorn-Maschine eingeschaltet.");
	}

	public void aus() {
		System.out.println("Popcorn-Maschine ausgeschaltet.");
	}

	public void starten() {
		System.out.println("Lecker Popcorn.");
	}

}
