package flensburg._2.algo.facade;

public class Tuner {
	public void ein() {
		System.out.println("Tuner eingeschaltet.");
	}

	public void aus() {
		System.out.println("Tuner ausgeschaltet.");
	}

	public void setAM() {
		System.out.println("Modus: Amplituden-Moduliert.");
	}

	public void setFM() {
		System.out.println("Modus: Frequenz-Moduliert.");
	}

	public void setKanal() {
		System.out.println("Kanal neu gesetzt.");
	}

}
