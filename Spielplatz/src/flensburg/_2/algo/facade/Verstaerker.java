package flensburg._2.algo.facade;

public class Verstaerker {
	public void ein() {
		System.out.println("Verstaerker eingeschaltet.");
	}
	
	public void aus() {
		System.out.println("Verstaerker ausgeschaltet.");
	}
	
	public void setCD() {
		System.out.println("Eingang: CD");
	}
	
	public void setDVD() {
		System.out.println("Eingang: DVD");
	}
	
	public void setStereoSound() {
		System.out.println("Soundmodus: Stereo");
	}
	
	public void setSurroundSound() {
		System.out.println("Soundmodus: Surround");
	}
	
	public void setTuner() {
		System.out.println("Eingang: Radio");
	}
	
	public void setLautstaerke(int volume) {
		System.out.println("Lautst�rke angepasst auf: " + volume + ".");
	}
	
}
