package flensburg._2.algo.graphen;

import java.util.ArrayList;
import java.util.Iterator;

public class ConnectedComponents {
	private Graph graph;
	private int n, c;
	private int[] mark;

	public ConnectedComponents(Graph graph_) {
		graph = graph_;
		n = graph.getSize();
		mark = new int[n];
		computeComponents();
	}

	/**
	 * @param c
	 *            : ist der Urpsrungsknoten
	 * @param v
	 *            : Iterator der For-Schleife
	 * @param n
	 *            : Anzahl der Kanten
	 *
	 *            falls eine
	 */
	public void computeComponents() {
		// alle Knotenmarkierungen auf 0 setzen
		clearMarks();

		c = 0;
		// alle Knoten v des Graphen durchlaufen
		for (int v = 0; v < n; v++)
			if (notVisited(v)) {
				c = c + 1; // neue Zusammenhangs�komponente gefunden
				depthFirstSearch(v);
			}
	}

	private void depthFirstSearch(int v) {
		mark[v] = c; // Knoten v mit der Komponenten�nummer c markieren
		System.out.println(v + " " + c);
		// alle Nachbarn w des Knotens v durchlaufen
		int w;
		Iterator<Integer> it = new NeighbourIterator(graph, v);
		while (it.hasNext()) {
			w = it.next();
			if (notVisited(w))
				depthFirstSearch(w);
		}
	}

	// liefert true, wenn der Knoten v noch mit 0 markiert ist
	private boolean notVisited(int v) {
		return mark[v] == 0;
	}

	// alle Knotenmarkierungen auf 0 setzen
	private void clearMarks() {
		for (int v = 0; v < n; v++)
			mark[v] = 0;
	}

	// liefert die Anzahl der Zusammenhangs�komponenten des Graphen
	public int getNumberOfComponents() {
		return c;
	}

	// liefert alle Knoten der Zusammenhangs�komponente i (i=1, 2, ...)
	public ArrayList<Integer> getComponent(int i) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		for (int v = 0; v < n; v++)
			if (mark[v] == i)
				a.add(v);
		return a;
	}

} // end class ConnectedComponents