package flensburg._2.algo.graphen;

import java.util.ArrayList;

public class Graph {
	public int size = 0;
	public ArrayList<ArrayList<Boolean>> arr = new ArrayList<ArrayList<Boolean>>();

	public Graph(int graphSize) {
		size = graphSize;
		for (int i = 0; i < graphSize; i++) {
			arr.add(new ArrayList<Boolean>());

			for (int k = 0; k < graphSize; k++) {
				arr.get(i).add(false);
			}
		}
	}

	public void setEdge(int a, int b) {
		arr.get(a).set(b, true);
		arr.get(b).set(a, true);
	}

	public int getSize() {
		return size;
	}

	public boolean isEdge(int a, int b) {
		return arr.get(a).get(b);
	}
}