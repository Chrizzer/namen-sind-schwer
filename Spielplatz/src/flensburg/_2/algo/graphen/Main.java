package flensburg._2.algo.graphen;

public class Main {

	public static void main(String[] args) {

		int graphSize = 9;

		Graph graph = new Graph(graphSize);

		// ZK 1
		graph.setEdge(0, 5);
		graph.setEdge(0, 4);

		// ZK 2
		graph.setEdge(1, 2);
		graph.setEdge(1, 3);
		graph.setEdge(1, 6);
		graph.setEdge(2, 6);
		graph.setEdge(3, 6);

		// ZK 3
		graph.setEdge(7, 8);

		// Zusammenhangskomponenten ermitteln
		ConnectedComponents ccp = new ConnectedComponents(graph);

		System.out.println(ccp.getComponent(1));
		System.out.println(ccp.getComponent(2));
		System.out.println(ccp.getComponent(3));
		System.out.println(ccp.getComponent(4));
	}
}
