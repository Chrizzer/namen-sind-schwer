package flensburg._2.algo.prototype;

public class ChessPlayingFieldDC {
	public String[][] field;
	
	/**
	 * Parameterloser Konstruktor
	 */
	public ChessPlayingFieldDC() {
		
		// Figuren als Strings in einzelnen Arrays anlegen
		
		String[] row8 = {"Turm ", "Sprin", "L�ufe", "Queen", "King ", "L�ufe", "Sprin", "Turm "};
		String[] row7 = {"Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer"};
		String[] row6 = {"  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  "};
		String[] row5 = {"  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  "};
		String[] row4 = {"  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  "};
		String[] row3 = {"  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  ", "  x  "};
		String[] row2 = {"Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer", "Bauer"};
		String[] row1 = {"Turm ", "Sprin", "L�ufe", "Queen", "King ", "L�ufe", "Sprin", "Turm "};
		
		
		// Reihen als Objekte in einem Array anlegen
		String[][] field = {row1, row2, row3, row4, row5, row6, row7, row8};		
		
		// dieses Array der globalen Variable zuweisen
		this.field = field;
	}
	
	/**
	 * Konstruktor, der ein zweidimensionales Array erwartet.
	 * Dieses stellt das Spielfeld dar.
	 * @param field
	 */
	public ChessPlayingFieldDC(String[][] field) {
		this.field = field;
	}
	
	/**
	 * Gibt Spielfeld als String aus
	 * @return
	 */
	public void printField() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				System.out.print(field[i][j] + " ");
			}
			System.out.println();
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}
	
	/**
	 * Erzeugt tiefe Kopie des eigenen Objekts
	 * @return
	 */
	public ChessPlayingFieldDC getCopyOfField() {
		String[][] newField = new String[8][8];
		for (int i = 0; i < 8; i++) {
			newField[i] = this.field[i].clone();
		}
		
		//String[][] fieldcopy = this.field.clone();
		ChessPlayingFieldDC cpfCopy = new ChessPlayingFieldDC(newField);
		return cpfCopy;
		
	}
}
