package flensburg._2.algo.prototype;

public class Main {
    public static void main(String[] args) {

        // Deep Copy

        // Neues Schachbrett erzeugen
        ChessPlayingFieldDC field1 = new ChessPlayingFieldDC();
        // Schachfeldvariable deklarieren (zunächst noch NULL)
        ChessPlayingFieldDC field2;

        System.out.println("*** 1. Schachbrett nach Erzeugung ***");
        field1.printField();

        // Schachbrett 1 nach Schachbrett 2 kopieren
        field2 = field1.getCopyOfField();

        // Schachbrett 2 ändern
        field2.field[0][1] = "   x  ";
        field2.field[2][2] = "Spring";

        System.out.println("*** 1. Schachbrett nach Änderung ***");
        field1.printField();
        System.out.println("*** 2. Schachbrett nach Änderung ***");
        field2.printField();

        // Shallow Copy

        // Neues Schachbrett erzeugen
        ChessPlayingField field3 = new ChessPlayingField();
        // Schachfeldvariable deklarieren (zunächst noch NULL)
        ChessPlayingField field4;

        System.out.println("*** 3. Schachbrett nach Erzeugung ***");
        field3.printField();

        // Schachbrett 3 nach Schachbrett 4 kopieren
        field4 = field3.getCopyOfField();

        // Schachbrett 4 ändern
        field4.field[0][1] = "   x  ";
        field4.field[2][2] = "Spring";

        System.out.println("*** 3. Schachbrett nach Änderung ***");
        field3.printField();
        System.out.println("*** 4. Schachbrett nach Änderung ***");
        field4.printField();

    }
}
