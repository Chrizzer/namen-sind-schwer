package flensburg._2.algo.singleton;

public class Main {

	public static void main(String[] args) {

		SingletonSimple as;
		SingletonSimple bs;
		SingletonSimple cs;
		
		normalClass an;
		normalClass bn;
		normalClass cn;

		as = SingletonSimple.getInstance();
		bs = SingletonSimple.getInstance();
		cs = SingletonSimple.getInstance();
		
		an = new normalClass();
		bn = new normalClass();
		cn = new normalClass();

		as.setAttribute(1);
		bs.setAttribute(2);
		cs.setAttribute(3);
		
		an.setAttribute(1);
		bn.setAttribute(2);
		cn.setAttribute(3);

		System.out.println("SingletonInstance(a): " + as.getAttribute());
		System.out.println("SingletonInstance(b): " + bs.getAttribute());
		System.out.println("SingletonInstance(c): " + cs.getAttribute());
		
		System.out.println("NormalInstance(a): " + an.getAttribute());
		System.out.println("NormalInstance(b): " + bn.getAttribute());
		System.out.println("NormalInstance(c): " + cn.getAttribute());

	}
}
