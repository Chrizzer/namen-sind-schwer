package flensburg._2.algo.singleton;

public class SingletonSimple {

	private int attribut;
	public String myText;
	
	static SingletonSimple instance = null;

	private SingletonSimple() {
	}

	public static SingletonSimple getInstance() {
		if (instance == null) {
			instance = new SingletonSimple();
		}
		return instance;
	}
	
	public void setAttribute(int i){
		attribut = i;
	}
	
	public int getAttribute() {
		return attribut;
	}
}

