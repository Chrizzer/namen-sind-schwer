package flensburg._2.algo.sortieren;

public abstract class CountingSorter implements Sorter {
	public long counter;

	public CountingSorter() {
	}

	protected int c(int i) {
		counter++;
		return i;
	}

	public long getCounter() {
		return counter;
	}

	public void resetCounter() {
		counter = 0;
	}
}