package flensburg._2.algo.sortieren;

public class InsertionSort extends CountingSorter implements Sorter {
	public InsertionSort() {
	}

	int[] temp;
	int n;

	// sort ruft die Methode InsertionSort auf, in welcher das eigentliche
	// Sortieren stattfindet
	// des Weiteren wird der sortierte Array zwischengespeichert
	@Override
	public void sort(int[] array) {
		temp = array;
		n = array.length;
		insertionSort();
	}

	// InsertionSort wendet den Algorithmus an
	public void insertionSort() {
		int i, j, t;
		for (i = 1; i < n; i++) {
			j = i;
			t = temp[c(j)];
			while (j > 0 && temp[c(j - 1)] > t) {
				temp[c(j)] = temp[c(j - 1)];
				j--;
			}
			temp[c(j)] = t;
		}
	}
}
