package flensburg._2.algo.sortieren;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame {

	static final int R = 10;
	int xOffset = 0;
	int yOffset = 0;
	static final String[] sortierMethode = { "Sorted", "Bound", "Random" };

	public MyFrame() {
		for (int index = 0; index < sortierMethode.length; index++) {
			neuesFenster("QuickSortClassic", index);
			neuesFenster("QuickSortRandomX", index);
			neuesFenster("QuickSortMedian", index);
			neuesFenster("QuickSortCheck", index);
			xOffset += 70;
			if (index == 1) {
				yOffset += 1040;
				xOffset = 0;
			}
		}
	}

	public void neuesFenster(String str, int index) {
		JFrame f = new JFrame("Algorithmen und Datenstrukturen " + str + " "
				+ sortierMethode[index]);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new GridLayout(4, 6));
		String[] kopfZeile = { (R + "^n"), ("Bestzeit in ms"),
				("Schlechteste Zeit in ms"), ("� Durchschnittszeit in ms"),
				(" Gesamtdauer in ms"), ("Counter ") };
		for (int j = 0; j < kopfZeile.length; j++) {
			f.add(zeileErstellen(kopfZeile)[j]);
		}
		for (int i = 3; i <= 5; i++) {
			Uhr t = new Uhr(R, str, sortierMethode[index], i);
			String[] zeilenArray = { R + "^" + Integer.toString(i),
					String.valueOf(t.getBest()), String.valueOf(t.getWorst()),
					String.valueOf(t.getAvg()), String.valueOf(t.getRuntime()),
					String.valueOf((Uhr.tempCounter) / Uhr.n) };
			for (int j = 0; j < zeilenArray.length; j++) {
				f.add(zeileErstellen(zeilenArray)[j]);
			}
			f.setLocation(yOffset, xOffset);
			f.pack();
			f.setVisible(true);
			xOffset += 35;
		}
	}

	public JLabel[] zeileErstellen(String[] strArray) {
		JLabel[] arr = new JLabel[strArray.length];
		for (int i = 0; i < strArray.length; i++) {
			arr[i] = new JLabel();
			arr[i].setText(strArray[i]);
		}
		return arr;
	}
}