package flensburg._2.algo.sortieren;

public class QuickSortCheck extends CountingSorter implements Sorter {

	private int[] a;
	private int n;

	public void sort(int[] a) {
		this.a = a;
		n = a.length;
		if (itsSorted(a)==false)
			quicksort(0, n - 1);
	}

	private void quicksort(int lo, int hi) {
		int i = lo, j = hi;

		// Vergleichselement x
		int x = a[c((lo + hi) / 2)];

		// Aufteilung
		while (i <= j) {
			while (a[c(i)] < x)
				i++;
			while (a[c(j)] > x)
				j--;
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}

		// Rekursion
		if (lo < j)
			quicksort(lo, j);
		if (i < hi)
			quicksort(i, hi);
	}

	private boolean itsSorted(int array[]) {
		boolean dun = true;
		for (int i = 0; i < array.length-1; i++) {
			if (array[c(i)] > array[c(i + 1)]) {
				dun = false;
			}
		}
		return dun;
	}

	private void exchange(int i, int j) {
		int t = a[c(i)];
		a[c(i)] = a[c(j)];
		a[c(j)] = t;
	}
} // end class QuickSorter
