package flensburg._2.algo.sortieren;

public class QuickSortRandomX extends CountingSorter implements Sorter {

	private int[] a;
	private int n;

	public void sort(int[] a) {
		this.a = a;
		n = a.length;
		quicksort(0, n - 1);
	}

	private void quicksort(int lo, int hi) {
		int i = lo, j = hi;

		// Vergleichselement x
		// als Vergleichselement x ein zuf�lliges ELement der Eingabefolge
		// w�hlen.
		// v1
		 int x = a[c((int) (Math.random() * (hi - lo) + lo))];

		// Aufteilung
		while (i <= j) {
			while (a[c(i)] < x)
				i++;
			while (a[c(j)] > x)
				j--;
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}

		// Rekursion
		if (lo < j)
			quicksort(lo, j);
		if (i < hi)
			quicksort(i, hi);
	}

	private void exchange(int i, int j) {
		int t = a[c(i)];
		a[c(i)] = a[c(j)];
		a[c(j)] = t;
	}
} // end class QuickSorter
