package flensburg._2.algo.sortieren;

public interface Sorter {

	public abstract void sort(int[] a);

	public abstract long getCounter();

	public abstract void resetCounter();
}