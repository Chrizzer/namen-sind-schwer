package flensburg._2.algo.sortieren;

public class Uhr {

	public static long best, worst, tempCounter;
	public static double avg;
	final static int n = 1;
	static long runTime = 0;
	static int[] rArray = null;

	public Uhr(int R, String str, String sortierMethode, int p) {
		CountingSorter obj = null;
		switch (str) {
		case "InsertionSort":
			obj = new InsertionSort();
			break;
		case "QuickSortRandomX":
			obj = new QuickSortRandomX();
			break;
		case "QuickSortMedian":
			obj = new QuickSortMedian();
			break;
		case "QuickSortClassic":
			obj = new QuickSortClassic();
			break;
		case "QuickSortCheck":
			obj = new QuickSortCheck();
			break;
		}
		obj.resetCounter();
		long[] run_times = new long[n];
		long start, finish;
		long anfang, ende;
		anfang = start = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			fillArray((int) Math.pow(R, p), sortierMethode);
			start = System.currentTimeMillis();
			obj.sort(rArray);
			finish = System.currentTimeMillis();
			run_times[i] = finish - start;
			tempCounter = obj.getCounter();
		}
		ende = System.currentTimeMillis();
		runTime = ende - anfang;
		best = findMin(run_times);
		worst = findMax(run_times);
		avg = findAverage(run_times);
	}

	public static long findMax(long[] times) {
		long max = times[0];
		for (int i = 1; i < times.length; i++) {
			if (max < times[i]) {
				max = times[i];
			}
		}
		return max;
	}

	public static long findMin(long[] times) {
		long min = times[0];
		for (int i = 1; i < times.length; i++) {
			if (min > times[i]) {
				min = times[i];
			}
		}
		return min;
	}

	public static double findAverage(long[] times) {
		long sum = 0;
		double avg;
		for (int i = 0; i < times.length; i++) {
			sum += times[i];
		}
		avg = (double) sum / (double) times.length;
		return avg;
	}

	public static void fillArray(int potenz, String str) {
		rArray = new int[potenz];
		for (int i = 0; i < rArray.length; i++) {
			switch (str) {
			case "Random":
				// f�llt Array mit Zufallsgenerierten Zahlen, die nicht
				// gr��er ist
				// als die l�nge des Arrays
				rArray[i] = (int) (Math.random() * potenz);
				break;
			case "Sorted":
				// f�llt den Array mit aufeinander folgenden Zahlen
				rArray[i] = i;
				break;
			case "Bound":
				// f�llt Array mit Zufallszahlen von 0-99
				rArray[i] = (int) (Math.random() * 99);
				break;
			}
		}
	}

	public long getRuntime() {
		return runTime;
	}

	public long getBest() {
		return best;
	}

	public long getWorst() {
		return worst;
	}

	public double getAvg() {
		return avg;
	}
}
