package flensburg._2.oop._1;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Aufgabe1 {
	// KLASSEN IMMER GRO�
	public static void main(String[] args) {
		Scanner input = new Scanner(JOptionPane.showInputDialog(null,
				"Bitte Anzahl an Fenstern eingeben.", "OOP-Aufgabe 1",
				JOptionPane.PLAIN_MESSAGE));
		int arrayLänge = input.nextInt();

		MyClass[] fensterArray = new MyClass[arrayLänge];
		for (int i = arrayLänge - 1; i >= 0; i--) {
			fensterArray[i] = new MyClass("Moin", (i * 100 + 200),
					("Fenster " + (i + 1)));
		}
		input.close();
	}
}
/*
 * Scanner input = new Scanner(JOptionPane.showInputDialog(null,
 * "Bitte Anzahl an Fenstern eingeben.", "OOP-Aufgabe 1",
 * JOptionPane.PLAIN_MESSAGE)); int arrayLänge = input.nextInt(); JFrame[]
 * fensterArray = new JFrame[arrayLänge]; for (int stelleImArray = 0;
 * stelleImArray < arrayLänge; stelleImArray++) { JFrame frame = new JFrame();
 * fensterArray[stelleImArray] = frame;
 * fensterArray[stelleImArray].add(myClass.setLabel("Moin"));
 * fensterArray[stelleImArray] .setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 * } // -1, weil 0 die 1. Stelle im Array ist // Array wird gef�llt, mittels
 * For-Schleife for (int stelleImArray = 0; stelleImArray < arrayLänge;
 * stelleImArray++) { int fensterGr��e = stelleImArray * 100 + 200;
 * fensterArray[stelleImArray].setSize(fensterGr��e, fensterGr��e);
 * fensterArray[stelleImArray] .setTitle("Fenster " + stelleImArray);
 * fensterArray[stelleImArray].setLocationRelativeTo(null); } // Array wird
 * sichtbar gemacht durch For-Schleife // Von hinten nach Vorne, weil das
 * neueste Fenster immer vor dem zuvor // ge�ffneten ge�ffnet wird. for (int
 * z�hler = fensterArray.length-1; z�hler > 0; z�hler--) {
 * fensterArray[z�hler].setVisible(true); } // Scanner wird zu gemacht
 */