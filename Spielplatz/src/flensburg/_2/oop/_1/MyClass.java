package flensburg._2.oop._1;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyClass extends JFrame {

    public MyClass(String labelText, int fenstergröße, String fenstername) {
        super();
        JLabel label = new JLabel(labelText);
        add(label);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(fenstergröße, fenstergröße);
        setTitle(fenstername);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
/*
 *
 * Scanner input = new Scanner(JOptionPane.showInputDialog(null,
 * "Bitte Anzahl an Fenstern eingeben.", "OOP-Aufgabe 1",
 * JOptionPane.PLAIN_MESSAGE)); int arrayL�nge = input.nextInt();
 *
 * myClass[] fensterArray=new myClass[arrayL�nge]; for(int
 * i=arrayL�nge-1;i=>0;i--){ int fensterGr��e = i*100+200; fensterArray[i] = new
 * myClass("Moin",fensterGr��e,("Fenster "+ i)); myClass.setVisible(true); }
 */
