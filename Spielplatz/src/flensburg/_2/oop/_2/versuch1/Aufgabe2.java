package flensburg._2.oop._2.versuch1;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Aufgabe2 {

	public static void main(String[] args) {
		int i = 1111;
		int z = 2013;
		String[] bLabelText = { ("Hallo Flensburg!"), ("Hallo Deutschland!"),
				(" Hallo Welt!") };
		String überschriftC = ("Zahlen");
		Scanner input = new Scanner(JOptionPane.showInputDialog("Wieviele Zahlen sollen wiedergegeben werden?"));
		int länge = input.nextInt();
		int beliebigeZahlen[] = new int[länge];		
		for(int arrayLänge=0;arrayLänge<=beliebigeZahlen.length-1;arrayLänge++){
			JOptionPane.showInputDialog(null,"Zahl eingeben");
			beliebigeZahlen[arrayLänge] = input.nextInt();
		}
		int stelleA = 400;
		int stelleB = 500;
		int abstand = 200;
		MyFrameA a = new MyFrameA();
		a.setTitle(i);
		a.setLocation(stelleA, stelleB);

		MyFrameB b = new MyFrameB();
		b.setTitle(z);
		b.setLabel(bLabelText);
		b.setLocation(stelleA+abstand, stelleB);

		MyFrameC c = new MyFrameC(überschriftC,100, beliebigeZahlen);
		c.setLocation(stelleA+abstand*2, stelleB);
		input.close();
	}
}