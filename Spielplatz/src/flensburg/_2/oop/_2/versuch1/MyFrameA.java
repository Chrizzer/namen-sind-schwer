package flensburg._2.oop._2.versuch1;

import javax.swing.JFrame;

public class MyFrameA extends JFrame {
	public int defaultSize=200;
	public MyFrameA() {
		super();
		setSize(defaultSize,defaultSize);
		// super ist zum aufrufen aller extendeten Methoden und dergleichen
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	void setTitle(int i) {
		this.setTitle(String.valueOf(i));
	}

}