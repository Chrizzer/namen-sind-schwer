package flensburg._2.oop._2.versuch1;

import java.awt.FlowLayout;

import javax.swing.JLabel;

public class MyFrameB extends MyFrameA {

	public void setLabel(String[] labelTextArray) {
		setLayout(new FlowLayout());
		for (int counter = 0; counter < labelTextArray.length; counter++) {
			JLabel label = new JLabel(labelTextArray[counter]);
			add(label);
		}
	}
}
