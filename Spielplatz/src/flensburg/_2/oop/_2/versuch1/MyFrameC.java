package flensburg._2.oop._2.versuch1;

import java.awt.FlowLayout;

import javax.swing.JLabel;

public class MyFrameC extends MyFrameB {

	private String a;
	
	public MyFrameC(String a, int größe, int[] beliebigeZahl) {

		this.setTitle(a);
		this.setSize(defaultSize+100, defaultSize+100);
		this.setLayout(new FlowLayout());
		for (int i = 0; i <= beliebigeZahl.length; i++) {
				JLabel label = new JLabel(String.valueOf(beliebigeZahl[i]));
				add(label);
		}
	}
}