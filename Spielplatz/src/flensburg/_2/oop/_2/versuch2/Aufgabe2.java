package flensburg._2.oop._2.versuch2;

public class Aufgabe2 {

	public static void main(String[] args) {
		int i=1111;
		int x=200;
		int z=2013;
		int y=100;
		String bLabelText=("Hallo Flensburg!/nHallo Deutschland!/nHallo Welt!");
		String überschriftC=("Zahlen");
		
		MyFrameA a = new MyFrameA();
		a.setTitle(i);
		a.setSize(x,x);
		
		MyFrameB b = new MyFrameB();
		b.setTitle(z);
		b.setSize(x,x);
		b.setLabel(bLabelText);
		
		MyFrameC c= new MyFrameC(überschriftC, x, y);
		
		a.setVisible(true);
		b.setVisible(true);
		c.setVisible(true);
	}
}