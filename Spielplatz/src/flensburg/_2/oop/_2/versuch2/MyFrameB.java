package flensburg._2.oop._2.versuch2;

import javax.swing.JLabel;

public class MyFrameB extends MyFrameA {

	public MyFrameB() {
		super();
	}

	public void setLabel(String i) {
		JLabel label = new JLabel(i);
		add(label);
	}
}
