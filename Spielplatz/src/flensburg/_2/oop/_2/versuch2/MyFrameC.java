package flensburg._2.oop._2.versuch2;

import javax.swing.JLabel;

public class MyFrameC extends MyFrameB {

	private String a;

	public MyFrameC() {
		super();
	}

	public MyFrameC(String a, int x, int y) {

		setTitle(a);
		setSize(x, x);
		for (int i = 0; i < y; i++) {
			if (i % 2 != 1) {
				this.a = (i + " " + this.a);
			}
		}
		JLabel label = new JLabel(this.a);
		add(label);
	}

}
