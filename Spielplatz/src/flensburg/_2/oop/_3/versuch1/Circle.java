package flensburg._2.oop._3.versuch1;

import java.awt.Graphics;

public class Circle extends GeometricObject{
	
	public Circle()
	{
		super();
	}
	protected void paintComponent(Graphics g) {
		System.out.println("Wird gemahlt 1");
		super.paintComponent(g);
		g.setColor(setColor());
		g.fillOval(10, 15, 100, 100);
	}
}
