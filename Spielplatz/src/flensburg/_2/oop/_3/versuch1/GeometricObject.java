package flensburg._2.oop._3.versuch1;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class GeometricObject extends JPanel {

	public GeometricObject(){
		
	}
	public GeometricObject(Component form, JPanel panel) {
		panel.add(new JPanel());
		add(form);
		BorderFactory.createLineBorder(Color.RED);
		validate();
	}

	public Color setColor() {
		Random r = new Random();
		ArrayList<Integer> rgb = new ArrayList<Integer>(255);
		for (int i = 0; i < 3; i++) {
			rgb.add(r.nextInt(255) + 1);
		}
		return new Color(rgb.get(0), rgb.get(1), rgb.get(2));
	}

	public void setRelativeSize() {
		
	}
}