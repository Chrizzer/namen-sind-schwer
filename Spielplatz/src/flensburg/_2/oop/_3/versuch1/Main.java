package flensburg._2.oop._3.versuch1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JPanel {

	public static void main(String[] args) {
		JFrame fenster = new JFrame();
		JPanel layoutPanel = new JPanel();
		int hgap = 0;
		int vgap = 0;
		// FENSTER-------------------------------------------------------------
		fenster.setTitle("Hauptfenster");
		fenster.setLocationRelativeTo(null);
		fenster.setSize(500, 500);
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenster.setVisible(true);
		fenster.add(layoutPanel, BorderLayout.CENTER);
		// JPANEL im FENSTER
		// --------------------------------------------------------------------
		layoutPanel.setLayout(new GridLayout(2, 3, hgap, vgap));
		layoutPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		// JPANEL f�llen
		// ---------------------------------------------------------------------
		for (int i = 0; i <= 4; i++) {
			layoutPanel.add(new GeometricObject(getForm(i),layoutPanel));
		}
		fenster.revalidate();
	}
	public static Component getForm(int i){
		Component form = null;
		switch (i) {
		case 0:
		//	Square quadrat = new Square();
			form = new Square();
			break;
		case 1:
		//	Circle kreis = new Circle();
			form = new Circle();
			break;
		case 2:
		//	Triangle dreieck = new Triangle();
			form = new Triangle();
			break;
		case 3:
		//	Pacman pacman = new Pacman();
			form = new Pacman();
			break;
		case 4:
		//	MySpecialShape shape = new MySpecialShape();
			form = new MySpecialShape();
			break;
		default:
			System.out.println("Fehler im Switchcase "+i);
			break;
		}
		return form;
	}
}