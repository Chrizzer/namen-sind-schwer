package flensburg._2.oop._3.versuch1;

import java.awt.Graphics;
import java.util.Random;

public class MySpecialShape extends GeometricObject {

	public MySpecialShape(){
		super();
	}
	
	protected void paintComponent(Graphics g) {
		System.out.println("Wird gemahlt 4");
		Random r = new Random();
		super.paintComponent(g);

		switch (r.nextInt(4)) {
		case 1:
			g.fillArc(r.nextInt(125), r.nextInt(125), r.nextInt(125),
					r.nextInt(125), r.nextInt(360), r.nextInt(360));
			break;
		case 2:
			g.fillOval(r.nextInt(125), r.nextInt(125), r.nextInt(125),
					r.nextInt(125));
			break;
		case 3:
			g.fillRect(r.nextInt(125), r.nextInt(125), r.nextInt(125),
					r.nextInt(125));
			break;
		case 4:
			int[] triangleX = { r.nextInt(125), r.nextInt(125), r.nextInt(125) };
			int[] triangleY = { r.nextInt(125), r.nextInt(125), r.nextInt(125) };
			int n = r.nextInt(9);
			g.fillPolygon(triangleX, triangleY, n);
			break;
		default:
			break;

		}
		g.setColor(setColor());
	}
}