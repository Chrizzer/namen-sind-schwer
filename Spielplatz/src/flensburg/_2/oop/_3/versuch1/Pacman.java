package flensburg._2.oop._3.versuch1;

import java.awt.Graphics;


public class Pacman extends GeometricObject{
	
	public Pacman(){
		super();
	}

	protected void paintComponent(Graphics g) {
		System.out.println("Wird gemahlt 3");
		super.paintComponent(g);
		g.setColor(setColor());
		g.fillArc(10, 15, 100, 100, 45, 270);
	}
}