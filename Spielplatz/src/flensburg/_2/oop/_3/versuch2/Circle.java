package flensburg._2.oop._3.versuch2;

import java.awt.Graphics;

public class Circle extends GeometricObject {

	public Circle() {
	}
	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		g.fillOval(width, height, x, y);
	}
}