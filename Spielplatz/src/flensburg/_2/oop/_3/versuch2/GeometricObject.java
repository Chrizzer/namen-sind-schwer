package flensburg._2.oop._3.versuch2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JLabel;

abstract public class GeometricObject extends JLabel {
	int rot, gelb, blau;

	public GeometricObject() {
		rot = r.nextInt(255) + 1;
		gelb = r.nextInt(255) + 1;
		blau = r.nextInt(255) + 1;
	}

	Random r = new Random();
	double relativeSize = Math.max(Math.random(), 0.5);
	Color tempcolor;

	abstract void specialPaint(Graphics g, int x, int y, int width, int height);

	public void paint(Graphics g) {
		super.paint(g);
		int width = getWidth();
		int height = getHeight();
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, width, height);

		g.setColor(new Color(255, 0, 0));
		g.drawRect(0, 0, (width - 1), (height - 1));

		int platz = Math.min(width, height);
		int side = (int) Math.round(platz * getRelativeSize());
		int actualHeight = (height - side) / 2;
		int actualWidth = (width - side) / 2;
		setColor(rot, gelb, blau);
		g.setColor(tempcolor);
		specialPaint(g, side, side, actualWidth, actualHeight);
		validate();
		repaint();
	}

	public void setColor(int r, int g, int b) {
		tempcolor = new Color(r, g, b);
	}

	public void setRelativeSize(double relativeSize) {
		this.relativeSize = relativeSize;
	}

	public double getRelativeSize() {
		return relativeSize;
	}
}