package flensburg._2.oop._3.versuch2;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {

	public MyFrame() {
		JFrame fenster = new JFrame();
		JPanel layoutPanel = new JPanel();
		int hgap	= 0, 	vgap = 0;
		int reihe 	= 8, 	spalte = 8;
		int max 	= 5,	min = 1;

		fenster.setTitle("�bung 3");
		fenster.setLocationRelativeTo(null);
		fenster.setSize(500, 500);
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenster.add(layoutPanel, BorderLayout.CENTER);
		layoutPanel.setLayout(new GridLayout(reihe, spalte, hgap, vgap));
		layoutPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		fenster.setVisible(true);
		GeometricObject[] abc = new GeometricObject[reihe * spalte];

		Random r = new Random();
		for (int i = 0; i < abc.length; i++) {
			switch (r.nextInt(max) + min) {
			case 1:
				abc[i] = new Square();
				break;
			case 2:
				abc[i] = new Circle();
				break;
			case 3:
				abc[i] = new Triangle();
				break;
			case 4:
				abc[i] = new Pacman();
				break;
			case 5:
				abc[i] = new MySpecialShape();
				break;
			default:
				break;
			}
			layoutPanel.add(abc[i]);
			System.out.println(i);
		}
		validate();
	}
}