package flensburg._2.oop._3.versuch2;

import java.awt.Graphics;
import java.util.Random;

public class MySpecialShape extends GeometricObject {
	Random r;
	int a, b;

	public MySpecialShape() {
		r = new Random();
		a=r.nextInt(360);
		b=r.nextInt(360);
	}

	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		g.fillArc(width, height, x, x, a, b);
	}
}