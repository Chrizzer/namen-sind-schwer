package flensburg._2.oop._3.versuch2;

import java.awt.Graphics;

public class Pacman extends GeometricObject {

	public Pacman() {
	}

	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		g.fillArc(width, height, x, x, 45, 270);
	}
}