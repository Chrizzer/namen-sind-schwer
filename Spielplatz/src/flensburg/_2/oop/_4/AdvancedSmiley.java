package flensburg._2.oop._4;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

public class AdvancedSmiley extends Smiley {

	Image happy = Toolkit.getDefaultToolkit().getImage("image/happy.jpg");
	Image neutral = Toolkit.getDefaultToolkit().getImage("image/neutral.jpg");
	Image sad = Toolkit.getDefaultToolkit().getImage("image/sad.jpg");

	AdvancedSmiley() {
	}

	@Override
	public void specialPaint(Graphics g, int x, int y, int width, int height) {
		switch (myMood) {
		case HAPPY:
			// :D
			g.drawImage(happy, width, height, x, y, this);
			break;
		case NEUTRAL:
			// :|
			g.drawImage(neutral, width, height, x, y, this);
			break;
		case SAD:
			// D:
			g.drawImage(sad, width, height, x, y, this);
			break;
		}
	}
}