package flensburg._2.oop._4;

import java.awt.Graphics;

public class Circle extends GeometricObject {

	public Circle() {
	}

	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		g.fillOval(width, height, x, y);
	}
}