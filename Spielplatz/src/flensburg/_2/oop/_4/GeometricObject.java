package flensburg._2.oop._4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;

import javax.swing.JPanel;

abstract public class GeometricObject extends JPanel implements MouseListener,
		MouseMotionListener {
	int rot, gelb, blau;
	Random r = new Random();
	double relativeSize = Math.max(Math.random(), 0.5);
	Color tempcolor = randomColor();

	public GeometricObject() {
		rot = r.nextInt(255) + 1;
		gelb = r.nextInt(255) + 1;
		blau = r.nextInt(255) + 1;
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	abstract void specialPaint(Graphics g, int x, int y, int width, int height);

	public void paint(Graphics g) {
		super.paint(g);
		int width = getWidth();
		int height = getHeight();
		int platz = Math.min(width, height);
		int side = (int) Math.round(platz * getRelativeSize());
		int actualHeight = (height - side) / 2;
		int actualWidth = (width - side) / 2;
		
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, width, height);

		g.setColor(new Color(255, 0, 0));
		g.drawRect(0, 0, (width - 1), (height - 1));

		g.setColor(this.tempcolor);
		specialPaint(g, side, side, actualWidth, actualHeight);
	}

	public Color randomColor() {
		int rot = r.nextInt(255);
		int gelb = r.nextInt(255);
		int blau = r.nextInt(255);
		return new Color(rot, gelb, blau);
	}

	public void setColor(Color color) {
		this.tempcolor = color;
		repaint();
	}

	public void setRelativeSize(double relativeSize) {
		this.relativeSize = relativeSize;
	}

	public double getRelativeSize() {
		return relativeSize;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		setRelativeSize(Math.random());
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// Auto-generated method stub
	}
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// Auto-generated method stub
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// nothing
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// nothing
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		setColor(randomColor());
		repaint();
	}
}