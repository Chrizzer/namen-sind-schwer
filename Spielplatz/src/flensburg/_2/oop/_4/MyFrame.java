package flensburg._2.oop._4;

import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	public MyFrame() {
		int reihe 	= 8, 	spalte = 8;
		int max 	= 7,	min = 1;

		setTitle("übung 4");
		setLocationRelativeTo(null);
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(reihe, spalte));
		setVisible(true);
		GeometricObject[] abc = new GeometricObject[reihe * spalte];
		
		Random r = new Random();
		for (int i = 0; i < abc.length; i++) {
			switch (r.nextInt(max) + min) {
			case 1:
				abc[i] = new Square();
				break;
			case 2:
				abc[i] = new Circle();
				break;
			case 3:
				abc[i] = new Triangle();
				break;
			case 4:
				abc[i] = new Pacman();
				break;
			case 5:
				abc[i] = new MySpecialShape();
				break;
			case 6:
				abc[i] = new Smiley();
				break;
			default:
				abc[i] = new AdvancedSmiley();
				break;
			}
			add(abc[i]);
		}
	
		validate();
	}
}