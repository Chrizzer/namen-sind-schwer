package flensburg._2.oop._4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Smiley extends Circle {

	Smiley() {

	}

	enum Mood {
		HAPPY, NEUTRAL, SAD;
	}

	protected Mood myMood = getMood();

	public void setMood(Mood mood) {
		myMood = mood;
	}

	public Mood getMood() {
		Random r = new Random(2);
		switch (r.nextInt(2)) {
		case 1:
			myMood = Mood.HAPPY;
			break;
		case 2:
			myMood = Mood.NEUTRAL;
			break;
		default:
			myMood = Mood.SAD;
			break;
		}
		return myMood;
	}

	public Mood changeMood(Mood myMood) {
		if (myMood == Mood.NEUTRAL) {
			switch (r.nextInt(2)) {
			case 1:
				setMood(Mood.HAPPY);
				break;
			default:
				setMood(Mood.SAD);
				break;
			}
		} else if (myMood == Mood.SAD || myMood == Mood.HAPPY) {
			setMood(Mood.NEUTRAL);
		}
		return myMood;
	}

	@Override
	public void specialPaint(Graphics g, int x, int y, int width, int height) {
		//Kreis
		super.specialPaint(g, x, y, width, height);
		// Augen
		g.setColor(new Color(255, 255, 255));
		g.fillArc(width + x / 3, height + y / 3, x / 5, y / 5, 0, 360);
		g.fillArc(width + x * 2 / 3, height + y / 3, x / 5, y / 5, 0, 360);
		// Mund
		switch (myMood) {
		case HAPPY:
			// :D
			g.fillArc(width + x / 4, height + y * 2 / 4, x / 2, y / 3, 180, 180);
			break;
		case NEUTRAL:
			// :|
			g.drawLine(width + x / 4, height + y * 2 / 3, width + x * 3 / 4,
					height + y * 2 / 3);
			break;
		case SAD:
			// D:
			g.fillArc(width + x / 4, height + y * 2 / 3, x / 2, y / 4, 0, 180);
			break;
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		changeMood(myMood);
		repaint();
	}
}
