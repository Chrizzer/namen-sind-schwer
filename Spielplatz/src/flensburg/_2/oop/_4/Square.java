package flensburg._2.oop._4;

import java.awt.Graphics;

public class Square extends GeometricObject {

	public Square() {
	}

	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		g.fillRect(width, height, x, x);
	}
}