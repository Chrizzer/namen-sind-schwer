package flensburg._2.oop._4;

import java.awt.Graphics;

public class Triangle extends GeometricObject {

	public Triangle() {
	}

	@Override
	void specialPaint(Graphics g, int x, int y, int width, int height) {
		int n = 3;
		int[] triangleX2 = { width, width + x / 2, width + x };
		int[] triangleY2 = { height + y, height, height + y };
		g.fillPolygon(triangleX2, triangleY2, n);
	}
}