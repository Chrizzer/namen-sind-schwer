package flensburg._2.oop._5;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Bild extends JPanel implements MouseWheelListener,
		MouseMotionListener, MouseListener {

	public Bild() {
		addMouseWheelListener(this);
		addMouseMotionListener(this);
		addMouseListener(this);
	}

	File myPictureDirectory = new File("pictures");
	Color borderColor = new Color(R, G, B);
	double zoom = 1;
	int picIndex = 0;
	double degrees = 0;
	static int R = 255;
	static int G = 0;
	static int B = 0;

	public void resetImage() {
		vektorX = 0;
		vektorY = 0;
		zoom = 1;
	}

	public int getWhichPicture() {
		return picIndex;
	}

	public void setWhichPicture(int picIndex) {
		this.picIndex = picIndex;
	}

	public double getZoom() {
		return zoom;
	}

	public void setZoom(double zoom) {
		this.zoom = Math.min(Math.max(0.2, zoom), 4);
	}

	public void setDefaultZoom() {
		this.zoom = 1;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color c) {
		this.borderColor = c;
		repaint();
	}

	public File getMyPictureDirectory() {
		return myPictureDirectory;
	}

	public void setMyPictureDirectory(File myPictureDirectory) {
		this.myPictureDirectory = myPictureDirectory;
	}

	int imgWidth, imgHeight, pWidth, pHeight, abstand = 2;

	public void paint(Graphics g) {
		super.paint(g);
		pWidth = getWidth();
		pHeight = getHeight();

		Image image = getImage(picIndex);
		imgWidth = (int) (image.getWidth(null) * zoom);
		imgHeight = (int) (image.getHeight(null) * zoom);
		int restHeight = (pHeight - imgHeight) / 2;
		int restWidth = (pWidth - imgWidth) / 2;
		int imgX = restWidth + vektorX;
		int imgY = restHeight + vektorY;
		if(degrees == 0){
			g.drawImage(image, imgX, imgY, imgWidth, imgHeight, this);

			g.setColor(borderColor);
			g.drawRect(imgX, imgY, imgWidth - abstand, imgHeight - abstand);
		} else {
			Graphics2D g2d = (Graphics2D)g;
			g.translate(pWidth/2, pHeight/2);
			g2d.rotate(Math.toRadians(degrees));
			g2d.drawImage(image, -imgWidth/2, -imgHeight/2, imgWidth, imgHeight, this);

			g2d.setColor(borderColor);
			g2d.drawRect(-imgWidth/2, -imgHeight/2, imgWidth - abstand, imgHeight - abstand);
		}


	}

	public Image getImage(int n) {
		File[] myFiles = myPictureDirectory.listFiles();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Pictures", "jpg", "jpeg", "png", "bmp");
		int directoryLength = 0;
		for (int i = 0; i < myFiles.length; i++) {
			if (filter.accept(myFiles[i]))
				directoryLength++;
		}

		if (n < 0)
			n = directoryLength - 1 + (n % (directoryLength - 1));

		int file = 0;
		int counter = 0;
		while (true) {
			if (file >= directoryLength)
				file = 0;

			boolean fileIsPicture = filter.accept(myFiles[file]);
			if (fileIsPicture && (counter >= n)) {
				break;
			} else if (counter == n) {
				file++;
			} else if (fileIsPicture) {
				file++;
				counter++;
			} else {
				file++;
			}
		}
		return Toolkit.getDefaultToolkit().getImage(myFiles[file].toString());
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches = e.getWheelRotation();
		if (notches < 0) {
			setZoom(getZoom() + 0.1);
			repaint();
		} else {
			setZoom(getZoom() - 0.1);
			repaint();
		}
	}

	int mouseX1, mouseY1;
	int mouseX2, mouseY2;
	int vektorX, vektorY;

	@Override
	public void mouseDragged(MouseEvent e) {
		// Vektor
		// hinterer Punkt minus vorederPunkt
		vektorX = vektorX + e.getX() - mouseX1;
		vektorY = vektorY + e.getY() - mouseY1;
		repaint();
		// Hinterer Punkt wird Vorderer Punkt
		mouseX1 = e.getX();
		mouseY1 = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// vorderer Punkt
		mouseX1 = e.getX();
		mouseY1 = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}
}
