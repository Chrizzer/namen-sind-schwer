package flensburg._2.oop._5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyFrame extends JFrame implements ChangeListener, ActionListener {

	Bild pic = new Bild();
	int a, b, c;

	JButton next = new JButton("Weiter");
	JButton previous = new JButton("Zur�ck");
	JButton zoomOut = new JButton("Rauszoomen");
	JButton zoomReal = new JButton("Zur�cksetzen");
	JButton zoomIn = new JButton("Reinzoomen");

	JSlider rSlider = new JSlider(0, 255, 255);
	JSlider gSlider = new JSlider(0, 255, 0);
	JSlider bSlider = new JSlider(0, 255, 0);
	JSlider rotate = new JSlider(0,360,0);

	JPanel colorPanel = new JPanel();

	public MyFrame() {

		setTitle("�bung 5: Bildbetrachter");
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new BorderLayout());

		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.BLACK);

		next.addActionListener(this);
		previous.addActionListener(this);
		zoomOut.addActionListener(this);
		zoomReal.addActionListener(this);
		zoomIn.addActionListener(this);
		
		rotate.addChangeListener(this);

		rSlider.addChangeListener(this);
		gSlider.addChangeListener(this);
		bSlider.addChangeListener(this);

		buttonPanel.add(previous);
		buttonPanel.add(zoomOut);
		buttonPanel.add(zoomReal);
		buttonPanel.add(zoomIn);
		buttonPanel.add(next);
		buttonPanel.add(rotate);

		add(pic, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		add(colorPanel, BorderLayout.NORTH);
		JMenuBar myMenuBar = new JMenuBar();
		JMenu myMenu = new JMenu("Optionen");
		JMenuItem myMenuItem = new JMenuItem("Verzeichnis ausw�hlen");
		JMenuItem myColorItem = new JMenuItem("Randfarbe ausw�hlen");
		myMenuBar.add(myMenu);
		myMenu.add(myMenuItem);
		myMenu.add(myColorItem);
		setJMenuBar(myMenuBar);
		myMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser myFileDialog = new JFileChooser();

				myFileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				int result = myFileDialog.showDialog(null, "Choose Directory");

				if (result == JFileChooser.APPROVE_OPTION) {
					pic.setMyPictureDirectory(myFileDialog.getSelectedFile());
					repaint();
				}
			}
		});
		myColorItem.addActionListener(this);
		validate();
		setVisible(true);
	}

	public void stateChanged(ChangeEvent ce) {
		if (ce.getSource() == rSlider) {
			createColor('r', rSlider.getValue());
		} else if (ce.getSource() == gSlider) {
			createColor('g', gSlider.getValue());
		} else if (ce.getSource() == bSlider) {
			createColor('b', bSlider.getValue());
		} else if (ce.getSource() == rotate) {
			pic.degrees = rotate.getValue();
			repaint();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == next) {
			int thisPicture = pic.getWhichPicture();
			pic.setWhichPicture(++thisPicture);
			pic.resetImage();
			pic.repaint();
		} else if (e.getSource() == previous) {
			int thisPicture = pic.getWhichPicture();
			pic.setWhichPicture(--thisPicture);
			pic.resetImage();
			pic.repaint();
		} else if (e.getSource() == zoomOut) {
			pic.setZoom(pic.getZoom() - 0.1);
			pic.repaint();
		} else if (e.getSource() == zoomReal) {
			pic.resetImage();
			pic.repaint();
		} else if (e.getSource() == zoomIn) {
			pic.setZoom(pic.getZoom() + 0.1);
			pic.repaint();
		} else {
			colorPanel.add(new JLabel("Farbe einstellen R - G - B"));
			rSlider.setMajorTickSpacing(30);
			gSlider.setMajorTickSpacing(30);
			bSlider.setMajorTickSpacing(30);
			rSlider.setMinorTickSpacing(15);
			gSlider.setMinorTickSpacing(15);
			bSlider.setMinorTickSpacing(15);
			rSlider.setPaintTicks(true);
			gSlider.setPaintTicks(true);
			bSlider.setPaintTicks(true);
			rSlider.setPaintLabels(true);
			gSlider.setPaintLabels(true);
			bSlider.setPaintLabels(true);
			colorPanel.add(rSlider);
			colorPanel.add(gSlider);
			colorPanel.add(bSlider);
			validate();
		}
	}

	public void createColor(char chr, int x) {
		switch (chr) {
		case 'r':
			a = x;
			break;
		case 'g':
			b = x;
			break;
		case 'b':
			c = x;
			break;
		}
		Color k = new Color(a, b, c);
		pic.setBorderColor(k);
	}
}
