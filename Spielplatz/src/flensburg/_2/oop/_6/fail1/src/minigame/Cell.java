package flensburg._2.oop._6.fail1.src.minigame;

public class Cell {
	boolean visited = false;

	public void setVisited(boolean b) {
		visited = b;
	}

	public boolean getVisited() {
		return visited;
	}
}