package flensburg._2.oop._6.fail1.src.minigame;

import fhfl.miniGame.engine.Sprite;

public abstract class CustomSprite extends Sprite {
	protected int speed;
	protected static int size_X = 32;

	CustomSprite(MyMiniGame game) {
		super(size_X, size_X, game);
	}

	public void setSpeed(int i) {
		speed = i;
	}

	public int getSpeed() {
		return speed;
	}
}