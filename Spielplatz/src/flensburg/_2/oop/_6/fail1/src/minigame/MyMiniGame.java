package flensburg._2.oop._6.fail1.src.minigame;

import java.util.Random;

import fhfl.miniGame.engine.Background;
import fhfl.miniGame.engine.MiniGame;
import fhfl.miniGame.engine.Sprite;

//This is the implementation of YOUR mini game...
public class MyMiniGame extends MiniGame {
	public MyMiniGame() {
	}

	Background bg = getBackgroundPicture();
	Player p1 = new Player(this);
	int[] coords = new int[640 / 32];
	Enemy[] foes;
	boolean[] bArray = new boolean[640 / 32];
	Random r = new Random();
	int min = 0;
	private static int difficulty = 0, computerScore = 0, playerScore = 0;

	@Override
	protected void initGame() {
		p1.setHp(100);
		foes = new Enemy[getDif()];
		bg.clear();
		fillCoords();
		drawLabyrinth();
	}

	@Override
	protected void gameHasStarted() {
		p1.setPosition(coords[5], coords[16]);
		createEnemies();
		setPlayerScore(p1.getHp());
	}

	@Override
	protected void gameUpdate() {
		boolean op = foes[getEnemy()].overlapsSprite(p1);
		for (int i = 0; i < foes.length; i++) {
			if (op) {
				p1.takeDmg(5 * getDif());
				addComputerScore(100 - p1.getHp());
				op = false;
			}
		}
		setPlayerScore(p1.getHp());
	}

	@Override
	protected void gameHasFinished() {
		// Sprites h�ren auf sich zu bewegen.
		p1.stopAnimation();
		p1.dontShow();
		for (int i = 0; i < foes.length; i++) {
			foes[i].stopAnimation();
			foes[i].dontShow();
		}
		endScreen();
	}

	@Override
	protected void playerMove(Action action) {
		makeMove(p1, action);
	}

	@Override
	protected void computerMove(Action action) {
		makeMove(foes[getEnemy()], action);
	}

	/**
	 * 
	 * @param p
	 * @param action
	 */
	public void makeMove(Sprite p, Action action) {
		int[] a = getCoords();
		int x = Math.max(findCurrentX(p), 1);
		int y = Math.max(findCurrentY(p), 1);
		if (p.animationHasFinished())
			if (validMove(y, x))
				switch (action) {
				case UP:
					p.animateTo(a[x], a[y - 1], 150);
					break;
				case DOWN:
					p.animateTo(a[x], a[y + 1], 150);
					break;
				case LEFT:
					p.animateTo(a[x - 1], a[y], 150);
					break;
				case RIGHT:
					p.animateTo(a[x + 1], a[y], 150);
					break;
				case GO:
					if (p.toString().equals("player")) {

					} else {
						foes[getEnemy()].animateTo(p1.getXPosition(),
								p1.getYPosition(), foes[getEnemy()].getSpeed());
					}
					break;
				}
	}

	/**
	 * 
	 * Eckpunkte 640x640
	 */
	public void drawLabyrinth() {
		bg.clear();
		bg.fill(0, 0, 0);
		int width = bg.getWidth(), height = bg.getHeight();
		Cell[][] c = new Cell[coords.length][coords.length];
		for (int x = 0; x < coords.length; x++) {
			for (int y = 0; y < coords.length; y++) {
				c[x][y] = createCell(coords[x], coords[y], width
						/ coords.length, height / coords.length, 1);
				c[x][y].setVisited(false);
			}
		}
	}

	public void endScreen() {
		bg.clear();
		bg.fill(255, 255, 255);
		if (p1.getHp() > 0)
			bg.paintString(coords[coords.length / 2],
					coords[coords.length / 2], "Winner :" + "Player 1!", 0, 0,
					0);
		else {
			bg.paintString(coords[coords.length / 2],
					coords[coords.length / 2], "You have died", 0, 0, 0);
			bg.paintString(p1.getXPosition(), p1.getYPosition(), "R.I.P", 0, 0,
					0);
			kreis(p1.getXPosition(), p1.getYPosition());
		}
	}

	/**
	 * 
	 * Erstellt eine Zelle f�r das Spielfeld
	 * 
	 * @param x
	 *            : Anfangswert X nach rechts
	 * @param y
	 *            : Anfangswert Y nach unten
	 * @param width
	 *            : breite der Zelle
	 * @param height
	 *            : h�he der Zelle
	 * @param rand
	 *            : dicke des Rands
	 */
	private Cell createCell(int x, int y, int width, int height, int rand) {
		Cell c = new Cell();
		bg.paintRectangle(x, y, width, height, rand, 255, 255, 255);
		return c;
	}

	/**
	 * 
	 * @param x
	 *            : Index des Arrays {@link coords} nach rechts
	 * @param y
	 *            : Index des Arrays {@link coords} nach unten
	 */
	public void kreis(int x, int y) {
		// x Nach Rechts und y nach unten
		bg.paintEllipse(coords[x], coords[y], 32, 32, 1, 255, 255, 255);
	}

	/**
	 * Array auff�llen in 32er Schritten, da coords[640/32] ist
	 */
	public void fillCoords() {
		int min = 0;
		for (int i = 0; i < coords.length; i++) {
			coords[i] = min;
			min += 32;
		}
	}

	public int[] getCoords() {

		return coords;
	}

	/**
	 * Erzeugt Gegner, je nach L�nge des zuvor initialisierten Gegner Arrays
	 */
	private void createEnemies() {
		for (int i = 0; i < foes.length; i++) {
			foes[i] = new Enemy(this);
			foes[i].setPosition(coords[i], coords[2 + i]);
		}
	}

	public int getEnemy() {
		int temp = r.nextInt(foes.length);
		return temp;
	}

	@Override
	public int getNrOfPlayerGoActionsMax(int difficulty) {
		MyMiniGame.difficulty = difficulty;
		return 5;
	}

	public int getDif() {
		return MyMiniGame.difficulty;
	}

	@Override
	public int getNrofComputerGoActions(int difficulty) {
		// Berechnungen f�r Schwierigkeitsgradder Gegner
		Enemy.setDif(difficulty);
		return 5 * difficulty;
	}

	public void setPlayerScore(int i) {
		playerScore = i;
	}

	public void setComputerScore(int i) {
		computerScore = i;
	}

	public void subtractPlayerScore(int i) {
		playerScore -= i;
	}

	public void addComputerScore(int i) {
		computerScore += i;
	}

	@Override
	public int getCurrentComputerScore() {
		return computerScore;
	}

	@Override
	public int getCurrentPlayerScore() {
		return playerScore;
	}

	/**
	 * 
	 * @param p
	 *            : Sprite p
	 * @return finds and returns current X-Value's Index in{@link coords}
	 */
	public int findCurrentX(Sprite p) {
		int[] pos = getCoords();
		int current = 0;
		// search
		for (int x = 0; x < coords.length; x++) {
			if (pos[x] == p.getXPosition())
				current = x;
		}
		return current;
	}

	public int findCurrentY(Sprite p) {
		int[] pos = getCoords();
		int current = 0;
		// search
		for (int y = 0; y < coords.length; y++) {
			if (pos[y] == p.getYPosition())
				current = y;
		}
		return current;
	}

	public boolean validMove(int y, int x) {
		if (y > 0 && x > 0 && y < 20 && x < 20) {
			return true;
		}
		return false;
	}

}
