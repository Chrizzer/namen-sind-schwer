package flensburg._2.oop._6.fail1.src.minigame;

public class Player extends CustomSprite {

	public int hp = 100;

	Player(MyMiniGame game) {
		super(game);
		paintImage("kp.png");
		show();
		speed = 100;
	}

	public void setHp(int i) {
		this.hp = i;
	}

	public int getHp() {
		return hp;
	}

	public void takeDmg(int i) {
		this.hp -= i;
		if (hp > 0) {
			// Adrenalinpush
		}
	}
}