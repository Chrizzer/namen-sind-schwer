package flensburg._2.oop._6.fail2.src.maze;

/**
 * A convenience structure that represents one cell. It contains a cell's
 * coordinates.
 *
 * @author Shawn Silverman
 */
public class Cell {
	protected int x;
	protected int y;

	/**
	 * Creates a new cell object having the given coordinates.
	 *
	 * @param x
	 *            the cell's X-coordinate
	 * @param y
	 *            the cell's Y-coordinate
	 */
	protected Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
