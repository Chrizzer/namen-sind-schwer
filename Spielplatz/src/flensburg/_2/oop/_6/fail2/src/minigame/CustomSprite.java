package flensburg._2.oop._6.fail2.src.minigame;

import fhfl.miniGame.engine.Sprite;

public abstract class CustomSprite extends Sprite {
	protected int speed;
	protected static int size_X = 30;

	CustomSprite(MyMiniGame game) {
		super(size_X, size_X, game);
	}

	public void setSpeed(int i) {
		speed = i;
	}

	public int getSpeed() {
		return speed;
	}
	public void setPosition(int[][] a){
		// super.setPosition(a[][], a[][]);
	}
}