package flensburg._2.oop._6.fail2.src.minigame;

public class Enemy extends CustomSprite {

	protected static int difficulty;

	Enemy(MyMiniGame game) {
		super(game);
		paintImage("background2.png");
		show();
		setSpeed(speed * difficulty);
		speed = 200;
	}

	public void search() {

	}

	public void attack() {

	}

	public void setSpeed(int i) {
		speed = i;
	}

	public static void setDif(int i) {
		difficulty = i;
	}

	public int getDif() {
		return difficulty;
	}
}