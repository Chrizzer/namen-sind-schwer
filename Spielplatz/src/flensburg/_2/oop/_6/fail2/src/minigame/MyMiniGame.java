package flensburg._2.oop._6.fail2.src.minigame;

import java.util.Random;

import fhfl.miniGame.engine.Background;
import fhfl.miniGame.engine.MiniGame;
import fhfl.miniGame.engine.Sprite;
import flensburg._2.oop._6.fail2.src.mazeTry.HuntAndKillMazeGenerator;
import flensburg._2.oop._6.fail2.src.mazeTry.MazeGenerator;

public class MyMiniGame extends MiniGame {
	public MyMiniGame() {
	}

	// Variablen festlegen
	Background bg = getBackgroundPicture();
	Player p1 = new Player(this);
	static int[][] coords = new int[672 / 32][672 / 32];
	Enemy[] foes;
	Random r = new Random();
	int min = 0, trailCounter = 0;
	private static int difficulty = 0, computerScore = 0, playerScore = 0;
	boolean[] horizontalWalls;
	boolean[] verticalWalls;
	Wall[][] horizWalls;
	Wall[][] vertWalls;

	@Override
	protected void initGame() {
		p1.setHp(100);
		foes = new Enemy[getDif()];
		fillCoords();
		drawLabyrinth();
	}

	@Override
	protected void gameHasStarted() {
		p1.setPosition(coords[0][0], coords[0][0]);
		createEnemies();
		setPlayerScore(p1.getHp());
	}

	@Override
	protected void gameUpdate() {
		if (foes[getEnemy()].overlapsSprite(p1)) {
			p1.takeDmg(5 * getDif());
			addComputerScore(100 - p1.getHp());
		}
		setPlayerScore(p1.getHp());
	}

	@Override
	protected void gameHasFinished() {
		// Sprites h�ren auf sich zu bewegen.
		stopSprites();
		// endScreen();
	}

	@Override
	protected void playerMove(Action action) {
		makeMove(p1, action);
		leaveTrail(p1);
	}

	@Override
	protected void computerMove(Action action) {
		makeMove(foes[getEnemy()], action);
	}

	public boolean validMove(Action action, int x, int y) {
		// falls eine Wand im weg ist, return false
		// fallse keine Wand im Weg ist, return true
		// um eine Zelle an Stelle X|Y in coords System liegen die
		// vertWalls[y][x-1] und vertWalls[y][x+1]
		// und horizWalls[x][y-1] und horizWalls[x][y+1]
		boolean temp = false;
		switch (action) {
		case UP:
			temp = !(horizWalls[y - 1][x].getSolidity());
			break;
		case DOWN:
			temp = !(horizWalls[y + 1][x].getSolidity());
			break;
		case LEFT:
			temp = !(vertWalls[y][x - 1].getSolidity());
			break;
		case RIGHT:
			temp = !(vertWalls[y][x + 1].getSolidity());
			break;
		case GO:
			System.out.println("GO");
			break;
		}
		return temp;
	}

	public void leaveTrail(Sprite p) {
		int[][] trail = new int[4][2];
		if (trailCounter < 4) {
			trail[trailCounter][0] = findCurrentX(p);
			trail[trailCounter][1] = findCurrentY(p);
			bg.paintLine(trail[trailCounter][0], trail[trailCounter][1],
					trail[trailCounter][0], trail[trailCounter][1], 0, 0, 255);
			// kreis(trail[trailCounter][0], trail[trailCounter][1]);
		} else {
			trailCounter = 0;
		}
		trailCounter++;
	}

	/**
	 * 
	 * @param p
	 * @param action
	 */
	public void makeMove(Sprite p, Action action) {
		int x = Math.max(findCurrentX(p), 0);
		int y = Math.max(findCurrentY(p), 0);
		if (p.animationHasFinished())

			switch (action) {
			case UP:
				System.out.println(p.toString() + " UP "
						+ validMove(action, x, y));
				if (validMove(action, x, y))
					p.animateTo(coords[x][y], coords[x][y - 1], 150);
				break;
			case DOWN:
				System.out.println(p.toString() + "DOWN "
						+ validMove(action, x, y));
				if (validMove(action, x, y))
					p.animateTo(coords[x][y], coords[x][y + 1], 150);
				break;
			case LEFT:
				System.out.println(p.toString() + "LEFT "
						+ validMove(action, x, y));
				if (validMove(action, x, y))
					p.animateTo(coords[x][y], coords[x - 1][y], 150);
				break;
			case RIGHT:
				System.out.println(p.toString() + "RIGHT "
						+ validMove(action, x, y));
				if (validMove(action, x, y))
					p.animateTo(coords[x][y], coords[x + 1][y], 150);
				break;
			case GO:
				if (p.toString().equals("player")) {

				} else {
					// foes[getEnemy()].animateTo(p1.getXPosition(),
					// p1.getYPosition(), foes[getEnemy()].getSpeed());
				}
				break;
			}

	}

	/**
	 * 
	 * Eckpunkte 640x640
	 */
	public void drawLabyrinth() {
		bg.clear();
		bg.fill(0, 0, 0);
		kreis(0, 0);
		createWalls();
		outlineWalls();
	}

	public void endScreen() {
		bg.clear();
		bg.fill(255, 255, 255);
		if (p1.getHp() > 0)
			bg.paintString(coords[coords.length / 2][coords.length / 2],
					coords[coords.length / 2][coords.length / 2], "Winner :"
							+ "Player 1!", 0, 0, 0);
		else {
			bg.paintString(coords[coords.length / 2][coords.length / 2],
					coords[coords.length / 2][coords.length / 2],
					"You have died", 0, 0, 0);
			kreis(p1.getXPosition(), p1.getYPosition());
			bg.paintString(p1.getXPosition(), p1.getYPosition(), "R.I.P", 0, 0,
					0);
		}
	}

	void createWalls() {
		MazeGenerator maze = new HuntAndKillMazeGenerator(coords.length,
				coords.length, coords[0][0], coords[0][0]);
		maze.generate();
		horizontalWalls = maze.getHorizWalls();
		verticalWalls = maze.getVertWalls();
		int width = maze.getWidth(), height = maze.getHeight();
		horizWalls = new Wall[coords.length][coords.length];
		vertWalls = new Wall[coords.length][coords.length];
		for (int y = 0; y < height; y++) {

			// create a row of horizontal walls
			// [y][x], weil sie sich in der h�he verschieben
			int rowBase = y * width;
			for (int x = 0; x < width; x++) {
				horizWalls[y][x] = new horizWall(horizontalWalls[rowBase + x]);
			}
			// create a row of vertical walls
			// [x][y],weil die vertical walls sich in der breite verschieben
			rowBase = y * (width + 1);
			for (int x = 0; x < width; x++) {
				vertWalls[x][y] = new vertWall(verticalWalls[rowBase + x]);
			}
			vertWalls[width - 1][y] = new vertWall(verticalWalls[rowBase
					+ width]);
		}

		// create the last row of horizontal walls
		int rowBase = height * width;
		for (int x = 0; x < width; x++) {
			horizWalls[height - 1][x] = new horizWall(horizontalWalls[rowBase
					+ x]);
		}
		// Prints Values of HorizWalls and VertWalls
		for (int j = 0; j < horizWalls.length; j++) {
			for (int k = 0; k < horizWalls.length; k++) {
				System.out.print(horizWalls[j][k].getSolidity() + " ");
			}
			System.out.println();
		}

		for (int j = 0; j < vertWalls.length; j++) {
			for (int k = 0; k < vertWalls.length; k++) {
				System.out.print(vertWalls[j][k].getSolidity() + " ");
			}
			System.out.println();
		}
	}

	void outlineWalls() {
		int height = coords.length, width = coords.length;
		for (int y = 0; y < height; y++) {
			// outline a row of horizontal walls
			int currentRow = y * coords.length;
			for (int x = 0; x < width; x++) {
				horizWalls[y][x].paintWall(bg, coords[x][y], coords[x][y],
						horizontalWalls[currentRow + x]);
			}
			// outline a row of vertical walls
			currentRow = y * (width + 1);
			for (int x = 0; x < width; x++) {
				vertWalls[x][y].paintWall(bg, coords[x][y], coords[x][y],
						verticalWalls[currentRow + x]);
			}
		}
		// outline the last row of horizontal walls
		int currentRow = height * width;
		for (int x = 0; x < width; x++) {
			horizWalls[coords.length - 1][x].paintWall(bg,
					coords[x][coords.length - 1],
					coords[x + 1][coords.length - 1],
					horizontalWalls[currentRow + x]);
		}
	}

	/**
	 * 
	 * @param x
	 *            : Index des Arrays {@link coords} nach rechts
	 * @param y
	 *            : Index des Arrays {@link coords} nach unten
	 */
	public void kreis(int x, int y) {
		// x Nach Rechts und y nach unten
		// bg.paintEllipse(coords[x], coords[y], 32, 32, 1, 255, 0, 0);
	}

	/**
	 * Array auff�llen in 32er Schritten, da coords[640/32] ist
	 */
	public void fillCoords() {
		int min = 0;
		for (int i = 0; i < coords.length; i++) {
			for (int j = 0; j < coords.length; j++)
				coords[i][j] = min;
			min += 32;
		}
	}

	public int[][] getCoords() {

		return coords;
	}

	/**
	 * Erzeugt Gegner, je nach L�nge des zuvor initialisierten Gegner Arrays
	 */
	private void createEnemies() {
		for (int i = 0; i < foes.length; i++) {
			foes[i] = new Enemy(this);
			foes[i].setPosition(coords[i][i], coords[i][2 + i]);
		}
	}

	public int getEnemy() {
		int temp = r.nextInt(foes.length);
		return temp;
	}

	@Override
	public int getNrOfPlayerGoActionsMax(int difficulty) {
		MyMiniGame.difficulty = difficulty;
		return 5;
	}

	public int getDif() {
		return MyMiniGame.difficulty;
	}

	@Override
	public int getNrofComputerGoActions(int difficulty) {
		// Berechnungen f�r Schwierigkeitsgradder Gegner
		Enemy.setDif(difficulty);
		return 5 * difficulty;
	}

	public void setPlayerScore(int i) {
		playerScore = i;
	}

	public void setComputerScore(int i) {
		computerScore = i;
	}

	public void subtractPlayerScore(int i) {
		playerScore -= i;
	}

	public void addComputerScore(int i) {
		computerScore += i;
	}

	@Override
	public int getCurrentComputerScore() {
		return computerScore;
	}

	@Override
	public int getCurrentPlayerScore() {
		return playerScore;
	}

	/**
	 * 
	 * @param p
	 *            : Sprite p
	 * @return finds and returns current X-Value's Index in{@link coords}
	 */
	public int findCurrentX(Sprite p) {
		int[][] pos = getCoords();
		int current = 0;
		// search
		for (int x = 0; x < coords.length; x++) {
			if (pos[x][0] == p.getXPosition())
				current = x;
		}
		return current;
	}

	public int findCurrentY(Sprite p) {
		int[][] pos = getCoords();
		int current = 0;
		// search
		for (int y = 0; y < coords.length; y++) {
			if (pos[0][y] == p.getYPosition())
				current = y;
		}
		return current;
	}

	public void stopSprites() {
		p1.stopAnimation();
		p1.dontShow();
		for (int i = 0; i < foes.length; i++) {
			foes[i].stopAnimation();
			foes[i].dontShow();
		}
	}
}
