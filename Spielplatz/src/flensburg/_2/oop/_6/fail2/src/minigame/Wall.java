package flensburg._2.oop._6.fail2.src.minigame;

import fhfl.miniGame.engine.Background;

public abstract class Wall {
	protected final static int RAND = 5;
	protected final static int rS = 0;
	protected final static int gS = 255;
	protected final static int bS = 0;

	protected final static int rN = 0;
	protected final static int gN = 0;
	protected final static int bN = 0;

	protected final static int LENGTH = 32;

	public Wall() {
	}

	/**
	 * 
	 * @param bg
	 *            : Background, auf dem Wand gezeichnet werden soll
	 * @param startX
	 *            : Index des Startpunktes im Array coords aus MyMiniGame
	 * @param startY
	 *            : Index des Startpunktes im Array coords aus MyMiniGame
	 * @param endpunkt
	 *            : Index des Endpunktes
	 * @param solid
	 *            : farbe der Wand
	 */
	abstract void paintWall(Background bg, int startX, int startY, boolean solid);

	public abstract boolean getSolidity();
}