package flensburg._2.oop._6.fail2.src.minigame;

import fhfl.miniGame.engine.Background;

public class horizWall extends Wall {
	boolean solid;

	public horizWall(boolean solid) {
		this.solid = solid;
	}

	void paintWall(Background bg, int startX, int startY, boolean solid) {
		if (solid) {
			bg.paintLine(startX, startY, startX + LENGTH, startY, RAND, rS, gS,
					bS, 255);
		} else {
			bg.paintLine(startX, startY, startX + LENGTH, startY, RAND, rN, gN,
					bN, 255);
		}
		bg.paintString(startX, startY, Integer.toString(startX), 255, 255, 255);
//		bg.paintString(startX, startY+ LENGTH, Integer.toString(startX+ 1), 255, 255, 255);
	}

	public boolean getSolidity() {
		return this.solid;
	}
}