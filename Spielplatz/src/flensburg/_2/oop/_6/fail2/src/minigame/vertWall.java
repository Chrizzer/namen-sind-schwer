package flensburg._2.oop._6.fail2.src.minigame;

import fhfl.miniGame.engine.Background;

public class vertWall extends Wall {
	boolean solid;

	public vertWall(boolean solid) {
		this.solid = solid;
	}

	void paintWall(Background bg, int startX, int startY, boolean solid) {
		if (solid) {
			bg.paintLine(startX, startY, startX, startY + LENGTH, RAND, rS, gS,
					bS, 255);
		} else {
			bg.paintLine(startX, startY, startX, startY + LENGTH, RAND, rN, gN,
					bN, 255);
		}
//		bg.paintString(startX, startY, Integer.toString(startY), 255, 255, 255);
//		bg.paintString(startX + LENGTH, startY,
//				Integer.toString(startY + 1), 255, 255, 255);
	}

	public boolean getSolidity() {
		return this.solid;
	}
}