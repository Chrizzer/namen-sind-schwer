package flensburg._2.oop._6.versuch3.src.minigame;

public class Barrel extends CustomSprite {

	Barrel(MyMiniGame game) {
		super(game);
		paintImage("barrel.png");
		show();
	}

	public void aufnehmen() {
		dontShow();
	}
}
