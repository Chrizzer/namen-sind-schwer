package flensburg._2.oop._6.versuch3.src.minigame;

import fhfl.miniGame.engine.Sprite;

public class Bullet extends CustomSprite {
	private int x = 350;

	public Bullet(MyMiniGame game) {
		super(game);
		paintImage("bullet.png");
		show();
	}

	/**
	 * 
	 * @param direction
	 *            : 0 for up,1 for left, 2 for down, 3 for right
	 *
	 */
	public void shoot(Sprite p, int direction) {
		switch (direction) {
		case 0:
			setPosition(p.getXPosition(), p.getYPosition()
					- MyMiniGame.Ycoords[1]);
			animate(0, -x);
			break;
		case 1:
			setPosition(p.getXPosition() - MyMiniGame.Xcoords[1],
					p.getYPosition());
			animate(-x, 0);
			break;
		case 2:
			setPosition(p.getXPosition(), p.getYPosition()
					+ MyMiniGame.Ycoords[1]);
			animate(0, x);
			break;
		case 3:
			setPosition(p.getXPosition() + MyMiniGame.Xcoords[1],
					p.getYPosition());
			animate(x, 0);
			break;
		default:
			dontShow();
			break;
		}
	}

	public void shootAt(Sprite p, int toX, int toY) {
		setPosition(p.getXPosition(), p.getYPosition());
		animateTo(toX, toY, 200);
	}
}
