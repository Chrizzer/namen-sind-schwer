package flensburg._2.oop._6.versuch3.src.minigame;

public class Enemy extends CustomSprite {

	protected static int difficulty;

	Enemy(MyMiniGame game) {
		super(game);
		paintImage("pirate.png");
		show();
	}

	public void search() {

	}

	public static void setDif(int i) {
		difficulty = i;
	}

	public int getDif() {
		return difficulty;
	}
}