package flensburg._2.oop._6.versuch3.src.minigame;

import java.util.Random;
import fhfl.miniGame.engine.Background;
import fhfl.miniGame.engine.MiniGame;
import fhfl.miniGame.engine.Sprite;

public class MyMiniGame extends MiniGame {
	public MyMiniGame() {
	}

	// Variablen festlegen
	Background bg = getBackgroundPicture();
	Player p1;
	static int[] Xcoords = new int[640 / 32], Ycoords = new int[640 / 32];
	static int[] barrelPosX = new int[50], barrelPosY = new int[50];
	Barrel[] barrels;
	Enemy pirate;
	Bullet[] bullets = new Bullet[55];
	Random r = new Random();
	private int difficulty = 0, bulletcounter = 0, malus = 0, minuspoints = 0,
			lastDir = 1;
	private static int computerScore = 0, playerScore = 0;
	public int pirate_score = 0;
	public int p1_barrels = 0;

	private int caught = 0;

	@Override
	protected void initGame() {
		try {
			stopSprites();
			resetScore();
		} catch (Exception e) {
			caught++;
		}
		bg.clear();
		drawIsland();
		gameSetup();
	}

	@Override
	protected void gameHasStarted() {
		p1.setPosition(Xcoords[Xcoords.length / 2], Ycoords[Ycoords.length / 2]);
	}

	@Override
	protected void gameUpdate() {
		setComputerScore(pirate_score);
		setPlayerScore(p1_barrels);
		spriteCollision();
		malus = p1_barrels * 5;

	}

	/**
	 * verringert die Anzahl der gehaltenen F�sser
	 * 
	 * @param p
	 * @param dropped
	 *            : Anzahl der fallengelassenen F�sser
	 */
	public void dropBarrels(Sprite p, int dropped) {
		if (p.equals(p1)) {
			p1_barrels -= dropped;
		} else if (p.equals(pirate)) {
			pirate_score -= dropped;
		}
	}

	@Override
	protected void gameHasFinished() {
		// Sprites h�ren auf sich zu bewegen.
		stopSprites();
		// Endscreen wird gezigt
		endScreen();
		System.out.println("Exceptions caught:" + caught);
	}

	@Override
	protected void playerMove(Action action) {
		makeMove(p1, action);
	}

	@Override
	protected void computerMove(Action action) {
		makeMove(pirate, action);
	}

	/**
	 * Methode f�r die m�glichen Z�ge der Spieler
	 * 
	 * @param p
	 * @param action
	 */
	public void makeMove(Sprite p, Action action) {
		int x = Math.max(findCurrentX(p), 0);
		int y = Math.max(findCurrentY(p), 0);
		int speed = (int) (p.equals(p1) ? 100 + malus : 150);
		try {
			if (p.animationHasFinished())
				switch (action) {
				case UP:
					p.animateTo(Xcoords[x], Ycoords[y - 1], speed);
					lastDir = 0;
					break;
				case LEFT:
					p.animateTo(Xcoords[x - 1], Ycoords[y], speed);
					lastDir = 1;
					break;
				case DOWN:
					p.animateTo(Xcoords[x], Ycoords[y + 1], speed);
					lastDir = 2;
					break;
				case RIGHT:
					p.animateTo(Xcoords[x + 1], Ycoords[y], speed);
					lastDir = 3;
					break;
				case GO:
					bullets[bulletcounter] = new Bullet(this);
					if (p.equals(p1)) {
						bullets[bulletcounter].shoot(p, lastDir);
					} else {
						int predictionX = r.nextInt(2) - 1;
						int predictionY = r.nextInt(2) - 1;
						// Computer w�hlt zuf�llig eine m�gliche Richtung des
						// Spielers aus
						bullets[bulletcounter].shootAt(p, p1.getXPosition()
								+ predictionX, p1.getYPosition() + predictionY);
					}
					bulletcounter++;
					break;
				}
		} catch (ArrayIndexOutOfBoundsException e) {
			caught++;
		}
	}

	/**
	 * 
	 * �berpr�ft das Verhalten der sich aktuell im Spiel befindenen Sprites.
	 * 
	 */
	public void spriteCollision() {
		for (int i = 0; i < barrels.length; i++) {
			if (pirate.overlapsSprite(barrels[i])) {
				barrels[i].aufnehmen();
				pirate_score++;
			}
			if (p1.overlapsSprite(barrels[i])) {
				barrels[i].aufnehmen();
				p1_barrels++;
			}
		}
		try {
			// checkt die Positionen der fliegenden Kugeln
			// und stoppt sie nachdem sie ihr Ziel erreichen
			for (int i = 0; i < bulletcounter; i++) {
				if (bullets[i].getXPosition() < Xcoords[1]
						|| bullets[i].getXPosition() > Xcoords[19]
						|| bullets[i].getYPosition() < Ycoords[1]
						|| bullets[i].getYPosition() > Ycoords[19]) {
					bullets[i].stopAnimation();
					bullets[i].dontShow();
				} else if (bullets[i].animationHasFinished()) {
					bullets[i].stopAnimation();
					bullets[i].dontShow();
				} else if (bullets[i].overlapsSprite(p1)) {
					// falls getroffen, dann verliere so und soviele Barrels
					dropBarrels(p1, r.nextInt(p1_barrels) + minuspoints);
					System.out.println("PLAYER HIT");
				} else if (bullets[i].overlapsSprite(pirate)) {
					dropBarrels(pirate, r.nextInt(pirate_score));
				}
			}
		} catch (IllegalArgumentException e) {
			caught++;
		}
	}

	/**
	 * Array auff�llen in 32er Schritten, da coords[640/32] ist
	 */
	private void fillCoords() {
		int min = 0;
		int abstandX = (640 / Xcoords.length);
		int abstandY = (640 / Ycoords.length);
		for (int y = 0; y < Ycoords.length; y++) {
			Ycoords[y] = min;
			min += abstandY;
		}
		min = 0;
		for (int x = 0; x < Xcoords.length; x++) {
			Xcoords[x] = min;
			min += abstandX;
		}
	}

	private void gameSetup() {
		p1 = new Player(this);
		pirate = new Enemy(this);
		fillCoords();
		createBarrels();
		switch (getDif()) {
		case 7:
			minuspoints = 5;
			break;
		case 8:
			minuspoints = 7;
			break;
		case 9:
			minuspoints = 9;
			break;
		case 10:
			minuspoints = 11;
			break;
		default:
			minuspoints = 0;
			break;
		}
	}

	private void drawIsland() {
		bg.clear();
		bg.paintImage("gameBG.png");
	}

	/**
	 * Baut den Endscreen
	 */
	private void endScreen() {
		bg.clear();
		String str = "";
		if (getCurrentPlayerScore() > getCurrentComputerScore()) {
			str = "Winner : Player 1!";
		} else if (getCurrentPlayerScore() < getCurrentComputerScore()) {
			str = "Yarr. The Pirates Won!";
		} else {
			str = "Unentschieden";
		}
		bg.paintImage("endscreen.png");
		// Strings to be written
		bg.paintString(Xcoords[Xcoords.length / 2],
				Ycoords[Ycoords.length / 2], str, 0, 0, 0);
	}

	/**
	 * Erzeugt den BarrelArray und verteilt die Barrels auf der Karte
	 */
	private void createBarrels() {
		barrels = new Barrel[50];
		// initialize the barrelPos Arrays
		for (int i = 0; i < barrelPosX.length; i++) {
			barrelPosX[i] = (r.nextInt(Xcoords.length) + 1) * 32;
			barrelPosY[i] = (r.nextInt(Ycoords.length) + 1) * 32;
		}

		for (int i = 0; i < barrels.length; i++) {
			barrels[i] = new Barrel(this);
			barrels[i].setPosition(barrelPosX[i], barrelPosY[i]);
		}
	}

	/**
	 * 
	 * @param str
	 * @return returns Xcoords oder Ycoords
	 */
	public int[] getCoords(String str) {
		if (str.equals("X")) {
			return Xcoords;
		} else {
			return Ycoords;
		}
	}

	@Override
	public int getNrOfPlayerGoActionsMax(int difficulty) {
		this.difficulty = difficulty;
		return 5;
	}

	public int getDif() {
		return difficulty;
	}

	@Override
	public int getNrofComputerGoActions(int difficulty) {
		Enemy.setDif(difficulty);
		return 5 * difficulty;
	}

	public void setPlayerScore(int i) {
		playerScore = i;
	}

	public void setComputerScore(int i) {
		computerScore = i;
	}

	@Override
	public int getCurrentComputerScore() {
		return computerScore;
	}

	@Override
	public int getCurrentPlayerScore() {
		return playerScore;
	}

	/**
	 * 
	 * @param p
	 *            : Sprite p
	 * @return finds and returns current X-Value's Index in coords
	 */
	public int findCurrentX(Sprite p) {
		int[] pos = getCoords("X");
		int current = 0;
		// search
		for (int x = 0; x < Xcoords.length; x++) {
			if (pos[x] == p.getXPosition())
				current = x;
		}
		return current;
	}

	/**
	 * 
	 * @param p
	 *            : Sprite p
	 * @return finds and returns current Y-Value's Index in coords
	 */
	public int findCurrentY(Sprite p) {
		int[] pos = getCoords("Y");
		int current = 0;
		// search
		for (int y = 0; y < Ycoords.length; y++) {
			if (pos[y] == p.getYPosition())
				current = y;
		}
		return current;
	}

	private void stopSprites() {
		p1.stopAnimation();
		p1.dontShow();
		pirate.stopAnimation();
		pirate.dontShow();
		try {
			for (int i = 0; i < barrels.length; i++) {
				barrels[i].stopAnimation();
				barrels[i].dontShow();
			}
			for (int i = 0; i < bullets.length; i++) {
				bullets[i].dontShow();
			}
		} catch (NullPointerException e) {
			caught++;
		}
	}

	private void resetScore() {
		try {
			pirate_score = 0;
			p1_barrels = 0;
			bulletcounter = 0;
			caught = 0;
		} catch (NullPointerException e) {
			caught++;
		}
	}
}