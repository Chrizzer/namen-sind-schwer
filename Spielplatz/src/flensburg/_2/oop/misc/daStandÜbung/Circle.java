package flensburg._2.oop.misc.daStandÜbung;

import java.awt.Graphics;

public class Circle extends GeometricObject {

	@Override
	public void paintObj(Graphics g, int xStart, int yStart, int xSize,
			int ySize) {
		g.fillOval(xSize, ySize, xStart, yStart);
	}

}
