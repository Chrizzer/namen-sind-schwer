package flensburg._2.oop.misc.daStandÜbung;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

/**
 * parent Class
 * 
 * @author Chris
 *
 */
public abstract class GeometricObject extends JPanel implements MouseListener,
		MouseMotionListener {
	private Color tempcolor = Color.green;

	/**
	 * Konstruktor der die Listener hinzuf�gt
	 */
	GeometricObject() {
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	/**
	 * Zeichnet das Objekt
	 * 
	 * @param xStart
	 * @param yStart
	 * @param xSize
	 * @param ySize
	 */
	public abstract void paintObj(Graphics g, int xStart, int yStart,
			int xSize, int ySize);

	/**
	 * GeometricObjects Paint-Methode. Sie berechnet den verf�gbaren Raum f�r
	 * die GOs und ruft die paintObj Methode der Tochterklassen auf.
	 */
	public void paint(Graphics g) {
		super.paint(g);// ruft die Methode paint(g) von JPanel auf
		// Berechnungen...
		int platz = (int) (Math.min(getWidth(), getHeight()) * 0.9);
		int actualHeight = (getHeight() - platz) / 2;
		int actualWidth = (getWidth() - platz) / 2;
		// Rand zeichnen...
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(new Color(255, 0, 0));
		g.drawRect(0, 0, (getWidth() - 1), (getWidth() - 1));
		// Farbe �ndern f�r das Objekt und Objekt zeichnen...
		g.setColor(this.tempcolor);
		paintObj(g, platz, platz, actualWidth, actualHeight);
	}

	private void setColor(Color c) {
		this.tempcolor = c;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		this.setColor(Color.BLACK);
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

}