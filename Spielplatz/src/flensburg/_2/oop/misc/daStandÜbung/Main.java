package flensburg._2.oop.misc.daStandÜbung;

/**
 * Aufgabenstellung: Erstelle ein Fenster in dem in einem Grid von 5x5
 * gleichm��ig Kreise,Vierecke und Dreiecke angezeigt werden. Dieses Fenster
 * soll sich in der Mitte des Bildschirms befinden. Die Farben der Objekte
 * sollen sich �ndern, wenn man mit der Maus auf sie klickt.
 * 
 * @author Chris
 *
 */
public class Main {

	public static void main(String[] args) {
		// neues Fenster wird erstellt
		new MyFrame();
	}
}
