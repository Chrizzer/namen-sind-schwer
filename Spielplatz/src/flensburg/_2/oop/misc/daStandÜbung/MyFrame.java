package flensburg._2.oop.misc.daStandÜbung;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	private int var = 0;

	MyFrame() {
		this(5); // Ruft den Konstruktor mit dem Parameter 5 auf
		setSize(500, 500); // legt Gr��e des Fenster fest
		setLocationRelativeTo(null); // setzt das Fenster in die Mitte
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(5, 5));
		setVisible(true); // zeigt das Fenster
	}

	/**
	 * Konstruktor um festzulegen wieviele Zellen das Fenster hat
	 */
	MyFrame(int zahl) {
		GeometricObject[] gos = new GeometricObject[zahl * zahl];
		for (int i = 0; i < gos.length; i++) {
			switch (i % 3) {
			case 1:
				gos[i] = new Square();
				break;
			case 2:
				gos[i] = new Circle();
				break;
			default:
				gos[i] = new Triangle();
				break;
			}
			this.add(gos[i]);
		}
		validate();
		repaint();
	}
}