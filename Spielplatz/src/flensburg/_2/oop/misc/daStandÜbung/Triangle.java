package flensburg._2.oop.misc.daStandÜbung;

import java.awt.Graphics;

public class Triangle extends GeometricObject {

	@Override
	public void paintObj(Graphics g, int xStart, int yStart, int xSize,
			int ySize) {
		int n = 3;
		int[] triangleX2 = { xSize, xSize + xStart / 2, xSize + xStart };
		int[] triangleY2 = { ySize + yStart, ySize, ySize + yStart };
		g.fillPolygon(triangleX2, triangleY2, n);
	}

}
