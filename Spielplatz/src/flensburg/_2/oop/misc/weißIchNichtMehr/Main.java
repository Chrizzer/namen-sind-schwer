package flensburg._2.oop.misc.weißIchNichtMehr;

import java.util.Scanner;

/**
 * Created by Chris on 10.04.2017.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Bitte Fensteranzahl eingeben:");
        Scanner input = new Scanner(System.in);

        MyClass[] fensterArray = new MyClass[input.nextInt()];
        int w = 1920;
        int h = 1080;
        int fw = 400;
        int fh = 50*2;
        for (int i = 0, c = 0, r = 0; i < fensterArray.length; i++) {
            fensterArray[i] = new MyClass(fw*c,fh*r,fw,fh);
            if(r*fh < h-fh*2){
                r++;
            }else{
                c++;
                r = 0;
            }
            fensterArray[i].setTitle("Fesnter "+i);
        }
        System.out.println("Bitte Fensternamen eingeben:");
        while(input.hasNext()){
            String o = input.next();
            for (int i = 0; i < fensterArray.length; i++) {
                fensterArray[i].setTitle(o);
                try{
                    Thread.sleep(100);
                }catch (Exception e){}
            }
        }

    }
}