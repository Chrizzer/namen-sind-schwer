package flensburg._2.oop.misc.weißIchNichtMehr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Chris on 10.04.2017.
 */
public class MyClass extends JFrame {
    public MyClass(int px,int py, int w, int h){
        super();
        setLocation(px,py);
        setSize(w,h);
        setLayout(null);
        setVisible(true);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        MakeButton();
    }

    private void MakeButton(){
        Button b = new Button("Close All");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        int w = getContentPane().getWidth();
        int h = getContentPane().getHeight();
        int bw = ((w/10) > 50) ? (w/10) : 50;
        int bh = ((h/10) > 25) ? (h/10) : 25;
        int px = w - bw;
        int py = h - bh;
        b.setLocation(px,py);
        b.setSize(bw,bh);
        b.setVisible(true);
        this.add(b);
    }
}
