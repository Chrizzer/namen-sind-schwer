package flensburg._2.oop.tutorium;

import flensburg._2.oop.tutorium.logic.GameManager;

/**
 * Created by Chris on 11.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        GameManager.getInstance().BuildGame();
    }
}
