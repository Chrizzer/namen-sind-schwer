package flensburg._2.oop.tutorium.logic;

import flensburg._2.oop.tutorium.ui.Blank;
import flensburg._2.oop.tutorium.ui.MField;
import flensburg._2.oop.tutorium.ui.MWindow;
import flensburg._2.oop.tutorium.ui.Mine;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

/**
 * Created by Chris on 24.04.2017.
 */
public class GameManager {
    private long moves = 0;
    private long endTime = 0;
    static int row = 16;
    static int col = 30;
    private int flags = 0;
    private MWindow mw = null;
    private JPanel minePanel = null;
    private MField[][] fields = null;
    private static GameManager Instance = null;
    private GameManager(){
        Instance = this;
    }
    public static GameManager getInstance(){
        if(Instance == null){
            Instance = new GameManager();
        }
        return Instance;
    }
    public void BuildGame(){
        this.flags = 1;
        if(mw == null){
            mw = new MWindow(128 + col * MField.width, 128 + row * MField.height, this.flags);
        }
        minePanel = new JPanel(new GridLayout(row,col));
        mw.add(minePanel);
        AddFields(minePanel);
        mw.validate();
        mw.repaint();
    }

    private void AddFields(JPanel panel){
        fields = new MField[row][col];
        Random rand = new Random();
        int minesPlaced = 0;
        for (int r = 0; r < row ; r++) {
            MField left = null, up = null;
            for (int c = 0; c < col; c++) {
                MField field = null;
                left = (r == 0) ? null : fields[r-1][c];
                up = (c == 0) ? null : fields[r][c-1];
                if(minesPlaced <= flags){
                    switch(rand.nextInt(9)){
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            field = new Blank("Blank ("+r+","+c+")");
                            break;
                        case 7:
                        case 8:
                            field = new Mine();
                            minesPlaced++;
                            break;
                        default:
                            System.out.println("WTF switch error");
                            break;
                    }
                }else{
                    field = new Blank("Blank ("+r+","+c+")");
                }
                field.setNeighbours(up, left);
                panel.add(field);
                fields[r][c] = field;
            }
        }
        flags = minesPlaced;
    }
    public void Update(){
        mw.Update(++this.moves, this.flags);
    }
    private void Reveal(MField field) {
        if(field instanceof Blank){
            Blank blank = (Blank) field;
            int mines = blank.getMines();
            blank.setZahl(mines);
            if(mines == 0){
                Reveal0(blank, null);
            }
        }
        if(field.flagged){
            field.flagged = false;
            flags++;
        }
        field.revealed = true;
        field.validate();
    }
    private void Reveal0(Blank field, Blank last){
        if(field != null && field instanceof Blank){
            if(((Blank) field).flagged){
                ((Blank) field).flagged = false;
                flags++;
            }
            int mines = field.getMines();
            field.revealed = (mines == 0);
            field.repaint();
            if(field.revealed){
                field.setZahl(mines);
                if(field.Up instanceof Blank){
                    if(field.Up != last && !field.Up.revealed){
                        Reveal0((Blank) field.Up, field);
                    }
                }if(field.Down instanceof Blank){
                    if(field.Down != last&& !field.Down.revealed){
                        Reveal0((Blank) field.Down, field);
                    }
                }if(field.Left instanceof Blank){
                    if(field.Left != last&& !field.Left.revealed){
                        Reveal0((Blank) field.Left, field);
                    }
                }if(field.Right instanceof Blank){
                    if(field.Right != last&& !field.Right.revealed){
                        Reveal0((Blank) field.Right, field);
                    }
                }
            }else{
                Reveal(field);
            }
        }
    }
    public void Handle(MField field, int mouseButton){
        switch(mouseButton){
            case MouseEvent.BUTTON1: // LMB
                Reveal(field);
                if(field instanceof Mine){
                    EndGame(false);
                }
                break;
            case MouseEvent.BUTTON3: // RMB
                // flag them
                if(!field.revealed){
                    field.flagged = !field.flagged;
                    if(field.flagged){
                        flags--;
                    }else{
                        flags++;
                    }
                }
                break;
            default:
                System.out.println("DEFAULT?"+mouseButton);
                break;
        }
        field.validate();
        Update();
        if(flags == 0){
            EndGame(true);
        }
    }
    private void EndGame(boolean won){
        String string = null;
        if(won){
            string = "YOU WON with only " + moves + " moves Click anywhere to continue";
        }else{
            string = "YOU LOST Click anywhere to continue";
        }
        mw.remove(minePanel);
        JPanel winPanel = new JPanel(new GridBagLayout());
        JLabel winLabel = new JLabel(string);
        winPanel.add(winLabel);
        mw.add(winPanel);
        mw.repaint();
        mw.validate();
        winPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                mw.remove(winPanel);
                Restart();
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }
    private void StartGame(){
        moves = 0;
    }
    public void Restart(){
        StartGame();
        mw.remove(minePanel);
        BuildGame();
    }
}
