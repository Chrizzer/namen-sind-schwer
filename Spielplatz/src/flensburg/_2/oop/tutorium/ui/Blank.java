package flensburg._2.oop.tutorium.ui;

import java.awt.*;

/**
 * Created by Chris on 11.04.2017.
 */
public class Blank extends MField {
    public String zahl;
    public Blank(String name){
        super(name);
        this.col = Color.green;
    }

    public void setZahl(int zahl) {
        this.zahl = Integer.toString(zahl);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Zahl malen
        if(revealed){
            try{
                g.setColor(Color.black);
                g.setFont(new Font("TimesRoman", Font.BOLD, 16));
                g.drawString(zahl,(width/2)-g.getFontMetrics().stringWidth(zahl)/2,(height/2)+(height/3));
            }catch(Exception e){

            }
        }
    }
}
