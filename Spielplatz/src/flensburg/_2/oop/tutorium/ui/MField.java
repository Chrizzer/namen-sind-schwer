package flensburg._2.oop.tutorium.ui;

import flensburg._2.oop.tutorium.logic.GameManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by Chris on 11.04.2017.
 */
public class MField extends JLabel {
    public String name;
    public static int width = 24;
    public static int height = 24;

    public MField Up = null;
    public MField Down = null;
    public MField Left = null;
    public MField Right = null;

    public MField UpLeft(){
        if(Up != null){
            return Up.Left;
        }
        if(Left != null){
            return Left.Up;
        }
        return null;
    }
    public MField UpRight(){
        if(Up != null){
            return Up.Right;
        }
        if(Right != null){
            return Right.Up;
        }
        return null;
    }
    public MField DownRight(){
        if(Down != null){
            return Down.Right;
        }
        if(Right != null){
            return Right.Down;
        }
        return null;
    }
    public MField DownLeft(){
        if(Down != null){
            return Down.Left;
        }
        if(Left != null){
            return Left.Down;
        }
        return null;
    }
    public Boolean revealed = false;
    private Color borderCol = Color.black;
    protected Color col = Color.black;
    public Boolean flagged = false;
    public int getMines(){
        // Zahl berechnen
        int mines = 0;
        if (Up instanceof Mine) mines++;
        if (Down instanceof Mine) mines++;
        if (Left instanceof Mine) mines++;
        if (Right instanceof Mine) mines++;
        if (UpLeft() instanceof Mine) mines++;
        if (UpRight() instanceof Mine) mines++;
        if (DownRight() instanceof Mine) mines++;
        if (DownLeft() instanceof Mine) mines++;
        return mines;
    }
    public void setNeighbours(MField up, MField left){
        if(up != null){
            this.Up = up;
            up.Down = this;
        }
        if(left != null){
            this.Left = left;
            left.Right = this;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public MField(String name){
        super();
        this.name = name;
        setSize(width, height);
        addMouseListener(new MinesweeperListener(this) {
            @Override
            public void mouseReleased(MouseEvent e) {
                GameManager.getInstance().Handle(instance, e.getButton());
                repaint();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                borderCol = Color.yellow;
                repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                borderCol = Color.black;
                repaint();
            }
        });
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(borderCol);
        g.drawRect(0,0,width,height);
        if(flagged && !revealed){
            g.setColor(Color.red);
            g.drawLine(16,16,width-16,width-16);
            g.drawLine(width-16,16,16,width-16);
        }
    }
}
