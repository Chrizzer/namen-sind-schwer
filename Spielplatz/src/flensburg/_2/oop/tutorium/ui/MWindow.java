package flensburg._2.oop.tutorium.ui;

import flensburg._2.oop.tutorium.logic.GameManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Chris on 11.04.2017.
 */
public class MWindow extends JFrame {
    private JPanel panel;
    private String gameTime = "Moves: ";
    private String minesLeft = "Flags left: ";
    private JLabel gameTimePanel = null;
    private JLabel minesLeftPanel = null;
    private JButton restart = null;
    private JPanel topPanel = null;
    public MWindow(int w, int h, int mines) {
        super();
        setSize(w,h);
        setVisible(true);
        setResizable(false);
        setTitle("Minesweeper");
        setLayout(new BorderLayout());
        topPanel = new JPanel(new GridLayout(1,2));
        gameTimePanel = new JLabel(gameTime);
        minesLeftPanel = new JLabel(minesLeft);
        restart = new JButton("Restart game");
        restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameManager.getInstance().Restart();
            }
        });
        topPanel.add(minesLeftPanel);
        topPanel.add(restart);
        topPanel.add(gameTimePanel);
        panel = new JPanel();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.add(topPanel,BorderLayout.PAGE_START);
        this.add(panel,BorderLayout.CENTER);
    }
    public void Update(long a, int b){
        gameTimePanel.setText(gameTime + a);
        minesLeftPanel.setText(minesLeft + b);
        validate();
    }
}