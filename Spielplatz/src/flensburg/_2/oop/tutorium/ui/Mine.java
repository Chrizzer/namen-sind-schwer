package flensburg._2.oop.tutorium.ui;

import java.awt.*;

/**
 * Created by Chris on 11.04.2017.
 */
public class Mine extends MField {
    public Mine(){
        super("Mine");
        this.col = Color.red;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Mine malen
        if(revealed){
            g.setColor(this.col);
            g.fillRect(1,1,this.width-1,this.height-1);
        }
        if(flagged){
            g.setColor(Color.green);
            g.drawLine(16,16,width-16,width-16);
            g.drawLine(width-16,16,16,width-16);
        }
    }
}
