package flensburg._2.oop.tutorium.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Chris on 11.04.2017.
 */
public class MinesweeperListener implements MouseListener {
    public MField instance = null;
    public MinesweeperListener(MField instance){
        this.instance = instance;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }
}
