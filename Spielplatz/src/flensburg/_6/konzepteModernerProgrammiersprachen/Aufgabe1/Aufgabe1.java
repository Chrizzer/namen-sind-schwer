package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe1;

/**
 * Created by Chris on 31.03.2017.
 */
public class Aufgabe1 {
    public static void main(String[] args) {
        Value[] values = new Value[]{
                Value.Value('5'),
                Value.Value(true),
                Value.Value(false)
        };
        for (Value v: values) {
            System.out.println(test(v));
        }
    }
    public static Value test(Value val){
        if(val instanceof BoolValue){
            ((BoolValue) val).value = !((BoolValue) val).value;
        }
        return val;
    }
}
