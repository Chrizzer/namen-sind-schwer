package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe1;

/**
 * Created by Chris on 31.03.2017.
 */
public class BoolValue extends Value {
    public boolean value;
    public BoolValue(boolean b){
        this.value = b;
    }
    @Override
    public String toString() {
        return ""+value;
    }
}
