package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe1;

/**
 * Created by Chris on 31.03.2017.
 */
public class CharValue extends Value {
    public char value;
    public CharValue(char b){
        this.value = b;
    }

    @Override
    public String toString() {
        return ""+value;
    }
}
