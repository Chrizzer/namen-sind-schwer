package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe1;

/**
 * Created by Chris on 31.03.2017.
 */
public class CoolValue<T> {
    private T value;
    public CoolValue(T value){
        this.value = value;
    }
    public T getValue() {
        return this.value;
    }
    public void setValue(T value) {
        this.value = value;
    }
    @Override
    public String toString() {
        return this.value.toString();
    }
    @Override
    public boolean equals(Object obj) {
        return this.value.equals(obj);
    }
}