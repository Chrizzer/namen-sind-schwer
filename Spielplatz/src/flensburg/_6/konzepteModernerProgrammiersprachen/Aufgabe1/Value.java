package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe1;

/**
 * Created by Chris on 31.03.2017.
 */
public abstract class Value {
    public static Value Value(boolean b){
        return new BoolValue(b);
    }
    public static Value Value(char b){
        return new CharValue(b);
    }
}
