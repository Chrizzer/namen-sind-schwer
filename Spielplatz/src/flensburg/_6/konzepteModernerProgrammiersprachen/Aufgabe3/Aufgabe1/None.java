package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe1;

import java.util.NoSuchElementException;

/**
 * Created by Chris on 21.04.2017.
 */
public class None<T> extends Option<T> {

    public None(){}

    @Override
    public boolean isPresent() {
        return false;
    }

    @Override
    public T get() {
        throw new NoSuchElementException("None has no value");
    }

    @Override
    public T orElse(T other) {
        return other;
    }

    @Override
    public String toString() {
        return "None";
    }
}
