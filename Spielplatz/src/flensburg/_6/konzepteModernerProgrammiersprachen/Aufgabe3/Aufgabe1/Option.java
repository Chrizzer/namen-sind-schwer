package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe1;

/**
 * Created by Chris on 21.04.2017.
 */
public abstract class Option<T>
{
    public abstract T get();
    public abstract boolean isPresent();
    public abstract T orElse(T other);
    public static <T> Option<T> of(T value){
            return new Some<T>(value);
    }
    public static <T> Option<T> ofNullable(T value){
        return value == null ? new None() : new Some(value);
    }
}