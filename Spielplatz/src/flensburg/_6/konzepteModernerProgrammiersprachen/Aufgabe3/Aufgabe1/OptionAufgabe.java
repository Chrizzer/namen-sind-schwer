package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe1;

/**
 * Created by Chris on 21.04.2017.
 */
public class OptionAufgabe {
    public static void main(String[] args) {
        // Usage with Option
            Option<String> a = Option.of("Hallo");
            System.out.println(a.get());
            Option<Integer> b = Option.ofNullable(null);
            if(b.isPresent()){
                System.out.println(b.get());
            }
    }
}
