package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe1;
import java.util.NoSuchElementException;

/**
 * Created by Chris on 21.04.2017.
 */
public class Some<T> extends Option<T> {

    private final T value;

    public Some(T value){
        if(value == null){
            throw new NoSuchElementException("Cannot init Some with null");
        }else{
            this.value = value;
        }
    }
    @Override
    public boolean isPresent() {
        return true;
    }

    @Override
    public T get() {
        return value;
    }

    @Override
    public T orElse(Object other) {
        return this.value;
    }

    @Override
    public String toString() {
        return "Some("+this.value.toString()+")";
    }
}
