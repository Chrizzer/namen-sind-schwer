package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe2;

import flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe1.Option;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chris on 21.04.2017.
 */
public class RNA {
    enum Base{
        U,A,C,G
    }
    public static void main(String[] args) {
        Option<List<Base>> bases = parseRNA("ugacugcugaucgaucguagcacgu");
        if(bases.isPresent()){
            for (Base c : bases.get()) {
                System.out.print(c);
            }
        }else{
            System.out.println("Option was empty");
        }
        System.out.println("");
        bases = parseRNA("ugacugcugaucgaucguagcacguf");
        if(bases.isPresent()){
            for (Base c : bases.get()) {
                System.out.print(c);
            }
        }else{
            System.out.print("Option was empty");
        }
    }
    static Option<Base> parseBase(char c){
        switch(c){
            case 'u':
            case 'U':
                return Option.of(Base.U);
            case 'a':
            case 'A':
                return Option.of(Base.A);
            case 'g':
            case 'G':
                return Option.of(Base.G);
            case 'c':
            case 'C':
                return Option.of(Base.C);
            default:
                return Option.ofNullable(null);
        }
    }
    static Option<List<Base>> parseRNA(String bases){
        boolean dirty = false;
        List<Base> returnList = new ArrayList<Base>();
        char[] chars = bases.toCharArray();
        int i = 0;
        while(i < chars.length && !dirty){
            char c = chars[i];
            Option<Base> o = parseBase(c);
            if(o.isPresent()) {
                returnList.add(o.get());
                i++;
            }else{
                dirty = true;
            }
        }
        return dirty ? Option.ofNullable(null) : Option.ofNullable(returnList);
    }
}
