package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe3;

/**
 * Created by Chris on 25/04/2017.
 */
public class FiniteIterator extends InfiniteIterator {

    private int count;

    public FiniteIterator(int start, int end){
        super(start);
        this.count = start + end;
    }
    public FiniteIterator(InfiniteIterator iterator, int count){
        super(iterator.start);
        this.count = start + count;
    }

    @Override
    public Object next() {
        return (this.hasNext()) ? start++ : null;
    }

    @Override
    public boolean hasNext() {
        return start < count;
    }
}
