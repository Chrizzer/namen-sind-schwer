package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe3;

import java.util.Iterator;

/**
 * Created by Chris on 25/04/2017.
 */
public class InfiniteIterator implements Iterator {

    protected int start;

    public InfiniteIterator(int start){
        this.start = start;
    }

    @Override
    public Object next() {
        return (hasNext()) ? start++ : null;
    }

    @Override
    public boolean hasNext() {
        return start < Integer.MAX_VALUE;
    }
}
