package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe3;

import java.util.*;
import java.util.function.UnaryOperator;
/**
 * Created by Chris on 21.04.2017.
 */
public class IteratorAufgabe {
    public static void main(String[] args) {
        Iterator it = enumFrom(1000);
        int count = 10;
        Iterator it2 = take(it,count);
        while(it2.hasNext()){
            System.out.println("Iterator While => "+it2.next());
        }
        Iterator it3 = take(it, count);
        for(Object o : createWithIterator(it3)) {
            System.out.println("Iterator Foreach => "+o.toString());
        }
        // Können sie das gleiche Verhalten auch mit Listen implementieren?
        // Ja.
        Iterator it4 = take(it, count);
        for(Object o : createExample(it4)) {
            System.out.println("Iterator Foreach List => "+o.toString());
        }
    }
    static Iterator<Integer> enumFrom(Integer start){
        return new InfiniteIterator(start);
    }
    static <T> Iterator<T> take(Iterator<T> iterator, Integer count){
        return new FiniteIterator((InfiniteIterator)iterator, count);
    }
    static Iterable<Object> createWithIterator(Iterator iterator)
    {
        return new Iterable<Object>() {
            @Override
            public Iterator<Object> iterator() {
                return iterator;
            }
        };
    }
    static List<Object> createExample(Iterator iterator){
        return new List<Object>(){
            public Iterator<Object> iterator() {
                return iterator;
            }
            // Unwichtiger Kram
            public void replaceAll(UnaryOperator<Object> operator) {

            }
            public void sort(Comparator<? super Object> c) {

            }
            public Spliterator<Object> spliterator() {
                return null;
            }
            public int size() {
                return 0;
            }
            public boolean isEmpty() {
                return false;
            }
            public boolean contains(Object o) {
                return false;
            }
            public Object[] toArray() {
                return new Object[0];
            }
            public <T> T[] toArray(T[] a) {
                return null;
            }
            public boolean add(Object o) {
                return false;
            }
            public boolean remove(Object o) {
                return false;
            }
            public boolean containsAll(Collection<?> c) {
                return false;
            }
            public boolean addAll(Collection<?> c) {
                return false;
            }
            public boolean addAll(int index, Collection<?> c) {
                return false;
            }
            public boolean removeAll(Collection<?> c) {
                return false;
            }
            public boolean retainAll(Collection<?> c) {
                return false;
            }
            public void clear() {

            }
            public Object get(int index) {
                return null;
            }
            public Object set(int index, Object element) {
                return null;
            }
            public void add(int index, Object element) {

            }
            public Object remove(int index) {
                return null;
            }
            public int indexOf(Object o) {
                return 0;
            }
            public int lastIndexOf(Object o) {
                return 0;
            }
            public ListIterator<Object> listIterator() {
                return null;
            }
            public ListIterator<Object> listIterator(int index) {
                return null;
            }
            public List<Object> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
    }
}
