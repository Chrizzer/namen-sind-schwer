package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe3.Aufgabe3;

import java.util.Iterator;

/**
 * Created by Chris on 25/04/2017.
 */
public class MyIterable implements Iterable {

    private int start;
    private int count;

    public MyIterable(int start, int count){
        this.start = start;
        this.count = count;
    }

    @Override
    public Iterator iterator() {
        return new FiniteIterator(start, count);
    }
}
