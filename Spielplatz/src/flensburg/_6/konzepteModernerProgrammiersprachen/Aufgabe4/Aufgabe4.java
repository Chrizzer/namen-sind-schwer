package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Chris on 28.04.2017.
 */
public class Aufgabe4 {

    public static void aufgabe1() {
        Number[] numbers = new Number[4];
        // Integer[] integers = numbers;
        Object[] objects = numbers;

        objects[0] = "Test";
        System.out.println(objects[0]);

        /*
        Arrays sind kovariant und demnach
            Integer <= Number <= Object

        Jedes Integer kann eine Number sein
        und jede Number kann ein Object sein
        Aber nicht jedes Object kann eine Number sein
        und nicht jede Number ein Integer
        */

        List<Number> numberList = new ArrayList<Number>();
        // List<Integer> integerList = numberList;
        // List<Object> objectList = numberList;
        /*
        Bei Listen ist die Zuweisung schwieriger, weil die Typparameter nur bedingten
        Einfluss auf die Typen der Listen haben. Die Zuweisung
            List<Number> x = new ArrayList<Number>
        lässt darauf schließen, dass Listen wie Objekte Kovariant sind.
        */
    }

    public static void aufgabe2() {
        Integer R = new Integer(1);
        Number T = R;
        Object O = new Object();
        List<Integer> list = Arrays.asList(1,2,3,4,5);

        // Das sind die Gründe, warum die Methode so nicht funktionieren kann/darf
        coSet(list,0, O);
        coSet(list,0, T);

        // Valide Anwendungen
        Integer i_co = coGet(list,0);
        Number n_co = coGet(list,0);

        // Object o_co = coGet(list,0);

        conSet(list,0,13);
        // Anhand dieser Beispiele sieht man, warum conGet nicht funktionieren sollte
        List<Object> list2 = Arrays.asList(O,O,O,O);
        Integer i_con = conGet(list2,0);
        Number n_con = conGet(list2,0);
        Object o_con = conGet(list2,0);
        System.out.println(i_con);
        System.out.println(n_con);
        System.out.println(o_con);
    }
    static <T> void coSet(List<? extends T> list, int index, T value){
        // list.set(index, value);
        /*
        Diese Methode funktioniert nicht, weil die Objekte die in der Liste
        gespeichert werden sub-classes von T sind.

           R <= T  | Integer extends Number
            R = T  | Number  = Integer      Valider Ausdruck
            T = R  | Integer = Number       Invalider Ausdruck

        Einem Integer darf niemals eine Number zugewiesen werden.
        Koraviantes setten darf nun nicht funktionieren.
        */
    }
    static <T> T coGet(List<? extends T> list, int index){
        /*
        Valide Methode
         */
        return list.get(index);
    }
    static <T> void conSet(List<? super T> list, int index, T value){
        /*
        Valide Methode
         */
        list.set(index, value);
    }
    static <T> T conGet(List<? super T> list, int index){
        /*
        Diese Methode lässt sich nicht so einfach definieren,
        weil die Liste immer mindestens gleichwertig des Rückgabetyps sein muss.
        In jedem Fall, wo sich der Rückgabetyp vom Listen Typ unterscheidet
        treten Laufzeitfehler auf. Deswegen gibt es auch schon einen Compilerfehler.
         */
        // Compilerfehler
        // return list.get(index);

        // Dieses Returnstatement führt zu Laufzeitfehlern, wenn man
        // versucht die Oberklasse zur sub-class zu casten
        return (T) list.get(index);
    }
    /*
    Abgesehen davon, dass die Performance der Set Methode sehr viel schlechter wird,
    müssten alle Elemente der Liste dann zu dem Rückgabetyp gecastet werden.
    Casten funktioniert nur kovariant und demnach würde die kovariante
    SetMethode funktionieren, während die kontravariante nicht funktionieren sollte.
    */
    static <T> List<T> covarianceSet(List<? extends T> list, int index, T value){
        List<T> result = new ArrayList<T>();
        if(index > list.size()){
            return null;
        }
        for (int i = 0; i < index; i++) {
            result.add(list.get(i));
        }
        result.add(value);
        for (int i = index+1; i < list.size(); i++) {
            result.add(list.get(i));
        }
        return result;
    }

    public static void beispiele2() {
        List<Object> list = Arrays.asList(1,2,3,4);
        Integer i_value = 5;
        Number n_value = 2;
        Object o_value = new Object();
        // Beispiele
        System.out.println(covarianceSet(list,0,i_value));
        System.out.println(covarianceSet(list,2,i_value));
        System.out.println(covarianceSet(list,4,i_value));
        System.out.println(covarianceSet(list,0,n_value));
        System.out.println(covarianceSet(list,2,n_value));
        System.out.println(covarianceSet(list,4,n_value));
        System.out.println(covarianceSet(list,0,o_value));
        System.out.println(covarianceSet(list,2,o_value));
        System.out.println(covarianceSet(list,4,o_value));
    }

    public static void main(String[] args) {
        // Option<Option<Integer>> o = Option.of(Option.of(23));
        // System.out.println(o.flatMap2(x -> Option.of(x.get()*2)));
        aufgabe1();
    }
    static <T> Option<T> flatten(Option<Option<T>> opt){
        //return Option.ofNullable(opt.get()).get();
        if(opt.isPresent()){
            return opt.get();
        }else{
            return new None<T>();
        }
    }
}