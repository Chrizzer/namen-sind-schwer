package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe4;

import java.util.NoSuchElementException;
import java.util.function.Function;

/**
 * Created by Chris on 21.04.2017.
 */
public class None<T> extends Option<T> {

    public None(){}

    @Override
    public boolean isPresent() {
        return false;
    }

    @Override
    public T get() {
        throw new NoSuchElementException("None has no value");
    }

    @Override
    public T orElse(T other) {
        return other;
    }

    @Override
    public String toString() {
        return "None";
    }

    public <R> Option<R> map(Function<T, R> f){
        return new None<R>();
    }
    public <R> Option<R> flatMap(Function<T,Option<R>> f){
        return new None<R>();
    }
    public <R> Option<R> map2(Function<T, R> f){
        return new None<R>();
    }
    public <R> Option<R> flatMap2(Function<T,Option<R>> f){
        return new None<R>();
    }
}
