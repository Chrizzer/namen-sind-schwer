package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe4;

import java.util.function.Function;

/**
 * Created by Chris on 21.04.2017.
 */
public abstract class Option<T>
{
    public abstract T get();
    public abstract boolean isPresent();
    public abstract T orElse(T other);
    public abstract <R> Option<R> map2(Function<T, R> f);
    public abstract <R> Option<R> map(Function<T, R> f);
    public abstract <R> Option<R> flatMap(Function<T,Option<R>> f);

    public <R> Option<R> flatMap2(Function<T, Option<R>> f){
        return flatten(map(f));
    }

    public static <T> Option<T> of(T value){
            return new Some<T>(value);
    }
    public static <T> Option<T> ofNullable(T value){
        return value == null ? new None() : new Some(value);
    }
    public <T> Option<T> flatten(Option<Option<T>> opt){
        //return Option.ofNullable(opt.get()).get();
        if(opt.isPresent()){
            return opt.get();
        }else{
            return new None<T>();
        }
    }
}