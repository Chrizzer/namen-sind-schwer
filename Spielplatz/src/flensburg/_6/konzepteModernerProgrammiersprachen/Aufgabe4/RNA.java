package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe4;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Chris on 21.04.2017.
 */
public class RNA {
    enum Base{
        U,A,C,G
    }
    public static void main(String[] args) {
        Option<List<Base>> bases = parseRNA("ugacugcugaucgaucguagcacgu");
        if(bases.isPresent()){
            for (Base c : bases.get()) {
                System.out.print(c);
            }
        }else{
            System.out.println("Option was empty");
        }
        System.out.println("");
        bases = parseRNA("ugacugcugaucgaucguagcacguf");
        if(bases.isPresent()){
            for (Base c : bases.get()) {
                System.out.print(c);
            }
        }else{
            System.out.print("Option was empty");
        }
    }
    static Option<Base> parseBase(char c){
        switch(c){
            case 'u':
            case 'U':
                return Option.of(Base.U);
            case 'a':
            case 'A':
                return Option.of(Base.A);
            case 'g':
            case 'G':
                return Option.of(Base.G);
            case 'c':
            case 'C':
                return Option.of(Base.C);
            default:
                return Option.ofNullable(null);
        }
    }static Base translate(char c){
        switch(c){
            case 'u':
            case 'U':
                return Base.U;
            case 'a':
            case 'A':
                return Base.A;
            case 'g':
            case 'G':
                return Base.G;
            case 'c':
            case 'C':
                return Base.C;
        }
        return null;
    }

    static Option<List<Base>> complementRNA(String bases){
        boolean dirty = false;
        boolean valid = false;
        List<Base> returnList = new ArrayList<Base>();
        char[] chars = bases.toCharArray();

        int i = 0;
        while(i < chars.length && !dirty){
            char c = chars[i];
            Option<Base> o = complementOf(parseBase(c));
            if(o.isPresent()) {
                if(o.get() == Base.A){
                    // ich gehe davon aus, dass A in "bases" vorkommen muss und nicht in returnList
                    // Falls dem nicht so sei, dann müsste einfach == Base.A zu Base.U geändert werden
                    valid = true;
                }
                returnList.add(o.get());
                i++;
            }else{
                dirty = true;
            }
        }
        return (dirty && valid) ? Option.ofNullable(null) : Option.ofNullable(returnList);
    }
    static <T> boolean aggregate(List<T> c, Predicate<T> f){
        boolean b = true;
        for(T t : c){
            b = b && f.test(t);
        }
        return b;
    }
    static <T, R> List<R> map(List<T> c, Function<T, R> f){
        List<R> l = new ArrayList<R>();
        for(T t : c){
            l.add(f.apply(t));
        }
        return l;
    }
    static Option<List<Base>> parseRNA(String bases){
        char[] chars = bases.toCharArray();
        List<Character> m = new ArrayList<>();
        for (char c : chars){
            m.add(c);
        }
        Function<Character, Base> f = (x) -> parseBase(x).get();
        return Option.of(map(m, f));
    }
    static Option<List<Base>> parseRNA2(String bases){
        boolean dirty = false;
        List<Base> returnList = new ArrayList<Base>();
        char[] chars = bases.toCharArray();
        int i = 0;
        while(i < chars.length && !dirty){
            char c = chars[i];
            Option<Base> o = parseBase(c);
            if(o.isPresent()) {
                returnList.add(o.get());
                i++;
            }else{
                dirty = true;
            }
        }
        return dirty ? Option.ofNullable(null) : Option.ofNullable(returnList);
    }
    static Option<Base> complementOf(char c){
        switch(c){
            case 'u':
            case 'U':
                return Option.of(Base.A);
            case 'a':
            case 'A':
                return Option.of(Base.U);
            case 'g':
            case 'G':
                return Option.of(Base.C);
            case 'c':
            case 'C':
                return Option.of(Base.G);
            default:
                return Option.ofNullable(null);
        }
    }
    static Option<Base> complementOf(Option<Base> b){
        return complementOf(b.get());
    }
    static Option<Base> complementOf(Base b){
        switch(b){
            case U:
                return Option.of(Base.A);
            case A:
                return Option.of(Base.U);
            case G:
                return Option.of(Base.C);
            case C:
                return Option.of(Base.G);
            default:
                return Option.ofNullable(null);
        }
    }
}
