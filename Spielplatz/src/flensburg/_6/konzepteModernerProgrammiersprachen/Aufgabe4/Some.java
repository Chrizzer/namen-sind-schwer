package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe4;
import java.util.NoSuchElementException;
import java.util.function.Function;

/**
 * Created by Chris on 21.04.2017.
 */
public class Some<T> extends Option<T> {

    private final T value;

    public Some(T value){
        if(value == null){
            throw new NoSuchElementException("Cannot init Some with null");
        }else{
            this.value = value;
        }
    }
    @Override
    public boolean isPresent() {
        return true;
    }

    @Override
    public T get() {
        return value;
    }

    @Override
    public T orElse(Object other) {
        return this.value;
    }

    @Override
    public String toString() {
        return "Some("+this.value.toString()+")";
    }

    public <R> Option<R> map(Function<T, R> f){
        return ofNullable(f.apply(get()));
    }
    public <R> Option<R> flatMap(Function<T,Option<R>> f){
        if(isPresent()){
            return f.apply(get());
        }else{
            return new None<R>();
        }
    }
    public <R> Option<R> map2(Function<T, R> f){
        return this.flatMap(x -> ofNullable(f.apply(x)));
    }
}
