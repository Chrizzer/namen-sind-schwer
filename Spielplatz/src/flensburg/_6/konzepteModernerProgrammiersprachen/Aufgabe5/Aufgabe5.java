package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe5;

/**
 * Created by Chris on 11.05.2017.
 */
public class Aufgabe5 {

    public static void main(String[] args) {
        // Fold example
        Option<Integer> o = Option.of(21);
        System.out.println(o.fold(0,(x) -> x*x));

        Option<Integer> o1 = Option.of(42);
        Option<Option<Integer>> o2 = Option.of(o1);
        /*Map    */ System.out.println(o1.coMap(x -> x*x));
        /*Flatten*/ // Ich glaube jetzt, dass Flatten sehr ungeeignet ist als Objektmethode...
        /*FlatMap*/ // Probleme bei Implementation
        /*Fold   */ System.out.println(o1.coFold(o,integer -> integer%3*4));
        /*Filter */ // Probleme bei Implementation

        /*Map    */ System.out.println(o1.contraMap(x -> x*x));
        /*Flatten*/ // Ich glaube jetzt, dass Flatten sehr ungeeignet ist als Objektmethode...
        /*FlatMap*/ // Probleme bei Implementation
        /*Fold   */ // Compilerfehler
        /*Filter */ // Probleme bei Implementation
    }
}
