package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe5;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Chris on 21.04.2017.
 */
public class None<T> extends Option<T>{

    public None(){}
    public boolean isPresent() {
        return false;
    }
    public T get() {
        throw new NoSuchElementException("None has no value");
    }
    public T orElse(T other) {
        return other;
    }
    public String toString() {
        return "None";
    }

    public <R> Option<R> map(Function<T, R> f){
        return new None<R>();
    }
    public <R> Option<R> flatMap(Function<T, Option<R>> f){
        return new None<R>();
    }
    public <R> R fold(R initial, Function<T, R> f) {
        return initial;
    }
    public void foreach(Consumer<T> c) { /*Nichts?*/ }
    public Option<T> filter(Predicate<T> p) { return new None<T>(); }
    // Covariance
    public <R> Option<R> coMap(Function<T, ? extends R> f) {
        return new None<R>();
    }
    public <R> R coFold(R initial, Function<T, ? extends R> f) { return initial; }
    public Option<? extends T> coFilter(Predicate<? extends T> p) { return new None<T>(); }
    public <R> Option<? extends R> coFlatMap(Function<T, Option<? extends R>> f) { return new None<R>(); }
    public Option<? extends T> coFlatten(Option<Option<? extends T>> opt) { return new None<T>(); }
    // Contravariance
    public <R> Option<R> contraMap(Function<T, ? super R> f) {
        return new None<R>();
    }
    public <R> R contraFold(R initial, Function<T, ? super R> f) { return initial; }
    public Option<? super T> contraFilter(Predicate<? super T> p) { return new None<T>(); }
    public <R> Option<? super R> contraFlatMap(Function<T, Option<? super R>> f) { return new None<R>(); }
    public Option<? super T> contraFlatten(Option<Option<? super T>> opt) { return new None<T>(); }
}
