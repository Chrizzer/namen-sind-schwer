package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe5;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Chris on 21.04.2017.
 */
public abstract class Option<T>
{
    public abstract T get();
    public abstract boolean isPresent();
    public abstract T orElse(T other);
    public static <T> Option<T> of(T value){
        return new Some<T>(value);
    }
    public static <T> Option<T> ofNullable(T value){
        return value == null ? new None() : new Some(value);
    }

    // Funktionen höherer Ordnung
    public abstract <R> Option<R> map(Function<T, R> f);
    public abstract <R> R fold(R initial, Function<T,R> f);
    public abstract <R> Option<R> flatMap(Function<T, Option<R>> f);
    public <T> Option<T> flatten(Option<Option<T>> opt){
        if(opt.isPresent()){
            return opt.get();
        }else{
            return new None<T>();
        }
    }
    public abstract void foreach(Consumer<T> c);
    public abstract Option<T> filter(Predicate<T> p);
    // Aufgabe 3 - Funktionsvarianz
    /*Map    */ public abstract <R> Option<? extends R> coMap(Function<T, ? extends R> f);
    /*Flatten*/ public abstract Option<? extends T> coFlatten(Option<Option<? extends T>> opt);
    /*FlatMap*/ public abstract <R> Option<? extends R> coFlatMap(Function<T, Option<? extends R>> f);
    /*Fold   */ public abstract <R> R coFold(R initial, Function<T, ? extends R> f);
    /*Filter */ public abstract Option<? extends T> coFilter(Predicate<? extends T> p);

    /*Map    */ public abstract <R> Option<? super R> contraMap(Function<T, ? super R> f);
    /*Flatten*/ public abstract Option<? super T> contraFlatten(Option<Option<? super T>> opt);
    /*FlatMap*/ public abstract <R> Option<? super R> contraFlatMap(Function<T, Option<? super R>> f);
    /*Fold   */ public abstract <R> R contraFold(R initial, Function<T, ? super R> f);
    /*Filter */ public abstract Option<? super T> contraFilter(Predicate<? super T> p);
}