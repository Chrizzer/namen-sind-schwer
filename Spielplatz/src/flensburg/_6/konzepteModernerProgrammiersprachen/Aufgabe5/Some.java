package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe5;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Chris on 21.04.2017.
 */
public class Some<T> extends Option<T> {

    private final T value;
    public Some(T value){
        if(value == null){
            throw new NoSuchElementException("Cannot init Some with null");
        }else{
            this.value = value;
        }
    }
    public boolean isPresent() {
        return true;
    }
    public T get() {
        return value;
    }
    public T orElse(Object other) {
        return this.value;
    }
    public String toString() {
        return "Some("+this.value.toString()+")";
    }
    public <R> Option<R> map(Function<T, R> f){
        return ofNullable(f.apply(get()));
    }
    public <R> Option<R> flatMap(Function<T, Option<R>> f){
        if(isPresent()){
            return f.apply(get());
        }else{
            return new None<R>();
        }
    }
    public <R> Option<R> map2(Function<T, R> f){ return this.flatMap(x -> ofNullable(f.apply(x))); }
    // Aufgabe 1 - Faltung für Option
    public <R> R fold(R initial, Function<T, R> f) {
        /*  Ich verstehe nicht ganz wie ein Fold auf einen Option funktionieren
            soll, weil ja nicht immer eine Collection die gefolded werden kann
            im value gespeichert ist.
            Außerdem kann diese Methode in dieser Form auch in die Oberklasse.
        */
        if(isPresent()){
            return f.apply(get());
        }else{
            return initial;
        }
    }
    public <R> Option<R> map_fold(Function<T, R> f){
        // Warum geht das nicht?
        // Kann ich die Methode nicht einfach weiterreichen?
        // return new Option<R>(fold(get(),f));
        throw new NotImplementedException();
    }
    public <R> Option<R> flatMap_fold(Function<T, R> f){
        // Siehe oben
        // return new Option<R>(fold(get(),f));
        throw new NotImplementedException();
    }
    public Option<T> filter(Predicate<T> p){
        if(isPresent()){
            return (p.test(get())) ? Option.of(get()) : Option.ofNullable(null);
        }else{
            return Option.ofNullable(null);
        }
    }
    // Aufgabe 2
    public void foreach(Consumer<T> c){
        if(isPresent()){
            c.accept(get());
        }
    }
    // Aufgabe 3 - Funktionsvarianz
    public <R> Option<? extends R> coMap(Function<T, ? extends R> f) {
        return Option.of(f.apply(get()));
    }
    public <R> R coFold(R initial, Function<T, ? extends R> f) {
        if(isPresent()){
            return f.apply(get());
        }else{
            return initial;
        }
    }
    public Option<? extends T> coFilter(Predicate<? extends T> p) {
        // return (p.test(get())) ? Option.of(get()) : Option.ofNullable(null);
        throw new NotImplementedException();
    }
    public <R> Option<? extends R> coFlatMap(Function<T, Option<? extends R>> f) {
        // Hatte vorher schon Probleme damit...
        throw new NotImplementedException();
    }
    public Option<? extends T> coFlatten(Option<Option<? extends T>> opt) {
        return opt.get();
    }
    public <R> Option<? super R> contraMap(Function<T, ? super R> f) {
        return ofNullable(f.apply(get()));
    }
    public <R> R contraFold(R initial, Function<T, ? super R> f) {
        if(isPresent()){
            // return f.apply(get());
            throw new NotImplementedException();
        }else{
            return initial;
        }
    }
    public Option<? super T> contraFilter(Predicate<? super T> p) {
            return (p.test(get())) ? Option.of(get()) : Option.ofNullable(null);
    }
    public <R> Option<? super R> contraFlatMap(Function<T, Option<? super R>> f) {
        // Hatte vorher schon Probleme damit...
        throw new NotImplementedException();
    }
    public Option<? super T> contraFlatten(Option<Option<? super T>> opt) {
        return opt.get();
    }
}
