package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Chris on 12.05.2017.
 */
public class Aufgabe6 {
    static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,5,6);
        System.out.println(count(list));
    }
    // Aufgabe 1
    public static <T, R> R fold(Function<T,Function<R,R>> f, List<T> list, R initial){
        R acc = initial;
        for (T t: list) {
            acc = f.apply(t).apply(acc);
        }
        return acc;
    }
    static <T> Integer count(List<T> list){
        // Function<T, Function<Integer,Integer>> f = (x)->(y)->1+y;
        return fold((x)->(y)->1+y, list, Integer.valueOf(0));
    }

    static Integer sum(List<Integer> list){
        return fold((x)->(y)->x+y, list, Integer.valueOf(0));
    }
    static Integer prod(List<Integer> list){
        return fold((x)->(y)->x*y, list, Integer.valueOf(1));
    }
    static Boolean and(List<Boolean> list){
        return fold((x)->(y)->x&&y, list, true);
    }
    // Aufgabe 2
    static <T,R> List<R> flatMap(Function<T, List<R>> f, List<T> list){
        // In der Aufgabenstellung ist ein Syntaxfehler und ich gehe nun davon
        // aus, dass diese Parameterliste korrekt ist
        List<R> returnList = new ArrayList<R>();
        for (T t : list) {
            if(false)
                returnList = f.apply(t); // Niklas Springhorns Vorschlag?
            else
                returnList.add((f.apply(t)).get(0)); // Mein Vorschlag
        }
        return returnList;
    }
    static <T> List<T> flatten(List<List<T>> list){
        List<T> returnList = new ArrayList<T>();
        for(List<T> l : list){
            for(T t : l){
                returnList.add(t);
            }
        }
        return returnList;
    }
    static <T> List<T> flatten_(List<List<T>> list){
        return flatMap((x->x),list);
    }
    public static <T> List<T> map(Function<T,T> f, List<T> list){
        List<T> l = new ArrayList<T>(list);
        for(int i = 0; i < list.size(); i++){
            l.set(i,f.apply(list.get(i)));
        }
        return l;
    }
}
