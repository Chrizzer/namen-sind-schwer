package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.*;
public class Aufgabe7
{
    static <T, S, R> Function<T, Function<S, R>> curry(
        BiFunction<T, S, R> f) {
        return x -> y -> f.apply(x, y);
    }
    static <T, S, R> BiFunction<T, S, R> uncurry(
        Function<T, Function<S, R>> f) {
        return (x,y) -> f.apply(x).apply(y);
    }
    public static void main(String[] args){
        // let arr = [1,2,3,4,5,6,7,8,9,10]
        // arr.map(curry( (x:number,y:number) => console.log( Math.pow(x,y) ) )(2))
        List<Double> list = Arrays.asList(1d,2d,3d,4d,5d,6d,7d,8d,9d,10d);
        // BiFunction<Integer,Integer,Integer> f = (x,y)->;
        System.out.println(
                map( curry(Math::pow).apply(2d), list)
        );
        // Aufgabe 2
        List<Integer> ints = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        System.out.println(
                Aufgabe7.<Integer,Integer>foldl()
                        .apply(x -> acc -> x + acc)
                        .apply(0)
                        .apply(ints)
        );
        // partial application
        Function<Integer, Function<List<Integer>,Integer>> core =
                Aufgabe7.<Integer, Integer>foldl()
                        .apply(x -> acc -> acc - x);
        // Wir subtrahieren vom initial
        // von 55
        System.out.println(
                core.apply(55)
                    .apply(ints)
        );
        // von 42
        System.out.println(
                core.apply(42)
                    .apply(ints)
        );
        // Aufgabe 3
        System.out.println(
                reverse(ints) + " => " + (reverse(ints) == ints)
        );
        System.out.println(
                scan(x->acc->x*acc,1,ints)
        );
    }
    static <T> List<T> map(Function<T,T> f, List<T> list){
        for(int i = 0; i < list.size(); i++){
            list.set(i,f.apply(list.get(i)));
        }
        return list;
    }
    // Aufgabe 2 - Funktionen statt Methoden
    // foldl::(t->r->r)->r->[t]->r
    // Foldl als Funktion implementiert! Curried
    static <T,R> Function<Function<T, Function<R, R>>, Function<R, Function<List<T>, R>> > foldl(){
        return (f->initial->list->{
            R acc = initial;
            for(T t : list){
                acc = f.apply(t).apply(acc);
            }
            return acc;
        });
    }
    // Aufgabe 3 - Scan
    // Fold als Methode implementiert! Nicht gecurried
    static <T, R> R fold(Function<T,Function<R,R>> f, List<T> list, R initial){
        R acc = initial;
        for (T t: list) {
            acc = f.apply(t).apply(acc);
        }
        return acc;
    }
    static <T> List<T> reverse(List<T> list){
        return Aufgabe7.<T, List<T>>fold(
            x -> acc ->{
                //...
                List<T> tAcc = new ArrayList<T>();
                tAcc.add(x);
                tAcc.addAll(acc);
                return tAcc;
            },
            list,
            new ArrayList<T>()
        );
    }
    /*
    static <T> List<T> reverse(List<T> list){
        List<T> l = new ArrayList<T>();
        return Aufgabe7.<T,List<T>>scan(
                x -> acc -> {
                    List<T> tAcc = new ArrayList<T>();
                    tAcc.add(x);
                    return x;
                },
                list,
                new ArrayList<T>()
        );
    }
    */
    static <T,R> List<R> scan(Function<T,Function<R,R>> f, R init, List<T> list){
        R acc = init;
        List<R> l = new ArrayList<R>();
        for(T t : list){
            acc = f.apply(t).apply(acc);
            l.add(acc);
        }
        return l;
    }
}