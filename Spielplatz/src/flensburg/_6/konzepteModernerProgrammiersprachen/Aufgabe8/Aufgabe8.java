package flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe8;

import flensburg._6.konzepteModernerProgrammiersprachen.Aufgabe6.Aufgabe6;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Chris on 02.06.2017.
 */
public class Aufgabe8 {
    public static void main(String[] args) {
        List<Integer> is = Arrays.asList(1,2,3,4);
        Aufgabe6.map(Function.identity(),is).forEach(x->System.out.print(x+" "));
        System.out.println("Map(identity,[1,2,3,4])");
        // Die Funktion Function.identity verändert die Liste in keiner Weise.
        Aufgabe6.map(x -> 2 * x, Aufgabe6.map(x -> x +1,is)).forEach(x->System.out.print(x+" "));
        // Die map funktion wird auf eine gemapte Version von der liste angewendet.
        // Also aus is wird is -> x + 1 wird -> x' * 2
        // Mit einer map methode würde es
        System.out.println("map(2x,map(x+1,[1,2,3,4]))");
        Aufgabe6.map(x -> 2 * (x + 1),is).forEach(x->System.out.print(x+" "));
        System.out.println("map(2x+1,[1,2,3,4])");
        // map(f,map(g,list)) = map(f•g,list)
        Function<Integer,Integer> f = x -> x*2;
        Function<Integer,Integer> g = x -> x+1;
        Aufgabe6.map(x -> f.apply(g.apply(x)),is).forEach(x->System.out.print(x+" "));
        System.out.println("map(f•g),[1,2,3,4])");
        // Der void Typ hat keinen Rückgabetyp und kann dementsprechend in Funktionen nicht verwenden werden
        Function<Integer,Void> f2 = x -> {System.out.println(x); return null;};
        Function<Integer,Void> g2 = x -> {System.out.println(x); return null;};
        Aufgabe6.map(x -> f.apply(g.apply(x)),is).forEach(x->System.out.print(x+" "));
        System.out.println("map(f•g),[1,2,3,4]) f,g -> Void");
        // Aufgabe 4 - Allgemeine Strukturen
        // Der Ausdruck Option.of(x:T).flatMap(f:(T->Option<R>))
        // liefert einen Option<Option<R>> zurück.

        // Option<T> opt;
        // opt.flatMap(Option::of);
        // Dieser Ausdruck sollte das gleiche Objekt zurückliefern, allerdings ist es nicht
        // Dasselbe Objekt. Also ein neuer gleicher Option<T> allerdings nicht der selbe.

        // Bei dem flatMap Methodenaufruf auf eine Liste wird immer entweder
        // Die originalListe returned
        // Das Ergebnis der Funktion F, welches auf eine Liste ist
        // Oder Null.
    }
    // Freie Theoreme
    // Weil wir keine genauen Aussagen über den Typ T treffen können, außer dass er
    // immer als Objekt verarbeitet werden kann, gibt es nur x, y und null.
    static <T> T method1(T x, T y){
        return x;
    }
    static <T> T method2(T x, T y){
        return y;
    }
    static <T> T method3(T x, T y){
        return null;
    }
    static <T> T method4(T x, T y){
        if(x == null){
            return y;
        }else if(y == null){
            return x;
        }else{
            return null;
        }
    }
    static <T> T method5(T x, T y){
        return method5(x, y);
    }
}
