package freetime.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Crawler {

    private final String path;
    private final List<String> keywords;

    public Crawler(String[] args) throws RuntimeException {

        // first argument is path
        if (args.length < 1)
        {
            throw new RuntimeException("Wrong arguments bro");
        }
        path = args[0];
        keywords = skip(args, 1);
    }

    public String ReadFile(String path){
        File file = new File(path);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            return reader.lines().reduce("", (x, acc) -> acc + "\n" + x);
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    public <R> R CrawlFile(String path, String[] keywords, Function<Stream<String>, R> f){
        File file = new File(path);
        System.out.println("File "+ (file.exists() ? "exists" : "does not exist at " + path));
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            return f.apply(reader.lines());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static <T> List<T> skip(T[] array, int n){
        List<T> result = new ArrayList<T>();
        for (int i = n; i < array.length; i++){
            result.add(array[i]);
        }
        return result;
    }

    public static <T> boolean any(T[] array, Predicate<T> p) {
        boolean result = false;
        for (T t : array) {
            result |= p.test(t);
        }
        return result;
    }
}
