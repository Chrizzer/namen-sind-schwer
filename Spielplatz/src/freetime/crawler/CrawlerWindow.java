package freetime.crawler;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class CrawlerWindow extends JFrame{

    public CrawlerWindow(String path) {
        super();
        this.setSize(640, 640);
        this.setTitle("Crawler");

        Container container = this.getContentPane();
        container.setLayout(new BorderLayout());
        JTextField tf = new JTextField();
        tf.setSize(320, 60);
        JTextPane textPane = new JTextPane();

        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);

        doc.setParagraphAttributes(0, doc.getLength(), center, false);
        textPane.setPreferredSize(new Dimension(320,640));

        textPane.setBackground(Color.RED);
        JButton button = new JButton("Crawl");
        button.addActionListener(e ->
                {
                String input = tf.getText();
                Crawler c = new Crawler(new String[]{path, input});
                String[] keywords = input.split(" ");
                textPane.setText(
                        c.CrawlFile(path, keywords,
                        x -> x.filter(line ->
                                Crawler.any(keywords,
                                            keyword -> line.contains(keyword)))
                              .count())
                        .toString());
                }
            );

        container.add(tf, BorderLayout.PAGE_START);
        container.add(textPane, BorderLayout.CENTER);
        container.add(button, BorderLayout.PAGE_END);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
