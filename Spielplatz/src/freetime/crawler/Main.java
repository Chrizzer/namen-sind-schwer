package freetime.crawler;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class Main extends Application {

    static String path;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Crawler");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);
        grid.setAlignment(Pos.CENTER);
        TextField tf = new TextField();

        Text counterField = new Text();
        Text textPane = new Text();
        ScrollPane scroll = new ScrollPane(textPane);

        Button button = new Button("Crawl");
        button.setOnMouseClicked(e -> {
                    String input = tf.getText();
                    Crawler c = new Crawler(new String[]{path, input});
                    String[] keywords = input.split(" ");
                    textPane.setText(c.ReadFile(path));
                    counterField.setText(
                            c.CrawlFile(path, keywords,
                                    x -> x.filter(line ->
                                            Crawler.any(keywords,
                                                    keyword -> line.contains(keyword)))
                                            .count())
                                    .toString());
                });

        grid.add(tf, 0,0);
        grid.add(scroll, 0,1);
        grid.add(counterField, 0,2);
        grid.add(button, 0,3);

        Scene scene = new Scene(grid, 640, 640);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        path = args[0];
        launch(args);
    }
}
