package freetime.exercises.challenges.leetcode.easy.NonDecreasingArray;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

@LeetcodeProblem(
        link = "https://leetcode.com/problems/non-decreasing-array/description/",
        difficulty = Difficulty.Easy
)
public class NonDecreasingArray {
    public static void main(String[] args) {
        checkPossibility(new int[]{-1, 4,2,3});
        checkPossibility(new int[]{3, 4,2,3});
        checkPossibility(new int[]{2,3,3,2,4});
    }

    public static boolean checkPossibility(int[] nums) {
        int index = -1;
        for (int p = -1, i = 0, n = 1; i < nums.length - 1; p++, i++, n++) {
            if (0 <= p) {
                // can check left
                if (nums[p] > nums[i]) System.out.print(">");
            }
            System.out.print(nums[i]);
            if (n < nums.length - 1){
                // can lookahead
                if (nums[i] > nums[n]) System.out.print("<");
            }
            System.out.print(" ");
        }
        System.out.println(nums[nums.length - 1]);
        return true;
    }

    public static boolean checkPossibility2(int[] nums) {
        boolean result = true;
        for (int i = nums.length; 0 < i; i--) {
            int current = nums[i];
            int prev = nums[i-1];
            // Does the requirement hold
            result &= prev <= current;
            // Consider changing once ?
        }
        return result;
    }
}
