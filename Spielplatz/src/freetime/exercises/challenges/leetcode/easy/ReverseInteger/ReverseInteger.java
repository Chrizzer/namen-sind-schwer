package freetime.exercises.challenges.leetcode.easy.ReverseInteger;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

import java.util.function.BinaryOperator;

@LeetcodeProblem(
        link = "https://leetcode.com/problems/reverse-integer/description/",
        difficulty = Difficulty.Easy
)
public class ReverseInteger {

    public static void main(String[] args) {
        /*
        BITWISE COMPARISON OPERATORS
==========================================================
           # A # B # (A & B) # (A | B) # (A ^ B) #
           #######################################
           # 0 # 0 #    0    #    0    #    0    #
           # 1 # 0 #    0    #    1    #    1    #
           # 0 # 1 #    0    #    1    #    1    #
           # 1 # 1 #    1    #    1    #    0    #
==========================================================
        INFO: [BIT]
==========================================================
        Unary Complement "~" : Stellenweise [0] -> [1]

               0 = [00000000000000000000000000000000]
              ~0 = [11111111111111111111111111111111]
                 = -1

              42 = [00000000000000000000000000101010]
             ~42 = [11111111111111111111111111010101]
                 = -43
==========================================================

        BITWISE SHIFT OPERATIONS
==========================================================

        Signed Left Shift "<<" : Bit wandert nach links

         i << n = i * 2^n
              ^ n modulo 32, also i << 35 = i << 3

              1 = [00000000000000000000000000000001]
         1 << 4 = [00000000000000000000000000010000]
                = 16
             42 = [00000000000000000000000000101010]
        42 << 4 = [00000000000000000000001010100000]
                = 672
             42 = [00000000000000000000000000101010]
        42 << 4 = [00000000000000000000001010100000]
                = 672

        ==================================================
        Signed Right Shift ">>"

         i >> n = i / 2^n
        -i >> n = i / 2^n-1

             16 = [00000000000000000000000000010000]
        16 >> 4 = [00000000000000000000000000000001]
                = 1
        ==================================================
        Unsigned Right Shift [>>>]

         i >> n = i / 2^n
        -i >> n = i / 2^n-1

             16 = [00000000000000000000000000010000]
        16 >> 4 = [00000000000000000000000000000001]
                = 1
        */
//      bitwiseOperation("Left Shift", "<<", 0, 32, (a, b) -> a << b);
//      bitwiseOperation("Left Shift", "<<", 1, 32, (a, b) -> a << b);
//      bitwiseOperation("Left Shift", "<<", -1, 32, (a, b) -> a << b);
//      bitwiseOperation("Left Shift", "<<", 42, 32, (a, b) -> a << b);
//      bitwiseOperation("Right Shift", ">>", -1, 32, (a, b) -> a >> b);
//      bitwiseOperation("Right Shift", ">>", -42, 32, (a, b) -> a >> b);
//      bitwiseOperation("Right Shift", ">>", 1, 32, (a, b) -> a >> b);
//      bitwiseOperation("Unsigned Right Shift", ">>>", 1, 32, (a, b) -> a >>> b);
//      bitwiseOperation("Unsigned Right Shift", ">>>", -1, 32, (a, b) -> a >>> b);
        /*
        System.out.println("120 -> 021 = 21");
        System.out.println(asBinary(120));
        System.out.println(asBinary(21));
        System.out.println("10 -> 01 = 1");
        System.out.println(asBinary(10));
        System.out.println(asBinary(1));
        System.out.println("-120 -> -021 = -21");
        System.out.println(asBinary(-120));
        System.out.println(asBinary(-21));
        */
        System.out.println(reverse(1534236469));
//        System.out.println(reverse(1563847412));
//        Random r = new Random();
//        for (int v = Integer.MIN_VALUE, i = 0; i < 100; v += r.nextInt(1000000000), i++) {
//            System.out.println(v + " <=> " + reverse(v));
//        }
    }

    public static int bestSolutionExplained(int x) {
        int result = 0;
        final int $90thPercentile = Integer.MAX_VALUE / 10;
        final int _90thPercentile = Integer.MIN_VALUE / 10;
        while (x != 0) {
            int remainder = x % 10;
            x /= 10;
            boolean isResultInTop90Percentile_MAX = $90thPercentile < result;
            boolean isResultEQ10thPercentile_MAX = result == $90thPercentile;
            boolean isRemainderGT7 = 7 < remainder;

            if (isResultInTop90Percentile_MAX || (isResultEQ10thPercentile_MAX && isRemainderGT7)) return 0;

            boolean isResultInTop90Percentile_MIN = result < _90thPercentile;
            boolean isResultEQ10thPercentile_MIN = result == _90thPercentile;
            boolean isRemainderLTNegative8 = remainder < -8;

            if (isResultInTop90Percentile_MIN || (isResultEQ10thPercentile_MIN && isRemainderLTNegative8)) return 0;
            result = result * 10 + remainder;
        }
        return result;
    }

    public static int bestSolution(int x) {
        int result = 0;
        while (x != 0) {
            int remainder = x % 10;
            x /= 10;
            if (result > Integer.MAX_VALUE / 10 || (result == Integer.MAX_VALUE / 10 && remainder > 7)) return 0;
            if (result < Integer.MIN_VALUE / 10 || (result == Integer.MIN_VALUE / 10 && remainder < -8)) return 0;
            result = result * 10 + remainder;
        }
        return result;
    }

    // Top 85%
    public static int reverse(int x) {
        //         Integer.signum(int)
        int sign = (x >> 31) | (-x >>> 31);

        int result = 0;

        x *= sign;
        int topEnd = getTopEnd(x);

        if (topEnd == 1000000000 && (x % 10) > 2) return 0;
        for (int div = 1; 0 < x; div *= 10) {
            int rest = (x % 10);
            if (rest > 0) {
                int pow10 = topEnd / div;
//            System.out.print("x = " + x);
//            System.out.print(" --- ");
//            System.out.print("pow10 = " + pow10);
//            System.out.print(" --- ");
//            System.out.println(rest * pow10);
                if ((result + rest * pow10) > result) {
                    result += rest * pow10;
                } else {
                    // overflow
                    return 0;
                }
            }
            x /= 10;
        }
        return sign * result;
    }

    private static int getTopEnd(int x) {
        if (x < 100000) {
            if (x < 100) {
                if (x < 10) return 1;
                else return 10;
            } else {
                if (x < 1000) return 100;
                else {
                    if (x < 10000) return 1000;
                    else return 10000;
                }
            }
        } else {
            if (x < 10000000) {
                if (x < 1000000) return 100000;
                else return 1000000;
            } else {
                if (x < 100000000) return 10000000;
                else {
                    if (x < 1000000000) return 100000000;
                    else return 1000000000;
                }
            }
        }
    }
    public static String asBinary(int x) {
        String initial = Integer.toBinaryString(x);
        for (int i = initial.length(); i < 32; i++) {
            initial = "0" + initial;
        }
        return "[" + initial + "]";
    }

    public static void bitwiseOperation(String title, String o, int left, int max, BinaryOperator<Integer> op) {
        System.out.println(title + " \"" + o + "\"");
        System.out.println(asBinary(left) + " = " + left);
        for (int i = 1; i <= max; i++) {
            Integer result = op.apply(left, i);
            System.out.println(
                    asBinary(result) + " " + o + " " +
                            (i < 10 ? " " + i : i) +
                            " =" + (result < 0 ? result : " " + result)
            );
        }
        System.out.println();
    }
}
