package freetime.exercises.challenges.leetcode.easy.RomanToInteger;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

@LeetcodeProblem(
        link = "https://leetcode.com/problems/roman-to-integer",
        difficulty = Difficulty.Easy
)
public class RomanToInteger {
    public static void main(String[] args) {
        System.out.println(romanToInt("XXVII"));
        System.out.println(romanToInt("LIX"));
        System.out.println(romanToInt("VIII"));
        System.out.println(romanToInt("MMMCMXCIX"));
    }

    /*
        I = 1
        V = 5
        X = 10
        L = 50
        C = 100
        D = 500
        M = 1000
    */

    // TOP 4% First Try
    public static int romanToInt(String s) {
        int result = 0;
        for (int i = s.length() - 1; 0 <= i; i--) {
            char current = s.charAt(i);
            if (current == 'I') {
                result += 1;
            } else if (current == 'V') {
                if (0 <= i - 1 && s.charAt(i - 1) == 'I') {
                    result += 4;
                    i = i-1;
                } else {
                    result += 5;
                }
            } else if (current == 'X') {
                if (0 <= i - 1 && s.charAt(i - 1) == 'I') {
                    result += 9;
                    i = i-1;
                } else {
                    result += 10;
                }
            } else if (current == 'L') {
                if (0 <= i - 1 && s.charAt(i - 1) == 'X') {
                    result += 40;
                    i = i-1;
                } else {
                    result += 50;
                }
            } else if (current == 'C') {
                if (0 <= i - 1 && s.charAt(i - 1) == 'X') {
                    result += 90;
                    i = i-1;
                } else {
                    result += 100;
                }
            } else if (current == 'D') {
                if (0 <= i - 1 && s.charAt(i - 1) == 'C') {
                    result += 400;
                    i = i-1;
                } else {
                    result += 500;
                }
            } else {
                if (0 <= i - 1 && s.charAt(i - 1) == 'C') {
                    result += 900;
                    i = i-1;
                } else {
                    result += 1000;
                }
            }
        }
        return result;
    }
}