package freetime.exercises.challenges.leetcode.easy.TwoSum;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

import java.util.Arrays;

/**
 * Created by Chris on 19/03/2017.
 * ACCEPTED
 * Runtime: 37 ms 67,24%
 */
@LeetcodeProblem(
        link = "https://leetcode.com/problems/two-sum/description/",
        difficulty = Difficulty.Easy
)
public class TwoSum {

    public static void main(String[] args) {
        int[] peter = new int[]{
          -1,-2,-3,-4,-5
        };
        Arrays.stream(twoSum(peter, -8)).forEach(System.out::println);
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        mainLoop : for (int i = 0; i < nums.length; i++) {
            int t = nums[i];
            if(target <= t || t < target){
                for(int j = i+1; j < nums.length; j++){
                    int b = nums[j];
                    if((t+b) == target){
                        result[0] = i;
                        result[1] = j;
                        break mainLoop;
                    }
                }
            }
        }
        return result;
    }
}