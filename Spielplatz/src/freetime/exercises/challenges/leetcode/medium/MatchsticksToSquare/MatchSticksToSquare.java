package freetime.exercises.challenges.leetcode.medium.MatchsticksToSquare;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

import java.util.HashMap;
import java.util.Map;

@LeetcodeProblem(
        link = "https://leetcode.com/problems/matchsticks-to-square/",
        difficulty = Difficulty.Medium
)
public class MatchSticksToSquare {
    public static void main(String[] args) {
        makesquare(1,1,2,2,2);
    }
    public static boolean makesquare(int... nums) {
        if(nums == null || nums.length < 4){
            return false;
        }
        // u is the perimeter
        int u = 0;
        // a is the assumed side length
        int a = 0;
        Map<Integer, Integer> parts = new HashMap<Integer,Integer>();
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            int t = nums[i];
            u += t;
            parts.put(t,(parts.containsKey(t))? parts.get(t) + 1 : 1);
            if(a < t){
                a = t;
            }
        }
        // If the perimeter u cannot be divided by 4 it cant be a square with integers
        if(u%4 != 0){
            return false;
        }
        // b is the actual side length
        int b = u/4;
        len = parts.size();
        int t_u = 0;
        for (int i = 0; t_u < u; i++) {
            // assemble the parts until we reached the desired perimeter
            for (int j = 0; j < len; j++) {
                int part = parts.get(j);
            }
        }
        return false;
    }
}
