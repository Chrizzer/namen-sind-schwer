package freetime.exercises.challenges.leetcode.medium.ReorderList;

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }

    @Override
    public String toString() {
        return "ListNode "+val;
    }
}
