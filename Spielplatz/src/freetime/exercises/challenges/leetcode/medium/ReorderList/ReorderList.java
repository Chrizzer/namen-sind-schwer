package freetime.exercises.challenges.leetcode.medium.ReorderList;

import freetime.exercises.challenges.leetcode.util.Difficulty;
import freetime.exercises.challenges.leetcode.util.LeetcodeProblem;

@LeetcodeProblem(
        link = "https://leetcode.com/problems/reorder-list/description/",
        difficulty = Difficulty.Medium
)
public class ReorderList
{
    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(3);
        ListNode d = new ListNode(4);
        a.next = b;
        b.next = c;
        c.next = d;
        reorderList(a);
        System.out.println(a.val + " => "+a.next);
        System.out.println(d.val + " => "+d.next);
        System.out.println(b.val + " => "+b.next);
        System.out.println(c.val + " => "+c.next);
    }
    static void reorderList(ListNode head) {
        recursiveApproach(head.next, head, null);
    }
    private static void recursiveApproach(ListNode current, ListNode start, ListNode end){
        if(start == end){
            current.next = null;
        }else if(current.next != end){
            recursiveApproach(current.next, start, end);
        }else{
            ListNode a = current;
            ListNode b = start.next;
            start.next = a;
            a.next = b;
            recursiveApproach(b, b, a);
        }
    }
}
