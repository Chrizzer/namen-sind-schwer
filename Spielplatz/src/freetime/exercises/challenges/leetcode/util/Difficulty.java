package freetime.exercises.challenges.leetcode.util;

public enum Difficulty {
    Easy,
    Medium,
    Hard
}
