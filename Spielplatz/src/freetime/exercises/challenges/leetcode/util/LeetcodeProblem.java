package freetime.exercises.challenges.leetcode.util;

import java.lang.annotation.Documented;

/**
 * Represents a problem on leetcode
 */
@Documented
public @interface LeetcodeProblem {
    String link();
    Difficulty difficulty();
    boolean challenge() default false;
}
