package freetime.exercises.designpatterns.creational.abstract_factory;

public class ConcreteFactory1 implements ProductFactory {
    @Override
    public ProductA createProductA() {
        return new ConcreteProductA1();
    }

    @Override
    public ProductB createProductB() {
        return new ConcreteProductB1();
    }
}
