package freetime.exercises.designpatterns.creational.abstract_factory;

public class ConcreteFactory2 implements ProductFactory {
    @Override
    public ProductA createProductA() {
        return new ConcreteProductA2();
    }

    @Override
    public ProductB createProductB() {
        return new ConcreteProductB2();
    }
}
