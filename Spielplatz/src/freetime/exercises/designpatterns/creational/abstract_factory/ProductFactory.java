package freetime.exercises.designpatterns.creational.abstract_factory;

public interface ProductFactory {
    ProductA createProductA();
    ProductB createProductB();
}
