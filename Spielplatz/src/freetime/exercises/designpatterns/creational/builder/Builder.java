package freetime.exercises.designpatterns.creational.builder;

public interface Builder {
    void Reset();
    void step1();
    void step2();
    void step3();
}
