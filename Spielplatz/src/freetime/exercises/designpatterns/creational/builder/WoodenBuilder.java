package freetime.exercises.designpatterns.creational.builder;

public class WoodenBuilder implements Builder {

    Object house;

    @Override
    public void Reset() {
        // Haus Abreißen
    }

    @Override
    public void step1() {
        // Bla Bla 1
    }

    @Override
    public void step2() {
        // Bla Bla 2
    }

    @Override
    public void step3() {
        // Bla Bla 3
    }

    public Object getResult() {
        return house;
    }
}
