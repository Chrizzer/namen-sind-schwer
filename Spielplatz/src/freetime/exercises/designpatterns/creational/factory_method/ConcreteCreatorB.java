package freetime.exercises.designpatterns.creational.factory_method;

public class ConcreteCreatorB extends Creator {
    @Override
    Product createProduct() {
        return new ConcreteProductB();
    }
}
