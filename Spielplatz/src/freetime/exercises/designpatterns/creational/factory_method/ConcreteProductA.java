package freetime.exercises.designpatterns.creational.factory_method;

public class ConcreteProductA implements Product {
    @Override
    public void doStuff() {
        // Things!
    }
}
