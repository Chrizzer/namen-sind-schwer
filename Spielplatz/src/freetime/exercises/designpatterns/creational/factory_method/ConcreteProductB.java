package freetime.exercises.designpatterns.creational.factory_method;

public class ConcreteProductB implements Product {
    @Override
    public void doStuff() {
        // Things!
    }
}