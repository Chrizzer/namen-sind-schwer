package freetime.exercises.designpatterns.creational.factory_method;

public abstract class Creator {
    void someMethod(){
        // ...
    }

    /**
     * Factory Method
     * @return
     */
    abstract Product createProduct();
}