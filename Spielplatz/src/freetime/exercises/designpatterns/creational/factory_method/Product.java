package freetime.exercises.designpatterns.creational.factory_method;

public interface Product {
    void doStuff();
}
