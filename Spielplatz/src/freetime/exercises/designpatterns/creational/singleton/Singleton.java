package freetime.exercises.designpatterns.creational.singleton;

/**
 * A Singleton<pre>
 * + Global unique instance of a specific type
 * + Scope of the type defines
 * - Tight coupling / dependencies
 * </pre>
 */
public class Singleton {

    // The key to the Singleton design pattern is a static instance
    public static Singleton instance;

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
            // At this point you could fill the singleton instance with more data
        }
        return instance;
    }
}
