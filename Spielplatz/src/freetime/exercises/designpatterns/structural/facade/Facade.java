package freetime.exercises.designpatterns.structural.facade;

/**
 * A facade provides abstraction from a larger collection of fields and methods.
 * Possibly adding functionality and local fields and methods on top of them.
 */
public class Facade {

    private String aString;
    private int anInt;
    private float aFloat;
    private Object aObject;
}
