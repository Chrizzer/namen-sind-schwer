package freetime.exercises.exceptions;

import freetime.exercises.exceptions.unchecked.IndexOutOfBounds;
import freetime.exercises.exceptions.unchecked.Nullpointer;
import freetime.exercises.exceptions.util.ICause;
import freetime.exercises.exceptions.util.INamed;

import java.util.function.Supplier;

public class Main {

    public static <T extends ICause<? extends Throwable> & INamed> void handle(Supplier<T>... list) {
        for (Supplier<T> f : list) {
            try {
                System.out.print(f.get().name() + " caused ");
                f.get().cause();
            } catch (Throwable throwable) {
                System.out.print(throwable.getClass().getSimpleName());
            } finally {
                System.out.println("...");
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Exception Area\n");
        /**
         * Checked exceptions are checked at compile-time.
         * It means if a method is throwing a checked exception then it should handle the exception using
         * try-catch block or it should declare the exception using throws keyword, otherwise the program
         * will give a compilation error.
         */
        handle(
        );
        /**
         * Unchecked exceptions are not checked at compile time.
         * It means if your program is throwing an unchecked exception and
         * even if you didn’t handle/declare that exception, the program won’t
         * give a compilation error. Most of the times these exception occurs due
         * to the bad data provided by user during the user-program interaction.
         * It is up to the programmer to judge the conditions in advance, that can cause
         * such exceptions and handle them appropriately. All Unchecked exceptions are
         * direct sub classes of RuntimeException class.
         */
        handle(
                Nullpointer::new,
                IndexOutOfBounds::new
        );

        System.out.println("---");
    }
}
