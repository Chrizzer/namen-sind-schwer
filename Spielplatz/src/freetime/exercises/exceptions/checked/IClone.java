package freetime.exercises.exceptions.checked;

public interface IClone {
    default IClone clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
}