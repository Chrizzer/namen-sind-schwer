package freetime.exercises.exceptions.unchecked;

import freetime.exercises.exceptions.util.ICause;
import freetime.exercises.exceptions.util.INamed;

import java.util.ArrayList;
import java.util.List;

public class IndexOutOfBounds implements ICause<IndexOutOfBoundsException>, INamed {

    @Override
    public void cause() throws IndexOutOfBoundsException {
        List<?> list = new ArrayList<>();
        list.get(0);
    }

    @Override
    public String name() {
        return "IndexOutOfBounds";
    }
}
