package freetime.exercises.exceptions.unchecked;

import freetime.exercises.exceptions.util.ICause;
import freetime.exercises.exceptions.util.INamed;

public class Nullpointer implements ICause<NullPointerException>, INamed {

    @Override
    public void cause() throws NullPointerException {
        Object $null = null;
        $null.toString();
    }

    @Override
    public String name() {
        return "Nullpointer";
    }
}
