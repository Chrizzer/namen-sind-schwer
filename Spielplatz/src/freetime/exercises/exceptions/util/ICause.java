package freetime.exercises.exceptions.util;

@FunctionalInterface
public interface ICause<T extends Throwable> {
    void cause() throws T;
}
