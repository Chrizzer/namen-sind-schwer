package freetime.exercises.exceptions.util;

@FunctionalInterface
public interface INamed {
    String name();
}
