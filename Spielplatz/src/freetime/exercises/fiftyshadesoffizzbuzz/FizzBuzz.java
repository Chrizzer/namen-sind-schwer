package freetime.exercises.fiftyshadesoffizzbuzz;

import java.util.ArrayList;

public class FizzBuzz {

    public static void main(String[] args) {
        System.out.println(shade1(1, 31));
    }

    public static String shade1(int start, int end) {

        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = start; i < end; i++) {
            numbers.add(i);
        }

        return numbers.stream().map(x -> {
            String msg = "";
            if (x % 3 == 0) {
                msg += "fizz";
            }
            if (x % 5 == 0) {
                msg += "buzz";
            }
            return msg == "" ? x.toString() : msg;
        }).reduce("", (x, acc) -> x + " " + acc);
    }
}
