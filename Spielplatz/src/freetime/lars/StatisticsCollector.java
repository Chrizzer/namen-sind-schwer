package freetime.lars;

import java.util.ArrayList;

public class StatisticsCollector {

    ArrayList<Double> nummern = new ArrayList<>();

    public void addItem(double d) {
        nummern.add(d);
    }

    public int getCount() {
        int count = 0;

        for (int i = 0; i < nummern.size(); i++) {
            count++;
        }

        return count;
    }

    public double getSum() {
        double sum = 0;

        for (int i = 0; i < nummern.size(); i++) {
            double num = nummern.get(i);
            sum = sum + num;
            // sum += num;
        }

        return sum;
    }

    public double getMinimum() {
        double minimum = Double.POSITIVE_INFINITY;

        for (int i = 0; i < nummern.size(); i++) {
            double num = nummern.get(i);
            if (num < minimum) {
                minimum = num;
            }
        }

        return minimum;
    }

    public double getMaximum() {
        double maximum = Double.MIN_VALUE;

        for (int i = 0; i < nummern.size(); i++) {
            double num = nummern.get(i);
            if (maximum < num) {
                maximum = num;
            }
        }

        return maximum;
    }

    public static void main(String[] args) {
        int[] array = {5, 92, 42, 17, 53, 23, 13, 1, 6};

        ArrayList<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            list.add(num);
        }

    }
}
