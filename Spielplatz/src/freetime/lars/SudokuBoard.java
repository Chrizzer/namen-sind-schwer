package freetime.lars;

import java.util.Scanner;

public class SudokuBoard {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] spielfeld = new int[9][9];
        /*
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                spielfeld[i][j] = 0;
            }
        }
        */
        String input = "";
        printField(spielfeld);
        while (scanner.hasNext()) {
            input = scanner.nextLine();
            if (input.equalsIgnoreCase("abort")) {
                System.out.println("Goodbye");
                break;
            } else {
                String[] aufgebröselt = input.split("-");
                int x = Integer.parseInt(aufgebröselt[0]);
                int y = Integer.parseInt(aufgebröselt[1]);
                int v = Integer.parseInt(aufgebröselt[2]);
                spielfeld[x][y] = v;
                // isFertig oder gewonnen
                printField(spielfeld);
            }
        }
    }

    public static void printField(int[][] spielfeld) {
        String feld = "";
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                feld += spielfeld[i][j];
                if (j + 1 < 9) {
                    feld += " ";
                }
            }
            if (i + 1 < 9) {
                feld += "\n";
            }
        }
        System.out.println(feld);
    }

}
