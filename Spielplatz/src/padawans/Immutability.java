package padawans;

public class Immutability {
    public static void main(String[] args) {
        // Immutability
        final Integer a = 42;
        final int b = 42;
        StaticExample e = new StaticExample();
        System.out.println(e instanceof Object);
    }

    void notSoMain() {
        Example e = new Example();
        StaticExample se = new StaticExample();
    }

    static class StaticExample {
        int something = 2;
    }
}

class Example {
    int somethingElse = 3;
}