package work.bijoubrigitte.com.company;

import java.math.BigDecimal;
import java.util.List;
import work.bijoubrigitte.com.company.jparsec.*;

public class CSVParser {

    private static final Character DELIMITER_CHAR = ';';
    private static final Character QUOTATION_MARK = '\"';

    private static Parser<Void> whitespace = Scanners.isChar(Character::isWhitespace).label("whitespace");
    private static Parser<List<Void>> whitespaces = whitespace.many().label("whitespaces");

    private static Parser<Object> lineEnd = Parsers.or(Scanners.isChar('\n'), Parsers.EOF).label("EndOfLine/File");
    private static Parser<Void> delimiter = Scanners.isChar(DELIMITER_CHAR).label("Delimiter");
    private static Parser<Void> quote = Scanners.isChar(QUOTATION_MARK);

    private static Parser<String> wert = Parsers.or(
            Parsers.sequence(
                    quote,
                    Scanners.notChar(QUOTATION_MARK).many1().source(),
                    quote,
                    (q, val, p) -> {
                        /*System.out.println("aVALUE = " + val);*/ return val;
                    }
            ),
            Scanners.notChar(DELIMITER_CHAR).many1().source()
    );

    private static Parser<String> optionalerWert = Parsers.or(
            Parsers.sequence(
                    quote,
                    Scanners.notChar(QUOTATION_MARK).many().source(),
                    quote,
                    (q, val, p) -> {
                        /*System.out.println("oVALUE = " + val);*/ return val;
                    }
            ),
            // X
            Scanners.notChar(DELIMITER_CHAR).many().source()
    );

    private static Parser<String> letzterWert = Parsers.or(
            Parsers.sequence(
                    quote,
                    Scanners.notChar('\n').many().source(),
                    quote,
                    (q, val, p) -> {
                        /*System.out.println("oVALUE = " + val);*/ return val;
                    }
            ),
            // X
            Scanners.notChar('\n').many().source()
    );

    private static Parser<String> line = Parsers.sequence(
            wert.followedBy(delimiter).label("id"),
            wert.followedBy(delimiter).label("bijounummer"),
            optionalerWert.followedBy(delimiter).label("farbe1"),
            optionalerWert.followedBy(delimiter).label("farbe2"),
            optionalerWert.followedBy(delimiter).label("bild1"),
            optionalerWert.followedBy(delimiter).label("bild2"),
            optionalerWert.followedBy(delimiter).label("bild3"),
            letzterWert.label("bild4"),
            (id, bijounummer, farbe1, farbe2, bild1, bild2, bild3, bild4) -> {
                System.out.println("id = " + id);
                System.out.println("bijounummer = " + bijounummer);
                return "Artikel[" + id + "](" + new BigDecimal(bijounummer) + ")\n " +
                        "\tFarben: " + farbe1 + ", " + farbe2 + "\n" +
                        "\tBild1: " + bild1 + "\n" +
                        "\tBild2: " + bild2 + "\n" +
                        "\tBild3: " + bild3 + "\n" +
                        "\tBild4: " + bild4 + "\n";
            }
    ).label("line");

    public static String parseLine(String csvContent) {
        return line.parse(csvContent);
    }

    public static List<String> parseLines(String csvContent) {
        return line.many().parse(csvContent);
    }
}
