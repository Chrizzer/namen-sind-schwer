package work.bijoubrigitte.com.company;

import java.util.List;
import work.bijoubrigitte.com.company.jparsec.*;

public class ECEParser {

    private static Parser<Void> whitespace = Scanners.isChar(Character::isWhitespace).label("whitespace");
    private static Parser<List<Void>> whitespaces = whitespace.many().label("whitespaces");

    public static String replaceHTML(String csv) {
        return bElement().many().source().parse(csv);
    }

    private static Parser<String> value = Scanners.notChar('<').many().source();

    private static Parser<Void> tag(String tag) {
        return Parsers.sequence(
                Scanners.isChar('<'),
                Scanners.string(tag),
                Scanners.notChar('>').many(),
                Scanners.isChar('>')
        ).label("<tag?>");
    }

    private static Parser<Void> shortTag(String tag) {
        return Parsers.sequence(
                Scanners.isChar('<'),
                Scanners.string(tag),
                Scanners.notChar('/').many(),
                Scanners.isChar('/'),
                Scanners.isChar('>')
        ).label("<shortTag?/>");
    }

    private static Parser<String> fallbackTag = Parsers.sequence(
            Scanners.isChar('<'),
            Scanners.notChar('/').many(),
            Scanners.isChar('/'),
            Scanners.isChar('>')
    ).source().label("<shortTag?/>");

    private static Parser<Void> br = shortTag("br");

    private static Parser<Void> paragraphOpen = tag("p");
    private static Parser<Void> fontOpen = tag("font");
    private static Parser<Void> divOpen = tag("div");
    private static Parser<Void> boldOpen = tag("b");

    private static Parser<Void> paragraphClose = tag("/p");
    private static Parser<Void> fontClose = tag("/font");
    private static Parser<Void> divClose = tag("/div");
    private static Parser<Void> boldClose = tag("/b");

    private static Parser<Void> emptyParagraph = Parsers.sequence(paragraphOpen, paragraphClose);
    private static Parser<Void> paragraph = Parsers.sequence(paragraphOpen, value, paragraphClose);
    private static Parser<Void> boldText = Parsers.sequence(boldOpen, value, boldClose);
    private static Parser<Void> font = Parsers.sequence(fontOpen, value, fontClose);
    private static Parser<Void> div = Parsers.sequence(divOpen, value, divClose);
    private static Parser<Void> emptyDiv = Parsers.sequence(divOpen, divClose);

    private static Parser<Void> anyTag = Parsers.sequence(
            Scanners.isChar('<'),
            Scanners.notChar('>').many1(),
            Scanners.isChar('>')
    ).label("aTag");

    private static Parser<String> bElement() {
        Parser.Reference<String> content = Parser.newReference();
        Parser<String> element = Parsers.or(
                anyTag,
                Parsers.or(
                        content.lazy(),
                        Scanners.notChar('<').many1()
                ),
                whitespaces,
                br,
                emptyParagraph,
                paragraph,
                boldText,
                font,
                div,
                emptyDiv,
                fallbackTag
        ).source();
        content.set(element.many().source());
        return element;
    }
}
