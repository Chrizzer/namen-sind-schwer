package work.bijoubrigitte.com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import work.bijoubrigitte.com.company.datatypes.Node;
import work.bijoubrigitte.com.company.jparsec.Parser;
import work.bijoubrigitte.com.company.jparsec.error.ParserException;

public class Main {

    public static void main(String[] args) throws IOException {

//        System.out.println(ECEParser.replaceHTML("<p><br /> <b>Material:</b> Silberfarbene Ohrclips in Tropfenform mit hellblauem <a href=\"javascript:FensterNeu('Katzenauge','420','200')\">Katzenauge</a> und einem <a href=\"javascript:FensterNeu('Strass','420','200')\">Strassstein</a>.<br /> <br /> <b>Länge:</b> 2,2 cm<br /> <br /> <b>Breite:</b> 1 cm</p>"));
        List<String> list = Files.readAllLines(
                Paths.get("C:\\ProgramData\\Entwicklung\\UIParser\\src\\test.xhtml")
        );
        String str = list.stream().reduce("", (s, s2) -> s + "\n" + s2, (s, s2) -> s + s2);

        str = str.replaceAll("([\\s\\w<>!a-zA-Z\".?=\\-\\/:]*)(<html.*>)", "");

        System.out.println(XMLParser3.document().parse(str));

        // CLEAN STUFF NOW
        /*
        // whitespaces
        
        test(" ", " ", XMLParser.whitespace.source());
        test("\t", "\t", XMLParser.whitespace.source());
        test("\n", "\n", XMLParser.whitespace.source());

        test(" ", " ", XMLParser.whitespaces.source());
        test("  ", "  ", XMLParser.whitespaces.source());
        test("              \n          ", "              \n          ", XMLParser.whitespaces.source());
        test("            \t          ", "            \t          ", XMLParser.whitespaces.source());
        test("          \t  \t          ", "          \t  \t          ", XMLParser.whitespaces.source());
        test(" \n         \t  \t          ", " \n         \t  \t          ", XMLParser.whitespaces.source());
        test("                        ", "                        ", XMLParser.whitespaces.source());
        
        // terminals

        test("=", "=", XMLParser.eq);

        test("'", "'", XMLParser.singleQuote);
        test("\"", "\"", XMLParser.doubleQuote);

        test("<", "<", XMLParser.open);
        test(">", ">", XMLParser.close);

        test("/", "/", XMLParser.slash);
        test("\\", "\\", XMLParser.backslash);

        test(":", ":", XMLParser.colon);
        test(".", ".", XMLParser.dot);
        
        // Letters, Characters, digits

        String[] chars = new String[]{
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                ".", ":", ",", "#", "+", "-", "<", ">", "!", "?", "\"", "'", "$", "{", "}", "/", "\\", "(", ")", "[", "]", "="
        };
        for (String s : chars) {
            test(s, s, XMLParser.letter);
        }
        for (String s : chars) {
            test(s, s, XMLParser.character);
        }
        for (String s : chars) {
            test(s, s, XMLParser.digit);
        }

        test("abc", "abc", XMLParser.letters);
        test("abc", "abc", XMLParser.characters);
        test("", "abc", XMLParser.digits);

        
        test("","123", XMLParser.letters);
        test("123","123", XMLParser.characters);
        test("123","123", XMLParser.digits);

        // identifier
        test("a", "a", XMLParser.identifier);
        test("aa", "aa", XMLParser.identifier);
        test("aa:aa", "aa:aa", XMLParser.identifier);
        test("?aa", "?aa", XMLParser.identifier);
        test("?aa:aa", "?aa:aa", XMLParser.identifier);
        test("!aa", "!aa", XMLParser.identifier);
        test("!aa:aa", "!aa:aa", XMLParser.identifier);

        // Properties

        Property p = new Property("name", "test");
        test(p, "name=\"test\"", XMLParser.property);
        test(p, "name =\" test \"", XMLParser.property);
        test(p, "name = \" test \"", XMLParser.property);

        Property p2 = new Property("name", "http://xmlns.jcp.org/jsf/core");
        test(p2, "name =\" http://xmlns.jcp.org/jsf/core\"", XMLParser.property);

        test(Property.noProperties(), "", XMLParser.properties);

        ArrayList<Property> props = new ArrayList<>();
        props.add(new Property("a", "1"));
        props.add(new Property("abc", "ä#qfjoüqüo:pj123"));
        props.add(new Property("ag", "42"));

        test(props, "a=\"1\" abc=\"ä#qfjoüqüo:pj123\" ag=\"42\"", XMLParser.properties);
        test(props, "a=\"1\"\n abc=\"ä#qfjoüqüo:pj123\"  \n  ag=\"42\" ", XMLParser.properties);

        // shortNode

        props = new ArrayList<>();
        props.add(new Property("a", "1"));

        Node n = Node.withProperties("kappa", props);

        test(n, "<kappa a=\"1\"/>", XMLParser.shortNode);
        test(n, "<kappa    a=\"1\"/>", XMLParser.shortNode);
        test(n, "<kappa    a  =  \"1\"  />", XMLParser.shortNode);

        System.out.println(XMLParser.shortNode.parse("<a/>"));
        System.out.println(XMLParser.shortNode.parse("<aa/>"));
        System.out.println(XMLParser.shortNode.parse("<aa:aa/>"));
        System.out.println(XMLParser.shortNode.parse("<!aa/>"));
        System.out.println(XMLParser.shortNode.parse("<?aa/>"));
        System.out.println(XMLParser.shortNode.parse("<!aa:aa />"));

*/
        /*
        Node nt = new Node("t");
//        test(nt, "<t>", XMLParser.sTag);
//        test("t", "</t>", XMLParser.eTag);
//        test("", "<t>", XMLParser.eTag);
//        test(nt, "</t>", XMLParser.sTag);

        System.out.println(XMLParser.fullNode().parse("<t a=\"13\"><b/></t>"));
        test(nt,"<t></t>", XMLParser.fullNode());
        */

//        System.out.println(CSVParser.parseLine("21253;\"101198610\";\"schwarz\";\"schwarz\";\"/bilder_aus_WWS/jyAFQUxYlK_101198610_14_95_001.jpg/6f/a2/15/jyAFQUxYlK_101198610_14_95_001.jpg\";\"/bilder_aus_WWS/gmMDZPAvSC_101198610_14_95_002.jpg/24/88/ef/gmMDZPAvSC_101198610_14_95_002.jpg\";;"));
        /*System.out.println(CSVParser.parseLines("21253;\"101198610\";\"schwarz\";\"schwarz\";\"/bilder_aus_WWS/jyAFQUxYlK_101198610_14_95_001.jpg/6f/a2/15/jyAFQUxYlK_101198610_14_95_001.jpg\";\"/bilder_aus_WWS/gmMDZPAvSC_101198610_14_95_002.jpg/24/88/ef/gmMDZPAvSC_101198610_14_95_002.jpg\";;\n" +
                "21254;\"101200760\";\"schwarz\";\"silber\";\"/bilder_aus_WWS/iBUXMgREis_101200760_14_95_001.jpg/6f/b5/fb/iBUXMgREis_101200760_14_95_001.jpg\";\"/bilder_aus_WWS/cRXApmFaJs_101200760_14_95_002.jpg/b5/e9/28/cRXApmFaJs_101200760_14_95_002.jpg\";;\n" +
                "22208;\"10121515\";\"silber\";\"weiß\";\"/bilder_aus_WWS/FvwHfIFRfi_010121515_5_95.jpg/42/ad/16/FvwHfIFRfi_010121515_5_95.jpg\";;;"));
*/
        /*
        String csvFile = "C:\\Users\\CStarck\\Downloads\\s_articles.csv";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String lines = "";
            String line = br.readLine();
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i < 10){
                    i++;
                    lines += line + "\n";
                } else {
                    i = 0;
                    CSVParser.parse(lines);
                }
            }
        } catch (Exception e){
            throw e;
        }
*/
    }

    public static <T> void test(T expected, String s, Parser<T> parser) {
        System.out.print("Test " + parser.toString() + " :");
        try {
            expected.equals(parser.parse(s));
        } catch (ParserException e) {
            if (e.getErrorDetails() != null) {
                int index = e.getErrorDetails().getIndex();
                if (expected instanceof String) {
                    String ex = (String) expected;
                    if (ex.isEmpty()) {
                        System.out.println(" failed accordingly on " + s);
                        return;
                    }
                }
                int len = s.length();
                System.out.print(" failed ");
                if (0 < index) {
                    System.out.print("' at " + s.substring(0, index));
                }
                if (index < len) {
                    System.out.print("__|>'" + s.charAt(index) + "'<|__");
                }
                if (index + 1 < len) {
                    System.out.print(s.substring(index + 1));
                }
                System.out.println();
            }
            e.printStackTrace();
            return;
        }
        if (expected instanceof String) {
            String ex = (String) expected;
            if (ex.isEmpty()) {
                System.out.println(" passed unexpectedly on " + s);
                return;
            }
        }
        System.out.println(" passed '" + s + "'");
    }

    public static void test(String str) {
        Node n = null;
        try {
            n = XMLParser2.document().parse(str);
        } catch (ParserException e) {
            int index = e.getErrorDetails().getIndex();
            int len = str.length();
            if (index + 1 < len)
                str = str.substring(0, index) + "\n====================\n" + e.getMessage() + "\n====================\n-> [" + str.charAt(index) + "] <-\n" + str.substring(index + 1, len - 1);
            e.printStackTrace();
            System.out.println("\n\nERRORRR\n\n");
        }
        System.out.println(str);
        System.out.println();
        System.out.println(n);
        System.out.println();
    }

    public static String indent(String s, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += " ";
        }
        result += s;
        return result;
    }
}