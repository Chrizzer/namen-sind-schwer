package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.datatypes.Node;
import work.bijoubrigitte.com.company.javafp.parsecj.Reply;
import work.bijoubrigitte.com.company.javafp.parsecj.input.Input;
import work.bijoubrigitte.com.company.javafp.parsecj.input.StringInput;
import work.bijoubrigitte.com.company.jparsec.Parser;
import work.bijoubrigitte.com.company.jparsec.error.ParserException;

public class Main2 {

    public static void main(String[] args) {
        /*shortNodeTest();
        System.out.println();
        weirdTagTest();
        System.out.println();*/
//        fullNodeTest();
        lazyNodeTest();
    }


    private static void lazyNodeTest() {
        String[] strings = {
                "<a></a>",
                "<a>   </a>",
                "<a >   </a>",
                "<a> b </a>",
                "<a id=val> </a>",
                "<a id=val id=val> a\nb</a>",
                "<a\nid=val\nid=val><b></b></a>"
        };

        for (String s : strings) {
            Reply<Character, String> parse = ParserJParser.dreckigsterParserImWildenWesten.parse(Input.of(s));
            System.out.println(parse.<String>match(
                    ok -> "Ok{ result=\"" + ok.result + "\" remaining=\"" +((StringInput) ok.rest).getCharSequence() + "\" }",
                    e -> e.toString()
            ));
        }
    }

    private static void fullNodeTest() {
        String[] strings = {"<a></a>", "<a>   </a>", "<a> b </a>", "<a id=val> </a>", "<a id=val id=val> a\nb</a>", "<a\nid=val\nid=val><b></b></a>"};

        for (String s : strings) {
            System.out.println(ParserJParser.fullNode.parse(Input.of(s)).<String>match(
                    ok -> "Ok{ " + ok.getResult() + " }",
                    e -> "Error{ " + e.getMsg() + " }"
            ));
        }
    }

    private static void weirdTagTest() {
        String[] strings = {
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">",
                "<!DOCTYPE html\n" +
                        "                PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n" +
                        "                \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">",
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">"
        };
        for (String s : strings) {
            System.out.println(ParserJParser.weirdTag.parse(Input.of(s)));
        }
    }

    private static void shortNodeTest() {
        String[] strings = {"<a/>", "<a id=val/>", "<a id=val id=val/>", "<a\nid=val\nid=val/>"};

        for (String s : strings) {
            System.out.println(ParserJParser.shortNode.parse(Input.of(s)));
        }
    }

    public static <T> void test(T expected, String s, Parser<T> parser) {
        System.out.print("Test " + parser.toString() + " :");
        try {
            expected.equals(parser.parse(s));
        } catch (ParserException e) {
            if (e.getErrorDetails() != null) {
                int index = e.getErrorDetails().getIndex();
                if (expected instanceof String) {
                    String ex = (String) expected;
                    if (ex.isEmpty()) {
                        System.out.println(" failed accordingly on " + s);
                        return;
                    }
                }
                int len = s.length();
                System.out.print(" failed ");
                if (0 < index) {
                    System.out.print("' at " + s.substring(0, index));
                }
                if (index < len) {
                    System.out.print("__|>'" + s.charAt(index) + "'<|__");
                }
                if (index + 1 < len) {
                    System.out.print(s.substring(index + 1));
                }
                System.out.println();
            }
            e.printStackTrace();
            return;
        }
        if (expected instanceof String) {
            String ex = (String) expected;
            if (ex.isEmpty()) {
                System.out.println(" passed unexpectedly on " + s);
                return;
            }
        }
        System.out.println(" passed '" + s + "'");
    }

    public static void test(String str) {
        Node n = null;
        try {
            n = XMLParser2.document().parse(str);
        } catch (ParserException e) {
            int index = e.getErrorDetails().getIndex();
            int len = str.length();
            if (index + 1 < len)
                str = str.substring(0, index) + "\n====================\n" + e.getMessage() + "\n====================\n-> [" + str.charAt(index) + "] <-\n" + str.substring(index + 1, len - 1);
            e.printStackTrace();
            System.out.println("\n\nERRORRR\n\n");
        }
        System.out.println(str);
        System.out.println();
        System.out.println(n);
        System.out.println();
    }

    public static String indent(String s, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += " ";
        }
        result += s;
        return result;
    }
}