package work.bijoubrigitte.com.company;

import java.util.List;
import java.util.stream.Collectors;
import work.bijoubrigitte.com.company.jparsec.*;
public class MyParser {

    public static Parser<Void> eq = Scanners.isChar('=').between(whitespace(), whitespace()).label("[=]");
    public static Parser<Void> quote = Scanners.isChar('\"').between(whitespace(), whitespace()).label("[\"]");
    public static Parser<Void> open = Scanners.isChar('<').between(whitespace(), whitespace()).label("[<]");
    public static Parser<Void> slash = Scanners.isChar('/').between(whitespace(), whitespace()).label("[/]");
    public static Parser<Void> close = Scanners.isChar('>').between(whitespace(), whitespace()).label("[>]");

    public static Parser<List<Void>> whitespace() {
        return Scanners.WHITESPACES.many().label("whitespace");
    }

    public static Parser<String> identifier() {
        return Terminals.Identifier.TOKENIZER.map(Tokens.Fragment::text).between(whitespace(), whitespace()).label("identifier");
    }

    public static Parser<String> property() {
        return Parsers.sequence(
                identifier(),
                eq,
                quote,
                (Scanners.isChar(Character::isLetterOrDigit).source().many1()).source().label("[anythingBut]"),
                quote,
                (id, $1, $2, value, $3) -> id + " = \"" + value + "\""
        ).label("property");
    }

    public static Parser<String> shortNode() {
        return Parsers.sequence(
                open,
                identifier(),
                property().many().asOptional(),
                slash,
                close,
                ($1, id, properties, $2, $3) ->
                        "weirdNode => " + String.join(
                                System.getProperty("line.separator"),
                                Main.indent("shortNode{", 0),
                                Main.indent("Identifier : " + id, 4),
                                properties.isPresent() && properties.get().size() > 0 ?
                                        String.join(
                                                System.getProperty("line.separator"),
                                                Main.indent("Properties : {", 4),
                                                Main.indent(
                                                        properties.get().stream()
                                                                .map(s -> Main.indent(s, 4))
                                                                .collect(Collectors.joining(",\n\t"))
                                                        , 4),
                                                Main.indent("}", 4)
                                        ) : "No Properties",
                                "}"
                        )
        ).label("shortNode");
    }

    public static Parser<String> weirdNode() {
        return Scanners.isChar(Character::isDefined).label("character").many().between(
                open, close.optional()
        ).source()
         .map(s -> "weirdNode => " + s)
         .label("weirdNode");
    }

    public static Parser<String> fullNode() {
        Parser.Reference<String> content = Parser.newReference();
        Parser<String> fullNode = Parsers.sequence(
                open,
                identifier(),
                property().many().asOptional(),
                close,
                content.lazy().many().asOptional(),
                open,
                slash,
                identifier(),
                close
        ).or(weirdNode())
         .or(shortNode())
         .map(o -> "fullNode => " + o.toString()).label("fullNode");
        content.set(fullNode);
        return fullNode;
    }

    public static Parser<String> document() {
        return fullNode().source().label("document");
    }
}
