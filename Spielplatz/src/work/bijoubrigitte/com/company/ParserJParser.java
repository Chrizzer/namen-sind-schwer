package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.javafp.data.*;
import work.bijoubrigitte.com.company.javafp.parsecj.*;
import static work.bijoubrigitte.com.company.javafp.parsecj.Combinators.*;
import static work.bijoubrigitte.com.company.javafp.parsecj.Text.*;

public class ParserJParser {


    private static final Parser.Ref<Character, String> nodes = Parser.ref();

    public static final Parser<Character, Unit> eof = Combinators.eof();

    /* Test stuff
    public static final Parser<Character, Character> a = Text.chr('a');
    public static final Parser<Character, Character> b = Text.chr('b');
    public static final Parser<Character, String> ab =
            a.bind($a ->
            b.bind($b -> Combinators.retn("" + $a + $b))
    );
    */
    public static final Parser<Character, Character> eq = label(chr('='), "equal sign");
    public static final Parser<Character, Character> open = label(chr('<'), "opening bracket");
    public static final Parser<Character, Character> close = label(chr('>'), "closing bracket");
    public static final Parser<Character, Character> slash = label(chr('/'), "slash");

    public static final Parser<Character, Character> letter = label(satisfy(Character::isLetter), "letter");
    public static final Parser<Character, String> letters = label(
            letter.many1().bind(list -> retn(IList.listToString(list))),
            "letters");

    public static final Parser<Character, String> property =
            letters.bind(id ->
                    eq.then(
                            letters.bind(value ->
                                    retn("\"" + id + "=" + value + "\"")
                            ).label("value")
                    )).label("property");

    public static final Parser<Character, String> tag =
            strBetween(open, close).bind(result ->
                    retn("<" + result + ">")
            );

    public static final Parser<Character, String> realTag =
            open.then(
                    letters.bind(name ->
                            wspaces.bind(ws ->
                                    property.sepEndBy(wspaces).bind(properties ->
                                            close.bind(irrelevant ->
                                                    retn(name + "[" + properties.size() + "]")
                                            )))));

    public static final Parser<Character, String> longNode =
            realTag.bind(openTag ->
                    choice(wspaces, letters).bind(inner ->
                            open.then(
                                    slash.then(
                                            letters.bind(name ->
                                                    close.bind(irrelevant ->
                                                            retn(openTag + " - " + inner + " - " + name)
                                                    ))))));

    public static final Parser<Character, String> realNode =
            open.then(
                    letters.label("realNodeLetters").bind(name ->
                            wspaces.then(
                                    property.sepEndBy(wspaces).bind(properties ->
                                            close.then(
                                                    nodes.many().bind(inner ->
                                                            open.then(
                                                                    slash.then(
                                                                            string(name).then(
                                                                                    wspaces.then(close).bind(irrelevant ->
                                                                                            retn(name + "[" + properties + "]{" + inner + "}")
                                                                                    ))))))))));

    public static final Parser<Character, String> text = satisfy((Character c) -> c != '<').many().bind(xs -> retn(IList.listToString(xs)));

    public static final Parser<Character, String> properties = property.sepEndBy(wspaces).bind(strings -> retn(strings.toString()));

    public static final Parser<Character, String> weirdTag = strBetween(open, close).bind(x -> retn(x));

    public static final Parser<Character, String> shortNode =
            open.then(letters.bind(name -> wspaces.then(properties.bind(props -> slash.then(close.bind(irr -> retn("<" + name + "[" + props + "]/>")))))));



    /*public static final Parser<Character, String> fullNode =
            open.then(letters.bind(name -> wspaces.then(properties.bind(props ->
                    between(
                            close,
                            string("</" + name + ">"),
                            nodes
                    ).bind(inner -> retn("<" + name + "[" + props.size() + "]{" + inner + "}></" + name + ">")
            )))));*/

    public static final Parser<Character, String> openTag =
            open.then(letters.bind(id -> wspaces.then(choice(
                    close.bind(irr -> retn(id)),
                    properties.bind(props -> wspaces.then(close.bind(irr -> retn(id)))))
            ))).label("openTag");

    public static final Parser<Character, String> closeTag(String name) {
        return string("</" + name + ">");
    }

    // TODO "einfach" so < bei jeder Node haben ist nicht ganz richtig, weil der Lookahead im LL(1) Grammar sowas in der Form nicht hinbekommt
    public static final Parser<Character, String> fullNode =
            openTag.bind(name ->
                    choice(
                            wspaces,
                            nodes
                    ).many().bind(inner ->
                            closeTag(name).bind(irr ->
                                    retn(name)
                            )
                    )
            );

    /*    public static final Parser<Character, String> fullNode =
            open.then(letters.bind(name -> wspaces.then(properties.bind(props -> close.bind(irr ->
                nodes.many().bind(inner ->
            open.then(slash.then(string(name).then(close.bind( irr2 ->
                    retn("<" + name + "[" + props.size() + "]{" + inner + "}></" + name + ">")
            ))))))))));
    */

    public static final Parser<Character, String> document = nodes.sepEndBy1(wspaces).bind(nodes -> retn(nodes.foldr1((x, xs) -> x + xs)));

    public static final Parser<Character, String> lazyNode =
            strBetween(open, close).bind(open -> {
                String name = (open.contains(" ") ? open.substring(0, open.indexOf(" ")) : open);
                return satisfy((Character c) -> c != '<').attempt().many().bind(xs -> retn(IList.listToString(xs))).bind(inner ->
                        string("</" + name + ">").bind(irr -> retn(name)));
            });

    public static final Parser<Character, String> dreckigsterParserImWildenWesten = null;/*
            chr('<').then(letters.bind(name ->
                            wspaces.then(
                                    choice(
                                            chr('>'),
                                            properties.bind(props ->
                                                    choice(
                                                            string("/>"),
                                                            chr('>')
                                                    )
                                            )
                                    )
                            )
                    )
            );*/

    static {
        nodes.set(label(
                choice(
                        letter,
//                      text.label("text"),
                        shortNode.label("shortNode"),
                        fullNode.label("fullNode"),
                        weirdTag.label("weirdNode")
                ).bind(xs -> retn(xs.toString())),
                "nodes"
                )
        );
    }
}