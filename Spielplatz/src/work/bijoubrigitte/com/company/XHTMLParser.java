package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.jparsec.*;
import static work.bijoubrigitte.com.company.jparsec.Scanners.ANY_CHAR;

public class XHTMLParser {

    private static final Parser<String> Whitespace = Scanners.string(" ").many().source();


    public static final Parser<Void> Slash = Scanners.string("/");
    public static final Parser<Void> AnfuehrungsStriche = Scanners.string("\"");
    public static final Parser<Void> Open_Paren = Scanners.string("<");
    public static final Parser<Void> Close_Paren = Scanners.string(">");

    public static final Parser<String> Name = Terminals.Identifier.TOKENIZER.map(fragment -> fragment.text());
    public static final Parser<String> Identifier = Parsers.sequence(AnfuehrungsStriche, Name, AnfuehrungsStriche, (aVoid, s, d) -> s);

    public static final Parser<String> Open_Tag = Parsers.sequence(Open_Paren, Name, Close_Paren, (o, s2, c) -> s2);
    public static final Parser<String> Close_Tag = Parsers.sequence(Open_Paren, Slash, Name, Close_Paren, (o, s, s2, c) -> s2);

    public static final Parser<String> InnerBlock() {
        Parser.Reference<String> blockRef = Parser.newReference();

        Parser<String> result = Parsers.sequence(Open_Tag, ANY_CHAR.not("<").many(), Close_Tag, (s, s2, d) -> s + "\n" + s2 +"\n" + d).many().map(
                strings -> strings.stream().reduce("", (mitlaufenderString, nextStringElement) -> mitlaufenderString + nextStringElement)
        );

        blockRef.set(result);
        return result;
    }

    public static final Parser<String> Begin_Tag = Tag(Name);

    public static final <T> Parser<T> Tag(Parser<T> content){
        Parser<Void> cParser = Close_Paren;
        return Parsers.sequence(Open_Paren, content, Close_Paren, (scheißegal1, ergebnisVonContentParser, scheißegal2) -> ergebnisVonContentParser);
    }

    public static final Parser<String> Block = Parsers.sequence(Open_Tag, InnerBlock().many(), Close_Tag, (s, s2, d) -> s + "\n" + s2 +"\n" + d);

    public static final Parser<?> XHTMLParser = Block;
}
