package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.datatypes.Node;
import work.bijoubrigitte.com.company.datatypes.Property;
import java.util.List;
import work.bijoubrigitte.com.company.jparsec.*;

public class XMLDocumentParser {

    public static Parser<Void> eq = Scanners.isChar('=').between(whitespace(), whitespace()).label("[=]");
    public static Parser<Void> quote = Scanners.isChar('\"').between(whitespace(), whitespace()).label("[\"]");
    public static Parser<Void> open = Scanners.isChar('<').between(whitespace(), whitespace()).label("[<]");
    public static Parser<Void> slash = Scanners.isChar('/').between(whitespace(), whitespace()).label("[/]");
    public static Parser<Void> close = Scanners.isChar('>').between(whitespace(), whitespace()).label("[>]");

    public static Parser<List<Void>> whitespace() {
        return Scanners.WHITESPACES.many().label("whitespace");
    }

    public static Parser<String> identifier() {
//        Parser<String> bigdecimal = Scanners.
        return Terminals.Identifier.TOKENIZER.map(Tokens.Fragment::text)
               .label("identifier");
    }

    public static Parser<Property> property() {
        return Parsers.sequence(
                identifier(),
                eq,
                quote,
                (Scanners.isChar(Character::isLetterOrDigit).source().many1()).source().label("[anythingBut]"),
                quote,
                (id, $1, $2, value, $3) -> new Property(id, value)
        ).label("property");
    }

    public static Parser<Node> shortNode() {
        return Parsers.sequence(
                open,
                identifier(),
                property().many().asOptional(),
                slash,
                close,
                ($1, id, properties, $2, $3) -> properties.isPresent() ? Node.withProperties(id, properties.get()) : new Node(id)
//                        "weirdNode => " + String.join(
//                                System.getProperty("line.separator"),
//                                StatisticsCollector.indent("shortNode{", 0),
//                                StatisticsCollector.indent("Identifier : " + bigdecimal, 4),
//                                properties.isPresent() && properties.get().size() > 0 ?
//                                        String.join(
//                                                System.getProperty("line.separator"),
//                                                StatisticsCollector.indent("Properties : {", 4),
//                                                StatisticsCollector.indent(
//                                                        properties.get().stream()
//                                                                .map(s -> StatisticsCollector.indent(s, 4))
//                                                                .collect(Collectors.joining(",\n\t"))
//                                                        , 4),
//                                                StatisticsCollector.indent("}", 4)
//                                        ) : "No Properties",
//                                "}"
//                        )
        ).label("shortNode");
    }

    public static Parser<Node> weirdNode() {
        return Parsers.sequence(
                open,
                Scanners.notChar('>').many1().source().label("weirdContent"),
                close,
                ($1, s, $2) -> new Node("weirdNode[" +s.length()+ "]")
                ).label("weirdNode");
    }

    public static Parser<Node> fullNode() {
        Parser.Reference<Node> content = Parser.newReference();
        Parser<Node> fullNode = ParserExtension.sequence(
                open,
                identifier(),
                property().many().asOptional(),
                close,
                content.lazy().many().asOptional(),
                open,
                slash,
                identifier(),
                close,
                ($1, id, properties, $2, children, $3, $4, id2, $5) -> {
                    if (id.equals(id2)) {
                        if (properties.isPresent() && children.isPresent()) {
                            return new Node(id, children.get(), properties.get());
                        } else {
                            if (properties.isPresent()) {
                                return Node.withProperties(id, properties.get());
                            } else {
                                return Node.withChildren(id, children.get());
                            }
                        }
                    }
                    return new Node("undefined");
                }
        ).or(shortNode()).label("fullNode");
        content.set(fullNode);
        return fullNode;
    }

    public static Parser<Node> document() {
        return fullNode().many().map(nodes -> Node.withChildren("document", nodes)).label("document");
    }
}
