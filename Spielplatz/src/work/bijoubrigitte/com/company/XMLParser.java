package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.datatypes.Node;
import work.bijoubrigitte.com.company.datatypes.Property;
import java.util.List;
import work.bijoubrigitte.com.company.jparsec.*;

public class XMLParser {

    public static Parser<Void> whitespace = Scanners.isChar(Character::isWhitespace).label("whitespace");
    public static Parser<List<Void>> whitespaces = whitespace.many().label("whitespaces");

    // Terminals
    public static Parser<String> eq = Scanners.isChar('=').source().label("[=]");
    public static Parser<String> singleQuote = Scanners.isChar('\'').source().label("[\']");
    public static Parser<String> doubleQuote = Scanners.isChar('\"').source().label("[\"]");
    public static Parser<String> open = Scanners.isChar('<').source().label("[<]");
    public static Parser<String> slash = Scanners.isChar('/').source().label("[/]");
    public static Parser<String> backslash = Scanners.isChar('\\').source().label("[\\]");
    public static Parser<String> close = Scanners.isChar('>').source().label("[>]");
    public static Parser<String> colon = Scanners.isChar(':').source().label("[:]");
    public static Parser<String> dot = Scanners.isChar('.').source().label("[.]");

    // Characters
    public static Parser<String> letter = Scanners.isChar(Character::isLetter).source().label("letter");
    public static Parser<String> character = Scanners.ANY_CHAR.source().label("character");
    public static Parser<String> digit = Scanners.isChar(Character::isDigit).source().label("digit");

    // characters läuft bis EOF
    public static Parser<String> characters = character.many1().source().label("characters");
    public static Parser<String> letters = letter.many1().source().label("letters");
    public static Parser<String> digits = digit.many1().source().label("digits");

    public static Parser<String> value(char c) {
        return Scanners.notChar(c).many1().source().label("anythingUntil");
    }

    // aa, a:aa, ?aa, !aa, aa:aa
    public static Parser<String> identifier = Parsers.sequence(
            Parsers.or(
                    Scanners.isChar('?'),
                    Scanners.isChar('!')
            ).source().asOptional(),
            letters.sepBy(colon)
    ).source().label("identifier");

    public static Parser<Property> property = Parsers.sequence(letters, whitespaces, eq, whitespaces, doubleQuote,
            value('\"'),
            doubleQuote,
            (name, $w, eq, $ww, $q, value, $p) -> new Property(name, value)).label("property");

    public static Parser<List<Property>> properties = property.followedBy(whitespaces).many().label("properties");

    public static Parser<Node> shortNode = Parsers.sequence(open, identifier, whitespaces, properties, slash, close,
            ($o, id, $w, properties, $1, $c) -> Node.withProperties(id, properties)).label("shortNode");

    public static Parser<Node> sTag = Parsers.sequence(open, identifier, whitespaces, properties, whitespaces, close,
            (s, id, voids, properties, voids2, s3) -> Node.withProperties(id, properties)).label("startTag");
    public static Parser<String> eTag = Parsers.sequence(open, slash, identifier, whitespaces, close, (s, s2, $s, voids, s3) -> s2).label("endTag");

    public static Parser<Node> fullNode() {
        Parser.Reference<List<Node>> content = Parser.newReference();
        Parser<Node> node = Parsers.or(
                shortNode,
                Parsers.sequence(
                        sTag,
                        content.lazy(),
                        eTag,
                        (node1, nodes, d) -> new Node(node1.identifier, nodes, node1.properties)
                )
        );
        content.set(node.many());
        return node.label("node");
    }

    public static <T> Parser<String> anythingBut(Parser<T> delimiter) {
        return Parsers.or(
                Scanners.isChar(Character::isWhitespace).label("whitespace"),
                delimiter.label("delimiter"),
                Scanners.ANY_CHAR.label("anyChar")
        ).many1().source().label("anythingBut");
    }

    public static Parser<Node> element() {
        Parser.Reference<List<Node>> content = Parser.newReference();
        Parser<Node> element =
                Parsers.or(
                Parsers.sequence(
                        open.source(),
                        anythingBut(Scanners.isChar('>')), (a, b) -> new Node(a+b)),
                Parsers.sequence(
                        open.source(),
                        content.lazy(),
                        (a, b) -> new Node(a+b))
        );
        content.set(elements);

        return element.label("element");
    }

    public static Parser<List<Node>> elements = element().many().label("elements");

    public static Parser<Node> document() {
        return elements.map(nodes -> Node.withChildren("document", nodes)).label("document");
    }
}
