package work.bijoubrigitte.com.company;

import java.util.List;
import work.bijoubrigitte.com.company.datatypes.Combo;
import work.bijoubrigitte.com.company.datatypes.Node;
import work.bijoubrigitte.com.company.datatypes.Property;
import work.bijoubrigitte.com.company.jparsec.Parser;
import work.bijoubrigitte.com.company.jparsec.ParserExtension;
import work.bijoubrigitte.com.company.jparsec.Parsers;
import work.bijoubrigitte.com.company.jparsec.Scanners;

public class XMLParser2 {


    public static Parser<Void> whitespace = Scanners.isChar(Character::isWhitespace).label("whitespace");
    public static Parser<List<Void>> whitespaces = whitespace.many().label("whitespaces");

    // Terminals
    public static Parser<Void> eq = Scanners.isChar('=').between(whitespaces, whitespaces).label("[=]");
    public static Parser<Void> singleQuote = Scanners.isChar('\'').between(whitespaces, whitespaces).label("[\']");
    public static Parser<Void> doubleQuote = Scanners.isChar('\"').between(whitespaces, whitespaces).label("[\"]");
    public static Parser<Void> open = Scanners.isChar('<').between(whitespaces, whitespaces).label("[<]");
    public static Parser<Void> slash = Scanners.isChar('/').between(whitespaces, whitespaces).label("[/]");
    public static Parser<Void> backslash = Scanners.isChar('\\').between(whitespaces, whitespaces).label("[\\]");
    public static Parser<Void> close = Scanners.isChar('>').between(whitespaces, whitespaces).label("[>]");
    public static Parser<Void> colon = Scanners.isChar(':').between(whitespaces, whitespaces).label("[:]");
    public static Parser<Void> dot = Scanners.isChar('.').between(whitespaces, whitespaces).label("[.]");

    public static Parser<Void> nonClosingBracket = Scanners.notChar('}');
    public static Parser<Void> nonClosing = Scanners.notChar('>').label("NOT[>]");
    public static Parser<String> nonTerminal = Scanners.notAmong("=\"</>").source();

    // Characters
    public static Parser<String> letter = Scanners.isChar(Character::isLetter).atomic().source().label("letter");
    public static Parser<String> character = Scanners.ANY_CHAR.atomic().source().label("character");
    public static Parser<String> number = Scanners.isChar(Character::isDigit).atomic().source().label("digit");

    public static Parser<String> characters = (character.or(number)).many1().source();
    public static Parser<String> letters = letter.many1().source();

    public static Parser<String> name = Parsers.sequence(
            Scanners.isChar(Character::isLetter).many1(),
            Parsers.sequence(
                    Scanners.isChar(':'),
                    Scanners.isChar(Character::isLetterOrDigit).or(Scanners.among("._:-")).many1()
            ).asOptional()
    ).source().label("name");

    public static Parser<String> elExpression = Parsers.sequence(
            Scanners.isChar('#'),
            Scanners.isChar('{'),
            Scanners.notAmong("}\"").many1(),
            Scanners.isChar('}')
    ).source().label("ELexpression");

    public static Parser<String> path = Parsers.sequence(
            Parsers.or(slash, backslash).source().asOptional(),
            // das ding dazwischen /X/.many
            Parsers.or(
                    Parsers.sequence(letters, slash).many1(),
                    Parsers.sequence(letters, backslash).many1()
            ).source(),
            Parsers.sequence(dot, letters).source().asOptional(),
            (leading, link, trailing) -> leading.orElse("") + link + trailing.orElse("")
    ).source().label("pathValue");

    public static Parser<String> hyperlink = Parsers.sequence(
            Scanners.string("http://").or(Scanners.string("https://")),
            Scanners.isChar(Character::isLetterOrDigit).source().sepBy(
                    Scanners.among("/_-:.")
            ).many1()
    ).source().label("hyperlinkValue");

    public static Parser<String> link = Parsers.or(path, hyperlink).label("linkValue");
    public static Parser<String> stringLiteral = Scanners.isChar(Character::isLetter).many1().source().label("stringValue");

    public static Parser<String> numberLiteral = Parsers.sequence(
            Scanners.isChar(Character::isDigit).many1().source(),
            Parsers.sequence(
                    Scanners.isChar('.').source(),
                    Scanners.isChar(Character::isDigit).many1().source()
            ).asOptional(),
            (s, s2) -> s + s2.orElse("")).label("digitValue");

    public static Parser<String> value(Parser<Void> delimiter) {
        // Parsers.or(stringLiteral, numberLiteral, link, elExpression)
        return Parsers.or(delimiter, character).many1().source().label("anythingBut");
    }

    public static Parser<Property> propertay() {
        return Parsers.sequence(
                name,
                eq,
                singleQuote,
                value(singleQuote),
                (id, $1, $2, value) -> new Property(id, value)
        ).label("property");
    }

    public static Parser<Property> property() {
        return Parsers.sequence(
                name,
                eq,
                doubleQuote,
                value(doubleQuote),
                // doubleQuote, // TODO FIX THIS
                (id, $1, $2, value) -> new Property(id, value)
        ).label("property");
    }

    public static Parser<List<Property>> propertays = (whitespace.next(propertay())).many().asOptional().map(p -> p.orElse(Property.noProperties()));
    public static Parser<List<Property>> properties = property().many().asOptional().map(p -> p.orElse(Property.noProperties()));

    public static Parser<Node> shortNode() {
//        System.out.println("shortNode");
        return Parsers.sequence(
                open.label("shortNode[<]"),
                name.label("shortNode[name]"),
                property().many().label("shortNode[( Property)*]"),
                slash.label("shortNode[/]"),
                close.label("shortNode[>]"),
                ($1, id, properties, $2, $3) -> Node.withProperties(id, properties)
//                        "weirdNode => " + String.join(
//                                System.getProperty("line.separator"),
//                                StatisticsCollector.indent("shortNode{", 0),
//                                StatisticsCollector.indent("Identifier : " + bigdecimal, 4),
//                                properties.isPresent() && properties.get().size() > 0 ?
//                                        String.join(
//                                                System.getProperty("line.separator"),
//                                                StatisticsCollector.indent("Properties : {", 4),
//                                                StatisticsCollector.indent(
//                                                        properties.get().stream()
//                                                                .map(s -> StatisticsCollector.indent(s, 4))
//                                                                .collect(Collectors.joining(",\n\t"))
//                                                        , 4),
//                                                StatisticsCollector.indent("}", 4)
//                                        ) : "No Properties",
//                                "}"
//                        )
        ).atomic().label("shortNode");
    }

    public static Parser<Node> weirdNode() {
//        System.out.println("weirdNode");
        return Parsers.sequence(
                open.label("weirdNode[<]"),
                name.asOptional().label("weirdNode[name?]"),
                nonClosing.many().source().label("weirdNode[!(>)*]"),
                close.label("weirdNode[>]"),
                ($1, id, s, $2) -> new Node("|" + id.orElse("undefined") + "|")
        ).atomic().label("weirdNode");
    }

    public static Parser<Node> fallbackNode() {
        return Parsers.sequence(
                open,
                nonClosing.many1().source(),
                close,
                ($1, s, $2) -> new Node("weirdNode[" + s.length() + "]")
        ).label("weirdNode");
    }

    public static Parser<Node> fullNode() {
//        System.out.println("fullNode");
        Parser.Reference<Node> content = Parser.newReference();
        Parser<Node> fullNode =
                ParserExtension.sequence(
                        open.label("fullNode[<]"),
                        name.label("fullNode[Name]"),
                        property().many().label("fullNode[Properties]"),
                        close.label("fullNode[>]"),
                        content.lazy().many().label("fullNode[Content]"),
                        open.label("fullNode[<]"),
                        slash.label("fullNode[/]"),
                        name.label("fullNode[name]"),
                        close.label("fullNode[>]"),
                        ($1, id, properties, $2, children, $3, $4, id2, $5) -> {
                            if (id.equals(id2)) {
                                return new Node(id, children, properties);
                            }
                            return new Node("undefined", children, properties);
                        }
                ).label("fullNode");
        content.set(fullNode);
        return fullNode;
    }

    public static <T> Parser<String> anythingBut(Parser<T> delimiter) {
        return Parsers.or(
                Scanners.isChar(Character::isWhitespace).label("whitespace"),
                delimiter.label("delimiter"),
                Scanners.ANY_CHAR.label("anyChar")
        ).many1().source().label("anythingBut");
    }

    public static Parser<Combo<String, List<Property>>> sTag =
            Parsers.sequence(
                    open,
                    name,
                    whitespace,
                    properties,
                    close,
                    ($1, id, $w, properties, $2) -> new Combo<>(id, properties)
            );

    public static Parser<String> eTag = Parsers.sequence(
            open, slash, name, close, ($1, $2, s, $3) -> s
    );

    public static Parser<Node> element() {
        Parser.Reference<List<Node>> content = Parser.newReference();
        Parser<Node> element = Parsers.or(
                Parsers.sequence(
                        open,
                        name,
                        whitespace,
                        properties,
                        slash,
                        close,
                        ($1, id, $w, properties, $2, $3) -> Node.withProperties(id, properties)
                ),
                Parsers.sequence(
                        open,
                        Scanners.isChar('?'),
                        name,
                        whitespace,
                        properties,
                        Scanners.isChar('?'),
                        close,
                        ($1, $2, id, $w, properties, $3, $4) -> Node.withProperties(id, properties)
                ),
                Parsers.sequence(
                        open,
                        Scanners.isChar('!'),
                        value(close),
                        (aVoid, aVoid2, d) -> new Node("!undefined")
                ),
                Parsers.sequence(
                        sTag,
                        content.lazy(),
                        eTag,
                        (combo, o, id) -> new Node(combo.getT(), null, combo.getR())
                ),
                Parsers.sequence(
                        open,
                        value(close),
                        (a, b) -> new Node("Fallback")
                )
        );
        content.set(elements);
        return element;
    }

    public static Parser<List<Node>> elements = element().many1().label("elements");

    public static Parser<Node> document() {
        return elements.map(nodes -> Node.withChildren("document", nodes)).label("document");
    }

    public static Parser<String> test() {
        return Parsers.sequence(
                doubleQuote,
                Scanners.ANY_CHAR.source().until(doubleQuote).many(),
                doubleQuote,
                (aVoid, lists, d) -> "\"" + lists.stream().reduce("", (a, b) -> a + b.stream().reduce("", (x, y) -> x + y, (x, y) -> a + b), (a, b) -> a + b) + "\""
        ).many().map(
                lists -> lists.stream().reduce("", (a, b) -> a + b, (a, b) -> a + b)
        );
    }

    public static Parser<String> test2() {
        return Parsers.sequence(
                doubleQuote,
                Parsers.or(
                        Scanners.isChar(Character::isLetterOrDigit),
                        Scanners.among("!§$%&/()?-.,;:_+-*#|"),
                        whitespace
                ).source().many(),
                doubleQuote,
                (aVoid, lists, d) -> "\"" + lists.stream().reduce("", (a, b) -> a + b, (a, b) -> a + b) + "\""
        ).many().map(
                lists -> lists.stream().reduce("", (a, b) -> a + b, (a, b) -> a + b)
        );
    }

    public static Parser<String> test3 = Parsers.or(
            Scanners.isChar(Character::isLetterOrDigit),
            Scanners.among("/_-:.")
    ).many1().source().label("linkValue");
}
