package work.bijoubrigitte.com.company;

import work.bijoubrigitte.com.company.datatypes.Node;
import java.util.List;
import work.bijoubrigitte.com.company.jparsec.Parser;
import work.bijoubrigitte.com.company.jparsec.Parsers;
import work.bijoubrigitte.com.company.jparsec.Scanners;

public class XMLParser3 {

    public static Parser<Void> whitespace = Scanners.isChar(Character::isWhitespace).label("whitespace");
    public static Parser<List<Void>> whitespaces = whitespace.many().label("whitespaces");

    // Terminals
    public static Parser<Void> eq = Scanners.isChar('=').between(whitespaces, whitespaces).label("[=]");
    public static Parser<Void> singleQuote = Scanners.isChar('\'').between(whitespaces, whitespaces).label("[\']");
    public static Parser<Void> doubleQuote = Scanners.isChar('\"').between(whitespaces, whitespaces).label("[\"]");
    public static Parser<Void> open = Scanners.isChar('<').between(whitespaces, whitespaces).label("[<]");
    public static Parser<Void> slash = Scanners.isChar('/').between(whitespaces, whitespaces).label("[/]");
    public static Parser<Void> backslash = Scanners.isChar('\\').between(whitespaces, whitespaces).label("[\\]");
    public static Parser<Void> close = Scanners.isChar('>').between(whitespaces, whitespaces).label("[>]");
    public static Parser<Void> colon = Scanners.isChar(':').between(whitespaces, whitespaces).label("[:]");
    public static Parser<Void> dot = Scanners.isChar('.').between(whitespaces, whitespaces).label("[.]");

    public static Parser<Void> nonClosingBracket = Scanners.notChar('}');
    public static Parser<Void> nonClosing = Scanners.notChar('>').label("NOT[>]");
    public static Parser<String> nonTerminal = Scanners.notAmong("=\"</>").source();

    // Characters
    public static Parser<String> letter = Scanners.isChar(Character::isLetter).atomic().source().label("letter");
    public static Parser<String> character = Scanners.ANY_CHAR.atomic().source().label("character");
    public static Parser<String> number = Scanners.isChar(Character::isDigit).atomic().source().label("digit");

    public static Parser<String> characters = (character.or(number)).many1().source();
    public static Parser<String> letters = letter.many1().source();


    public static <T> Parser<String> anythingBut(Parser<T> delimiter) {
        return Parsers.or(
                Scanners.isChar(Character::isWhitespace).label("whitespace"),
                delimiter.label("delimiter"),
                Scanners.ANY_CHAR.label("anyChar")
        ).many1().source().label("anythingBut");
    }

    public static Parser<Node> element() {
        Parser.Reference<List<Node>> content = Parser.newReference();
        Parser<Node> element =
                Parsers.or(
                Parsers.sequence(
                        open.source(),
                        anythingBut(Scanners.isChar('>')), (a, b) -> new Node(a+b)),
                Parsers.sequence(
                        open.source(),
                        content.lazy(),
                        (a, b) -> new Node(a+b))
        );
        content.set(element.many());

        return element.label("element");
    }

    public static Parser<List<Node>> elements = element().many().label("elements");

    public static Parser<Node> document() {
        return elements.map(nodes -> Node.withChildren("document", nodes)).label("document");
    }
}
