package work.bijoubrigitte.com.company.datatypes;

public final class Combo<T, R> {
    public T t;
    public R r;

    public Combo(T t, R r) {
        this.t = t;
        this.r = r;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public R getR() {
        return r;
    }

    public void setR(R r) {
        this.r = r;
    }

    public static <T, R> Combo<T, R> setT(Combo<T, R> combo, T t) {
        combo.setT(t);
        return combo;
    }

    public static <T, R> Combo<T, R> setR(Combo<T, R> combo, R r) {
        combo.setR(r);
        return combo;
    }
}
