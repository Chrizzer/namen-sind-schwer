package work.bijoubrigitte.com.company.datatypes;

import work.bijoubrigitte.com.company.Main;
import java.util.ArrayList;
import java.util.List;

public class Node {
    public String identifier = "undefined";
    public List<Node> children = new ArrayList<>();
    public List<Property> properties = new ArrayList<>();

    public Node(String identifier) {
        this.identifier = identifier;
    }

    public static Node withProperties(String identifier, List<Property> properties) {
        Node node = new Node(identifier);
        node.properties = properties;
        return node;
    }

    public static Node withChildren(String identifier, List<Node> children) {
        Node node = new Node(identifier);
        node.children = children;
        return node;
    }

    public Node(String identifier, List<Node> children, List<Property> properties) {
        this.identifier = identifier;
        this.children = children;
        this.properties = properties;
    }

    public String pretty(int currentIndent) {
        int nextIndent = currentIndent + 4;
        StringBuilder sb = new StringBuilder();
        sb.append(Main.indent("\"" + identifier + "\"" +" {", currentIndent));
        sb.append(Main.indent("\n", nextIndent));
        if (!properties.isEmpty()) {
            sb.append(Main.indent("Properties = {", nextIndent));
            sb.append("\n");
            for (Property p : properties) {
                sb.append(Main.indent(p.toString(), nextIndent + 4));
                sb.append(Main.indent("\n", nextIndent));
            }
            sb.append(Main.indent("}", nextIndent));
            sb.append(Main.indent("\n", nextIndent));
        }
        if (!children.isEmpty()) {
            for (Node c : children) {
                sb.append(c.pretty(nextIndent));
            }
        }
        sb.append(Main.indent("}", currentIndent));
        sb.append(Main.indent("\n", currentIndent));
        return sb.toString();
    }

    @Override
    public String toString() {
        return identifier
                +"[" + properties.size()//.stream().reduce("", (c1, c2) -> c1 + c2.toString(), (a, b) -> a + b)
                + "]"
                + "{\n"
                + Main.indent(children.stream().reduce("", (c1, c2) -> c1 + c2.toString(), (a, b) -> a + ", " + b), 4)
                + "\n}";
    }

    public static List<Node> noChildren() {
        return new ArrayList<Node>();
    }
}
