package work.bijoubrigitte.com.company.datatypes;

import java.util.ArrayList;
import java.util.List;

public class Property {
    public String identifier = "undefined";
    public String value = "undefined";

    public Property(String identifier, String value) {
        this.identifier = identifier;
        this.value = value;

    }

    public static List<Property> ofSingle(String identifier, String value) {
        return ofSingle(new Property(identifier, value));
    }

    public static List<Property> noProperties() {
        return new ArrayList<Property>();
    }

    public static List<Property> ofSingle(Property p) {
        ArrayList<Property> list = new ArrayList<>();
        list.add(p);
        return list;
    }

    @Override
    public String toString() {
        return "\"" + identifier + "\"" + " = " + "\"" + value + "\"";
    }
}
