package work.bijoubrigitte.com.company.javafp.parsecj.input;

public interface CharInput extends Input<Character> {
    CharSequence getCharSequence();
    CharSequence getCharSequence(int length);
}
