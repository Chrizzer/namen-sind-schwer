package work.bijoubrigitte.com.company.jparsec;

import work.bijoubrigitte.com.company.jparsec.functors.Map9;

public final class ParserExtension {
    public static <A, B, C, D, E, F, G, H, I, T> Parser<T> sequence(
            final Parser<A> p1, final Parser<B> p2, final Parser<C> p3, final Parser<D> p4,
            final Parser<E> p5, final Parser<F> p6, final Parser<G> p7, final Parser<H> p8,
            final Parser<I> p9,
            final Map9<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? extends T> map) {
        return new Parser<T>() {
            @Override boolean apply(ParseContext ctxt) {
                boolean r1 = p1.apply(ctxt);
                if (!r1) return false;
                A o1 = p1.getReturn(ctxt);
                boolean r2 = p2.apply(ctxt);
                if (!r2) return false;
                B o2 = p2.getReturn(ctxt);
                boolean r3 = p3.apply(ctxt);
                if (!r3) return false;
                C o3 = p3.getReturn(ctxt);
                boolean r4 = p4.apply(ctxt);
                if (!r4) return false;
                D o4 = p4.getReturn(ctxt);
                boolean r5 = p5.apply(ctxt);
                if (!r5) return false;
                E o5 = p5.getReturn(ctxt);
                boolean r6 = p6.apply(ctxt);
                if (!r6) return false;
                F o6 = p6.getReturn(ctxt);
                boolean r7 = p7.apply(ctxt);
                if (!r7) return false;
                G o7 = p7.getReturn(ctxt);
                boolean r8 = p8.apply(ctxt);
                if (!r8) return false;
                H o8 = p8.getReturn(ctxt);
                boolean r9 = p9.apply(ctxt);
                if (!r9) return false;
                I o9 = p9.getReturn(ctxt);
                ctxt.result = map.map(o1, o2, o3, o4, o5, o6, o7, o8, o9);
                return true;
            }
            @Override public String toString() {
                return map.toString();
            }
        };
    }
}
