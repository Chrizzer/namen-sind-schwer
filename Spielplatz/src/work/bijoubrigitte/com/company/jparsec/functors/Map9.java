package work.bijoubrigitte.com.company.jparsec.functors;

@FunctionalInterface
public interface Map9<A, B, C, D, E, F, G, H, I, R> {

    R map(A a, B b, C c, D d, E e, F f, G g, H h, I i);
}
